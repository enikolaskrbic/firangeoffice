/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Kupac;
import entity.Ponuda;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.Persistence;
import jpa.KupacJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class FizickaLicaController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxFilter;
    @FXML
    private Label lblNaslov;
    @FXML
    private TableView<Kupac> tblFizickaLica;
    @FXML
    private TableColumn<Kupac, String> tbcIme;
    @FXML
    private TableColumn<Kupac, String> tbcPrezime;
    @FXML
    private TableColumn<Kupac, String> tbcAdresa;
    @FXML
    private TableColumn<Kupac, String> tbcTelefon;
    @FXML
    private TableColumn<Kupac, String> tbcEmail;
    @FXML
    private Button btnDodajLice;
    @FXML
    private Button btnIzmeniLice;
    @FXML
    private Button btnObrisiLice;
    @FXML
    private Button btnIzaberi;

    private KupacJpaController fizickoLiceJpa;
    private ObservableList<Kupac> fizickoLiceAll = FXCollections.observableArrayList();
    private ObservableList<Kupac> fizickoLiceFilter = FXCollections.observableArrayList();
    private Kupac selectedKupac;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        fizickoLiceJpa = new KupacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        okClicked = false;
        hideButtons(update);
        initFizickaLice();
        disableButton(true);
    }

    private void hideButtons(boolean update) {
        btnDodajLice.setVisible(update);
        btnIzmeniLice.setVisible(update);
        btnObrisiLice.setVisible(update);
        btnIzaberi.setVisible(!update);
    }

    private void disableButton(boolean update) {
        //btnDodajLice.setDisable(update);
        btnIzaberi.setDisable(update);
        btnIzmeniLice.setDisable(update);
        btnObrisiLice.setDisable(update);
    }

    private void initFizickaLice() {
        getFizickaLica();
        fizickoLiceTableController();
        initTabel();
    }

    private void getFizickaLica() {
        fizickoLiceAll.clear();
        fizickoLiceFilter.clear();
        try {
            List<Kupac> list = fizickoLiceJpa.findKupacByDeleted(false);
            fizickoLiceAll = FXCollections.observableList(list);
        } catch (Exception e) {
           AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje fizičkih lica iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
           LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getFizickaLica()", this.getClass().getSimpleName());
        }
    }

    private void initTabel() {
        tbcIme.setCellValueFactory(new PropertyValueFactory<Kupac, String>("ime"));
        tbcPrezime.setCellValueFactory(new PropertyValueFactory<Kupac, String>("prezime"));
        tbcAdresa.setCellValueFactory(new PropertyValueFactory<Kupac, String>("adresa"));
        tbcTelefon.setCellValueFactory(new PropertyValueFactory<Kupac, String>("telefon"));
        tbcEmail.setCellValueFactory(new PropertyValueFactory<Kupac, String>("email"));
        // Add filtered data to the table
        tblFizickaLica.setItems(fizickoLiceFilter);
    }

    private void fizickoLiceTableController() {

        fizickoLiceFilter.addAll(fizickoLiceAll);
        tbxFilter.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                updateFilteredKupac();
            }
        });
        tblFizickaLica.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblFizickaLica.getSelectionModel().getSelectedItem() != null) {
                    selectedKupac = tblFizickaLica.getSelectionModel().getSelectedItem();
                    disableButton(false);
                } else {
                    selectedKupac = null;
                    disableButton(true);

                }
            }
        });

    }

    private void updateFilteredKupac() {

        fizickoLiceFilter.clear();

        for (Kupac s : fizickoLiceAll) {
            if (s.getIme().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getAdresa().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getEmail().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getTelefon().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getPrezime().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                fizickoLiceFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyPonudaTableSortOrder();
    }

    private void reapplyPonudaTableSortOrder() {

        ArrayList<TableColumn<Kupac, ?>> sortOrder = new ArrayList<>(tblFizickaLica.getSortOrder());
        tblFizickaLica.getSortOrder().clear();
        tblFizickaLica.getSortOrder().addAll(sortOrder);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    @FXML
    private void dodajLice(ActionEvent event) throws IOException {

        setModalDialog("/view/DodajFizickoLice.fxml");
        DodajFizickoLiceController t = (DodajFizickoLiceController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dodaj fizicko lice");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initFizickaLice();
        }
    }

    @FXML
    private void izmeniLice(ActionEvent event) throws IOException {
        if (selectedKupac == null) {
            return;
        }
        setModalDialog("/view/DodajFizickoLice.fxml");
        DodajFizickoLiceController t = (DodajFizickoLiceController) fl.getController();
        t.setStage(mf);
        t.initialize(true, selectedKupac);
        mf.setTitle("Izmeni fizicko lice");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initFizickaLice();
        }
    }

    @FXML
    private void obrisiLice(ActionEvent event) {
        if (selectedKupac == null) {
            return;
        }
        selectedKupac.setIdDeleted(true);
        try {
            fizickoLiceJpa.edit(selectedKupac);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "obrisiLice()", this.getClass().getSimpleName());
        }
        initFizickaLice();
    }

    @FXML
    private void izaberiLice(ActionEvent event) {
        if (selectedKupac == null) {
            return;
        }
        okClicked = true;
        this.stage.close();

    }

    public Kupac getSelectedKupac() {
        return selectedKupac;
    }

}
