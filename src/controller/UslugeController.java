/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import enums.TipArtikla;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class UslugeController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;
    
    @FXML
    private TextField tbxFilter;
    @FXML
    private Label lblNaslov;
    @FXML
    private TableView<Artikl> tblUsluge;
    @FXML
    private TableColumn<Artikl, String> tbcSifra;
    @FXML
    private TableColumn<Artikl, String> tbcNaziv;
    @FXML
    private TableColumn<Artikl, BigDecimal> tbcCena;
    @FXML
    private Button btnDodajUslugu;
    @FXML
    private Button btnIzmeniUslugu;
    @FXML
    private Button btnObrisiUslugu;
    
    private ObservableList<Artikl> artiklAll = FXCollections.observableArrayList();
    private ObservableList<Artikl> artiklFilter = FXCollections.observableArrayList();
    private ArtiklJpaController artiklJpa;
    private Artikl selectedArtikl;

    /**
     * Initializes the controller class.
     */
   @Override
    public void initialize(boolean update, Object objToUpdate) {
        artiklJpa = new ArtiklJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        initArtikl();
    }
    
     private void initArtikl() {
        getArtikl();
        initArtiklTableControllor();
        initArtiklTable();
        btnIzmeniUslugu.setDisable(true);
        btnObrisiUslugu.setDisable(true);
    }
     
     private void initArtiklTable() {
        tbcSifra.setCellValueFactory(new PropertyValueFactory<Artikl, String>("sifra"));
        tbcNaziv.setCellValueFactory(new PropertyValueFactory<Artikl, String>("naziv"));
        tbcCena.setCellValueFactory(new PropertyValueFactory<Artikl, BigDecimal>("cena"));
        tblUsluge.setItems(artiklFilter);
    }
     
     private void getArtikl() {
        artiklAll.clear();
        artiklFilter.clear();
        try {
            List<Artikl> list = artiklJpa.findByTip(TipArtikla.USLUGA.toString(), false);
            artiklAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje artikla iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getArtikl()", this.getClass().getSimpleName());
        }
    }
     
     private void initArtiklTableControllor() {
        artiklFilter.addAll(artiklAll);
        tbxFilter.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                updateFilteredArtikl();
            }
        });
        tblUsluge.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                selectedArtikl = tblUsluge.getSelectionModel().getSelectedItem();
                
                if(selectedArtikl==null){
                   
                    btnIzmeniUslugu.setDisable(true);
                    btnObrisiUslugu.setDisable(true);
                }else{
                   
                    btnIzmeniUslugu.setDisable(false);
                    btnObrisiUslugu.setDisable(false);
                }

            }
        });

        tblUsluge.setRowFactory(new Callback<TableView<Artikl>, TableRow<Artikl>>() {
            @Override
            public TableRow<Artikl> call(TableView<Artikl> tableView2) {
                final TableRow<Artikl> row = new TableRow<>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index > tblUsluge.getItems().size()) {
                            tblUsluge.getSelectionModel().clearSelection();
                            event.consume();
                            selectedArtikl = null;
                        }
                    }
                });
                return row;
            }
        });
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    

    @FXML
    private void dodajUslugu(ActionEvent event) throws IOException {
        setModalDialog("/view/DodajUslugu.fxml");
        DodajUsluguController t = (DodajUsluguController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dodaj uslugu");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if(t.isOkClicked()){
            initArtikl();
        }
    }

    @FXML
    private void izmeniUslugu(ActionEvent event) throws IOException {
        if(selectedArtikl==null)
            return;
        setModalDialog("/view/DodajUslugu.fxml");
        DodajUsluguController t = (DodajUsluguController) fl.getController();
        t.setStage(mf);
        t.initialize(true, selectedArtikl);
        mf.setTitle("Izmeni uslugu");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if(t.isOkClicked()){
            initArtikl();
        }
    }

    @FXML
    private void obrisiUslugu(ActionEvent event) {
        if(selectedArtikl==null){
            return;
        }
        selectedArtikl.setIdDeleted(true);
        try {
            artiklJpa.edit(selectedArtikl);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "obrisiUslugu()", this.getClass().getSimpleName());
        }
        initArtikl();
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }
    
     private void updateFilteredArtikl() {

        artiklFilter.clear();

        for (Artikl s : artiklAll) {
            if (s.getNaziv().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getSifra().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                artiklFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyArtiklTableSortOrder();
    }

    private void reapplyArtiklTableSortOrder() {

        ArrayList<TableColumn<Artikl, ?>> sortOrder = new ArrayList<>(tblUsluge.getSortOrder());
        tblUsluge.getSortOrder().clear();
        tblUsluge.getSortOrder().addAll(sortOrder);
    }
    
}
