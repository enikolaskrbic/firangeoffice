/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Dobavljac;
import entity.Kolekcija;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.KolekcijaJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajKolekcijuController extends AbstracController  {

     private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;
    
    @FXML
    private TextField tbxDana;
    @FXML
    private TextField tbxOpis;
    @FXML
    private TextField tbxNaziv;
    
    private KolekcijaJpaController kolekcijaJpa;
    private Dobavljac dobavljac;
    private Kolekcija kolekcija;
    
    private boolean update;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        
        kolekcijaJpa = new KolekcijaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        if(update){
            kolekcija = (Kolekcija) objToUpdate;
            initLabels();
        }else{
            dobavljac = (Dobavljac) objToUpdate;
        }
      
            
        setTextFieldChange();
    }
    
    private void initLabels(){
        tbxNaziv.setText(kolekcija.getNaziv());
        tbxDana.setText(""+kolekcija.getDaniIsporuke());
        tbxOpis.setText(kolekcija.getOpis());
    }
    
    private void setTextFieldChange(){
        tbxDana.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                 if (!newValue.matches("\\d*")) {
                    tbxDana.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        if(!tbxNaziv.getText().trim().equals("") &&
            !tbxDana.getText().trim().equals("") 
                ){
                if(update){
                    izmeniKolekciju();
                }else{
                    kreirajKolekciju();
                }
                
                okClicked = true;
                this.stage.close();
                
        }else{
            
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Greška", 
                    "Neispravan unos!", 
                    "Morate da popunite naziv i dane.",
                    Alert.AlertType.ERROR);
        }
    }
    
    private void izmeniKolekciju(){
        kolekcija.setNaziv(tbxNaziv.getText());
        int dana = Integer.parseInt(tbxDana.getText());
        kolekcija.setDaniIsporuke(dana);
        kolekcija.setOpis(tbxOpis.getText());
        try {
            kolekcijaJpa.edit(kolekcija);
        } catch (Exception e) {
             LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniKolekciju()", this.getClass().getSimpleName());
        }
    }
    private void kreirajKolekciju(){
        Kolekcija k = new Kolekcija();
        k.setNaziv(tbxNaziv.getText());
        int dana = Integer.parseInt(tbxDana.getText());
        k.setDaniIsporuke(dana);
        k.setOpis(tbxOpis.getText());
        k.setDobavljac(dobavljac);
        k.setIdDeleted(false);
        try {
            kolekcijaJpa.create(k);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajKolekciju()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void odustani(ActionEvent event) {
        this.stage.close();
    }

    
    
}
