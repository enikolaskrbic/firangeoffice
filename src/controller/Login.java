/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Korisnik;
import controller.MainFrameController;
import custom.AbstractAlertDialog;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.Persistence;
import jpa.KorisnikJpaController;
import loger.LoggerUtil;
import net.sf.jasperreports.engine.JRException;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class Login {

    @FXML
    private TextField tbxUsername;
    @FXML
    private PasswordField passField;
    @FXML
    private Button btnLogin;
    @FXML
    private Label lblInfoError;

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    private KorisnikJpaController korisnikJpa;
    private ObservableList<Korisnik> korisnici = FXCollections.observableArrayList();

    public void initialize() {
        // TODO
        korisnikJpa = new KorisnikJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        preuzmiKorisnike();
        changeTbxListener();
        setKeyEvents();
    }

    private void setKeyEvents() {
        if (stage != null) {
            stage.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
                if (t.getCode() == KeyCode.ENTER) {
                    try {
                        Prijava(new ActionEvent());
                    } catch (IOException | ClassNotFoundException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
                        LoggerUtil.logError(ex.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "setKeyEvents()", this.getClass().getSimpleName());
                    }
                }
                if (t.getCode() == KeyCode.ESCAPE) {
                    stage.close();
                }
            });
        }

    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(true);
        scene = new Scene(root);
    }

    private void preuzmiKorisnike() {
        korisnici.clear();
        try {
            List<Korisnik> list = korisnikJpa.findKorisnikEntities();
            korisnici = FXCollections.observableList(list);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "preuzmiKorisnike()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void Exit(ActionEvent event) {
        this.stage.close();
    }

    @FXML
    private void Prijava(ActionEvent event) throws IOException, ClassNotFoundException, NoSuchAlgorithmException, InvalidKeySpecException {

        String korisnickoIme = tbxUsername.getText();
        String passString = passField.getText();

        if (!checkFields(korisnickoIme, passString)) {
            lblInfoError.setText("Popunite sva polja!");
            lblInfoError.setVisible(true);
        } else {
            for (Korisnik k : korisnici) {
                if (k.getKorisnickoIme().equals(korisnickoIme.trim())
                        && k.getLozinka().equals(passString)) {
                    stage.close();
                    setModalDialog("/view/MainFrame.fxml");
                    MainFrameController t = (MainFrameController) fl.getController();
                    t.setStage(mf);
                    try {
                        t.initialize(k);
                    } catch (SQLException ex) {
                        LoggerUtil.logError(ex.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "Prijava()", this.getClass().getSimpleName());
                    } catch (JRException ex) {
                        LoggerUtil.logError(ex.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "Prijava()", this.getClass().getSimpleName());
                    }
                    mf.setTitle("Firange office");
                    mf.getIcons().add(new Image("/images/firangeIcon.png"));
                    mf.setScene(scene);
                    mf.setMaximized(true);
                    LoggerUtil.logInfo("Logovanje.", k.getKorisnickoIme(), "Prijava", this.getClass().getSimpleName());
                    mf.show();
                    return;
                }
            }
            lblInfoError.setText("Neuspela prijava na sistem!");
            lblInfoError.setVisible(true);
        }

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    private boolean checkFields(String korisnickoIme, String lozinka) {
        if (korisnickoIme.trim().equals("") || lozinka.equals("")) {
            return false;
        }
        return true;
    }

    private void changeTbxListener() {
        tbxUsername.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != newValue) {
                lblInfoError.setVisible(false);
            }
        });
        passField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != newValue) {
                lblInfoError.setVisible(false);
            }
        });
    }

}
