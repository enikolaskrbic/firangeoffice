/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Kupac;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.KupacJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajFizickoLiceController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxTelefon;
    @FXML
    private TextField tbxIme;
    @FXML
    private TextField tbxPrezime;
    @FXML
    private TextField tbxAdresa;
    @FXML
    private TextField tbxEmail;

    private KupacJpaController fizickoLiceJpa;
    private Kupac fizickoLice;
    private boolean update;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        fizickoLiceJpa = new KupacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        if (update) {
            fizickoLice = (Kupac) objToUpdate;
            initLabels();
        }
    }

    private void initLabels() {
        tbxAdresa.setText(fizickoLice.getAdresa());
        tbxEmail.setText(fizickoLice.getEmail());
        tbxIme.setText(fizickoLice.getIme());
        tbxPrezime.setText(fizickoLice.getPrezime());
        tbxTelefon.setText(fizickoLice.getTelefon());
    }

    private void setTextFieldChange() {
//        tbxTelefon.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                 if (!newValue.matches("\\d*")) {
//                    tbxTelefon.setText(newValue.replaceAll("[^\\d]", ""));
//                }
//            }
//        });
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        if (!tbxIme.getText().trim().equals("")
                && !tbxAdresa.getText().trim().equals("")
                && //!tbxEmail.getText().trim().equals("") &&
                !tbxPrezime.getText().trim().equals("")
                && !tbxTelefon.getText().trim().equals("")) {
            if (update) {
                izmeniFizickoLice();
            } else {
                kreirajFizickoLice();
            }
            okClicked = true;
            this.stage.close();

        } else {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neispravan unos!",
                    "Morate da popunite sva polja.",
                    Alert.AlertType.ERROR);
        }
    }

    private void kreirajFizickoLice() {
        fizickoLice = new Kupac();
        fizickoLice.setAdresa(tbxAdresa.getText().trim());
        fizickoLice.setEmail(tbxEmail.getText().trim());
        fizickoLice.setIme(tbxIme.getText().trim());
        fizickoLice.setPrezime(tbxPrezime.getText().trim());
        fizickoLice.setTelefon(tbxTelefon.getText().trim());
        fizickoLice.setIdDeleted(false);
        try {
            fizickoLiceJpa.create(fizickoLice);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajFizickoLice()", this.getClass().getSimpleName());
        }
    }

    private void izmeniFizickoLice() {

        fizickoLice.setAdresa(tbxAdresa.getText().trim());
        fizickoLice.setEmail(tbxEmail.getText().trim());
        fizickoLice.setIme(tbxIme.getText().trim());
        fizickoLice.setPrezime(tbxPrezime.getText().trim());
        fizickoLice.setTelefon(tbxTelefon.getText().trim());

        try {
            fizickoLiceJpa.edit(fizickoLice);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniFizickoLice()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void odustani(ActionEvent event) {
        okClicked = false;
        this.stage.close();

    }

    public Kupac getFizickoLice() {
        return fizickoLice;
    }

}
