/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Dobavljac;
import entity.OvlascenoLice;
import entity.Ponuda;
import entity.Uplata;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.persistence.Persistence;
import jpa.OvlascenoLiceJpaController;
import jpa.PonudaJpaController;
import jpa.UplataJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class UplateController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxFilter;
    @FXML
    private Label lblNaslov;
    @FXML
    private TableView<Uplata> tblUplate;
    @FXML
    private TableColumn<Uplata, Long> tbcSifra;
    @FXML
    private TableColumn<Uplata, Date> tbcDatum;
    @FXML
    private TableColumn<Uplata, BigDecimal> tbcIznos;
    @FXML
    private Button btnDodajUplatu;
    @FXML
    private Button btnObrisiUplatu;

    private Ponuda ponuda;
    private Uplata selectedUplata;
    private UplataJpaController uplataJpaController;
    private PonudaJpaController ponudaJpaController;
    private BigDecimal ukupno;

    private ObservableList<Uplata> uplataAll = FXCollections.observableArrayList();
    private ObservableList<Uplata> uplataFilter = FXCollections.observableArrayList();

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    @FXML
    private Label lblUkupno;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        uplataJpaController = new UplataJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        ponudaJpaController = new PonudaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        okClicked = true;
        if (objToUpdate != null) {
            ponuda = (Ponuda) objToUpdate;
            init();
        }

        disableButtons(true);

    }

    private void disableButtons(boolean update) {

        btnObrisiUplatu.setDisable(update);
    }

    private void init() {
        getUplata();
        uplataTableController();
        initUplataTable();
        initLableUkupno();
    }

    private void initLableUkupno() {
        //lblUkupno.setText("Ukupno: " + ponuda.getAvans().toString());
        ukupno = BigDecimal.ZERO;
        for (Uplata uplata : uplataAll) {
            ukupno = ukupno.add(uplata.getIznos());
        }
        lblUkupno.setText("Uplaćeno: " + ukupno.toString());
    }

    private void getUplata() {
        uplataAll.clear();
        uplataFilter.clear();
        try {
            List<Uplata> list = uplataJpaController.findByActive(false);
            list.stream().filter((uplata) -> (Objects.equals(uplata.getIdPonuda().getIdponuda(), ponuda.getIdponuda()))).forEach((uplata) -> {
                uplataAll.add(uplata);
            });

        } catch (Exception e) {
            e.printStackTrace();
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje uplata iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getUplata()", this.getClass().getSimpleName());
        }
    }

    private void uplataTableController() {
        uplataFilter.addAll(uplataAll);

        tbxFilter.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {

                updateFilteredUplata();
            }
        });

        tblUplate.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblUplate.getSelectionModel().getSelectedItem() != null) {
                    selectedUplata = tblUplate.getSelectionModel().getSelectedItem();
                    disableButtons(false);
                } else {
                    selectedUplata = null;
                    disableButtons(true);
                }
            }
        });
    }

    private void initUplataTable() {
        tbcSifra.setCellValueFactory(new PropertyValueFactory<Uplata, Long>("idUplata"));
        tbcIznos.setCellValueFactory(new PropertyValueFactory<Uplata, BigDecimal>("iznos"));
        tbcDatum.setCellValueFactory(new PropertyValueFactory<Uplata, Date>("datumUplate"));
        tbcDatum.setCellFactory(new Callback<TableColumn<Uplata, Date>, TableCell<Uplata, Date>>() {
            @Override
            public TableCell<Uplata, Date> call(TableColumn<Uplata, Date> p) {
                return new TableCell<Uplata, Date>() {
                    @Override
                    protected void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item != null) {
                            setText((String) sdf.format(item));
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });
        tblUplate.setItems(uplataFilter);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void dodajUplatu(ActionEvent event) {
        try {
            setModalDialog("/view/DodajAvans.fxml");
        } catch (IOException ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "dodajUplatu()", this.getClass().getSimpleName());
        }
        DodajAvansController tt1 = (DodajAvansController) fl.getController();
        tt1.setStage(mf);
        tt1.initialize(false, ponuda);
        mf.setTitle("Dodaj uplatu");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (tt1.isOkClicked()) {
            ponuda = ponudaJpaController.findPonuda(ponuda.getIdponuda());
            init();
        }
    }

    @FXML
    private void obrisiUplatu(ActionEvent event) {

        if (selectedUplata == null) {
            return;
        }
        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!", "Da li ste sigurni?",
                "Obrisaćete uplatu!", Alert.AlertType.CONFIRMATION);
        if (a.getResult().get() != ButtonType.OK) {
            return;
        }

        try {
            uplataJpaController.destroy(selectedUplata.getIdUplata());
            init();
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "obrisiUplatu()", this.getClass().getSimpleName());
        }
        ponuda = ponudaJpaController.findPonuda(ponuda.getIdponuda());
        ponuda.setAvans(ukupno);
        ponuda.setIsplacena("NE");
        try {
            ponudaJpaController.edit(ponuda);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "obrisiUplatu()", this.getClass().getSimpleName());
        }

        ponuda = ponudaJpaController.findPonuda(ponuda.getIdponuda());

    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    private void updateFilteredUplata() {

        uplataFilter.clear();

        for (Uplata s : uplataAll) {
            if (sdf.format(s.getDatumUplate()).toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getIznos().toString().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                uplataFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyPonudaTableSortOrder();
    }

    private void reapplyPonudaTableSortOrder() {

        ArrayList<TableColumn<Uplata, ?>> sortOrder = new ArrayList<>(tblUplate.getSortOrder());
        tblUplate.getSortOrder().clear();
        tblUplate.getSortOrder().addAll(sortOrder);
    }

    public BigDecimal getUkupno() {
        return ukupno;
    }
}
