/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.itextpdf.text.pdf.PdfWriter;
import com.mysql.jdbc.Connection;
import custom.AbstractAlertDialog;
import entity.Dobavljac;
import entity.Kupac;
import entity.Korisnik;
import entity.Ponuda;
import entity.Porudzbina;
import entity.StavkaPonude;
import entity.StavkaPorudzbine;
import entity.Uplata;
import enums.Role;
import enums.StatusPonude;
import enums.StatusPorudzbine;
import enums.StilTabele;
import enums.TipArtikla;
import enums.TipPonude;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.persistence.Persistence;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import jpa.KupacJpaController;
import jpa.PonudaJpaController;
import jpa.PorudzbinaJpaController;
import jpa.StavkaPorudzbineJpaController;
import loger.LoggerUtil;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Skrbic
 */
public class MainFrameController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    public static String DB = "jdbc:mysql://localhost:3306/firangeoffice";
    public static String USER = "root";
    public static String PASSWORD = "admin";

    public static String rootPath;
    public static String kupciPath;
    public static String ponudaJasperPath;
    public static String porudzbinaJasperPath;
    public static StilTabele stilTabele = StilTabele.BEZ_BOJE;

    @FXML
    private TextField tbxFilter;

    public static Korisnik korisnik;
    private KupacJpaController fizickoLiceJpa;
    private Kupac selectedKupac;

    private MenuItem cmItemPonudaPrihvacena = new MenuItem("Prihvati ponudu");
    private MenuItem cmItemPonudaOdbijena = new MenuItem("Odbij ponudu");
    private MenuItem cmItemPosaljiDobavljacima = new MenuItem("Pošalji dobavljačima");
    private MenuItem cmItemPotvrdiliDobavljaci = new MenuItem("Dobavljači potvrdili");
    private MenuItem cmItemOdbiliDobavljaci = new MenuItem("Dobavljači odbili");
    private MenuItem cmItemPrimljenaRoba = new MenuItem("Roba primljena");
    private MenuItem cmItemPosaljiNaSivenje = new MenuItem("Pošalji na šivenje");
    private MenuItem cmItemRobaSasivena = new MenuItem("Roba sašivena");
    private MenuItem cmItemMontaza = new MenuItem("Posalji na montažu");
    private MenuItem cmItemZavrsenaMontaza = new MenuItem("Završena montaža");
    private MenuItem cmItemPDF = new MenuItem("PDF");
    private MenuItem cmItemPorudzbine = new MenuItem("Porudžbine");
    private MenuItem cmItemUplate = new MenuItem("Uplate");
    private MenuItem cmItemStorniraj = new MenuItem("Storniraj");
    final ContextMenu cmPonuda = new ContextMenu();

    private ObservableList<Kupac> listAll = FXCollections.observableArrayList();
    private ObservableList<Kupac> listFilter = FXCollections.observableArrayList();
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    @FXML
    private TableView<Ponuda> tblPorudzbine;
    @FXML
    private TableColumn<Ponuda, Integer> tbcSifraPor;
    @FXML
    private TableColumn<Ponuda, Date> tbcDatumPor;
    @FXML
    private TableColumn<Ponuda, String> tbcPoslodavac;
    @FXML
    private TableColumn<Ponuda, BigDecimal> tbcAvansPor;
    @FXML
    private TableColumn<Ponuda, String> tbcStatus;
    @FXML
    private TableColumn<Ponuda, BigDecimal> tbcUkupno;
    @FXML
    private TableColumn<Ponuda, String> tbcKupac;
    @FXML
    private TableColumn<Ponuda, String> tbcTip;
    @FXML
    private TableColumn<Ponuda, String> tbcZakljuceno;

    @FXML
    private Button btnDodajPorudzbinu;
    @FXML
    private Label lblBrojPonuda;
    @FXML
    private GridPane gridPaneStatusBar;

    private PonudaJpaController ponudaJpaController;
    private PorudzbinaJpaController porudzbinaJpaController;
    private StavkaPorudzbineJpaController stavkaPorudzbineJpaController;
    private Ponuda selectedPonuda;
    private ObservableList<Ponuda> porudzbinaAll = FXCollections.observableArrayList();
    private ObservableList<Ponuda> porudzbinaKupac = FXCollections.observableArrayList();
    private ObservableList<Ponuda> porudzbinaFilter = FXCollections.observableArrayList();

    //init jasper
    public static JasperReport jasperReportPonuda;
    public static JasperReport jasperReportPorudzbina;
    public static Connection conn;
    @FXML
    private Label lblBrojPorudzbina;
    @FXML
    private Label lblIspacenih;
    @FXML
    private Label lblNeisplacenih;

    @FXML
    private Button btnOsveziPonude;
    @FXML
    private Button btnIzmeniPonudu;
    @FXML
    private CheckMenuItem chbPrikaziBojuReda;
    @FXML
    private CheckMenuItem chbPrikaziBojuCelije;
    @FXML
    private CheckMenuItem chbBezBoje;
    @FXML
    private Menu menuKorisnici;

    public void initialize(Korisnik k) throws IOException, ClassNotFoundException, SQLException, JRException {
        korisnik = k;
        fizickoLiceJpa = new KupacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        ponudaJpaController = new PonudaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        porudzbinaJpaController = new PorudzbinaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        stavkaPorudzbineJpaController = new StavkaPorudzbineJpaController(Persistence.createEntityManagerFactory("FirangePU"));
//        initFizickaLice();

        initPonuda();
        initPopupMenu();
        initStageController();
        initStatusBar();
        initProperties();
        initJasperProp();
        initMenuBar();
        btnIzmeniPonudu.setDisable(true);
        chbBezBoje.setSelected(true);

    }

    private void initMenuBar() {
        if (MainFrameController.korisnik.getRole().equals(Role.ADMIN.toString())
                || MainFrameController.korisnik.getRole().equals(Role.SUPERADMIN.toString())) {
            menuKorisnici.setVisible(true);
        } else {
            menuKorisnici.setVisible(false);
        }
    }

    public static void initJasperProp() throws ClassNotFoundException, SQLException, JRException {
        Class.forName("com.mysql.jdbc.Driver");
        conn = (Connection) DriverManager.getConnection(DB, USER, PASSWORD);
    }

    private void initStatusBar() {
        int brojPonuda = 0;
        int brojPorudzbina = 0;
        int brojIsplacenih = 0;
        int brojNeIsplacenih = 0;
        for (Ponuda ponuda : porudzbinaAll) {
            brojPonuda++;
            brojPorudzbina += ponuda.getPorudzbinaCollection().size();
            if (ponuda.getIsplacena().equals("NE")) {
                brojNeIsplacenih++;
            } else {
                brojIsplacenih++;
            }

        }
        lblBrojPonuda.setText("Broj ponuda: " + brojPonuda);
        lblBrojPorudzbina.setText("Broj porudžbina: " + brojPorudzbina);
        lblIspacenih.setText("Isplaćenih ponuda: " + brojIsplacenih);
        lblNeisplacenih.setText("Ne isplaćenih ponuda: " + brojNeIsplacenih);
    }

    private void initProperties() throws FileNotFoundException, IOException {

        File pathFile = new File("firange.properties");
        Properties properties;
        try (FileInputStream fileInput = new FileInputStream(pathFile)) {
            properties = new Properties();
            properties.load(fileInput);
        }

        Enumeration enuKeys = properties.keys();

        while (enuKeys.hasMoreElements()) {
            String key = (String) enuKeys.nextElement();
            String value = properties.getProperty(key);
            switch (key) {
                case "rootPath":
                    rootPath = value;
                    break;
                case "kupci":
                    kupciPath = value;
                    break;
                case "ponudaJasper":
                    ponudaJasperPath = value;
                    break;
                case "porudzbinaJasper":
                    porudzbinaJasperPath = value;
                    break;
            }
        }
    }

    private void initPopupMenu() {

        cmPonuda.getItems().add(cmItemPonudaPrihvacena);
        cmPonuda.getItems().add(cmItemPonudaOdbijena);

        cmPonuda.getItems().add(cmItemPosaljiDobavljacima);
        cmPonuda.getItems().add(cmItemPotvrdiliDobavljaci);
        cmPonuda.getItems().add(cmItemOdbiliDobavljaci);

        cmPonuda.getItems().add(cmItemPrimljenaRoba);
        cmPonuda.getItems().add(cmItemPosaljiNaSivenje);
        cmPonuda.getItems().add(cmItemRobaSasivena);
        cmPonuda.getItems().add(cmItemMontaza);
        cmPonuda.getItems().add(cmItemZavrsenaMontaza);
        cmPonuda.getItems().add(new SeparatorMenuItem());
        cmPonuda.getItems().add(cmItemPDF);

        cmPonuda.getItems().add(new SeparatorMenuItem());
        cmPonuda.getItems().add(cmItemPorudzbine);
        cmPonuda.getItems().add(cmItemUplate);
        cmPonuda.getItems().add(new SeparatorMenuItem());
        cmPonuda.getItems().add(cmItemStorniraj);

        cmItemPDF.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                        //Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/firangeoffice", "root", "admin");

                        //String jasperFileName = "D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\ponuda.jasper";
                        String jrxmlFileName = MainFrameController.ponudaJasperPath; //"D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\ponuda.jrxml";
                        //JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);

                        JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFileName);
                        // JasperCompileManager.compileReportToFile("D:/netbeansWorkspace/firangeOffice/src/reports/porudzbina.jrxml", "D:/netbeansWorkspace/firangeOffice/src/reports/porudzbina.jrxml");
                        Map map = new HashMap();
                        map.put("idPon", selectedPonuda.getIdponuda());

                        JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, map, conn);

                        // JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, conn);
                        JasperViewer.viewReport(jprint, false);

                    } catch (Exception e) {
                        LoggerUtil.logError(e.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "initPopupMenu()", this.getClass().getSimpleName());
                    }
                }

            }
        });

        cmItemPonudaPrihvacena.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    selectedPonuda.setTip(TipPonude.PREDRACUN.toString());
                    setStatusPorudzbine(StatusPonude.PRIHVACENA_PONUDA.toString());
                    kreirajPorudzbine(selectedPonuda);
                    initPonuda();

                }

            }
        });

        cmItemPosaljiDobavljacima.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ZAPOSLEN.toString())
                            || korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.NA_CEKANJU_DOBAVLJACA.toString());
                }

            }
        });

        cmItemOdbiliDobavljaci.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())
                            || korisnik.getRole().equals(Role.ZAPOSLEN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.DOBAVLJAC_ODBIO.toString());
                }

            }
        });

        cmItemPonudaOdbijena.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.KUPAC_ODBIO.toString());
                }

            }
        });

        cmItemPorudzbine.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    try {
                        setModalDialog("/view/Porudzbina.fxml");
                    } catch (IOException ex) {
                        LoggerUtil.logError(ex.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "initPopupMenu()", this.getClass().getSimpleName());
                    }
                    PorudzbinaControler tt1 = (PorudzbinaControler) fl.getController();
                    tt1.setStage(mf);
                    tt1.initialize(true, selectedPonuda);
                    mf.setTitle("Porudzbina");
                    mf.getIcons().add(new Image("/images/firangeIcon.png"));
                    mf.setScene(scene);
                    mf.showAndWait();
                    if (tt1.isOkClicked()) {
                        initPonuda();
                    }
                }

            }
        });

        cmItemUplate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {

                    try {
                        setModalDialog("/view/Uplate.fxml");
                    } catch (IOException ex) {
                        LoggerUtil.logError(ex.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "initPopupMenu()", this.getClass().getSimpleName());
                    }
                    UplateController tt1 = (UplateController) fl.getController();
                    tt1.setStage(mf);
                    tt1.initialize(false, selectedPonuda);
                    mf.setTitle("Uplate");
                    mf.getIcons().add(new Image("/images/firangeIcon.png"));
                    mf.setScene(scene);
                    mf.showAndWait();
                    if (tt1.isOkClicked()) {
                        String putanja = kreirajPutanjuPonude(selectedPonuda);
                        kreirajJasperPonude(putanja, selectedPonuda);
                        initPonuda();
                    }
                }

            }
        });

        cmItemPotvrdiliDobavljaci.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())
                            || korisnik.getRole().equals(Role.ZAPOSLEN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }

                    setStatusPorudzbine(StatusPonude.DOSTAVLJA_SE.toString());
                    for (Porudzbina p : selectedPonuda.getPorudzbinaCollection()) {
                        p.setStatus(StatusPorudzbine.PRIHVACENA.toString());
                        try {
                            porudzbinaJpaController.edit(p);
                        } catch (Exception e) {
                            LoggerUtil.logError(e.getMessage(),
                                    MainFrameController.korisnik.getKorisnickoIme(),
                                    "initPopupMenu()", this.getClass().getSimpleName());
                        }
                    }
                }

            }
        });

        cmItemPrimljenaRoba.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.ROBA_PRIMLJENA.toString());
                }

            }
        });

        cmItemPosaljiNaSivenje.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.ROBA_NA_SIVENJU.toString());
                }

            }
        });

        cmItemRobaSasivena.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.ROBA_SASIVENA.toString());
                }

            }
        });

        cmItemMontaza.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {

                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.ROBA_NA_MONTAZI.toString());
                }

            }
        });

        cmItemZavrsenaMontaza.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.ZAVRSENA.toString());
                }

            }
        });

        cmItemStorniraj.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPonuda != null) {
                    if (korisnik.getRole().equals(Role.ADMIN.toString())) {
                        AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                                "Neodgovarajuća privilegija!",
                                "Nemate pravo na ovu akciju.",
                                Alert.AlertType.WARNING);
                        return;
                    }
                    AbstractAlertDialog a = new AbstractAlertDialog("Upozorenje!",
                            "Da li ste sigurni?",
                            "Ova ponuda će biti storinirana!",
                            Alert.AlertType.CONFIRMATION);
                    if (a.getResult().get() != ButtonType.OK) {
                        return;
                    }
                    setStatusPorudzbine(StatusPonude.STORNIRANO.toString());
                }

            }
        });

    }

    private void setStatusPorudzbine(String status) {
        //if (!status.equals(selectedPonuda.getStatus())) {
        selectedPonuda.setStatus(status);
        try {
            ponudaJpaController.edit(selectedPonuda);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setStatusPorudzbine()", this.getClass().getSimpleName());
        }
        initPonuda();
        //}

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private void initPonuda() {
        getPonuda();
        porudzbinaTableController();
        initPonudaTable();
        initStatusBar();
    }

    private void getPonuda() {
        porudzbinaAll.clear();
        porudzbinaFilter.clear();
        try {
            List<Ponuda> list = ponudaJpaController.findPonudaEntities();
            porudzbinaAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje ponuda iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getPonuda()", this.getClass().getSimpleName());
        }
    }

    private void getFizickaLica() {
        listAll.clear();
        listFilter.clear();
        try {
            List<Kupac> list = fizickoLiceJpa.findKupacEntities();
            listAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje fizičkih lica iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getFizickaLica()", this.getClass().getSimpleName());
        }
    }

    private void initPonudaTable() {
        tbcSifraPor.setCellValueFactory(new PropertyValueFactory<Ponuda, Integer>("idponuda"));
        tbcSifraPor.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.05));
        tbcPoslodavac.setCellValueFactory(new PropertyValueFactory<Ponuda, String>("korisnikImePrezime"));
        tbcPoslodavac.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.1));
        tbcKupac.setCellValueFactory(new PropertyValueFactory<Ponuda, String>("kupacImePrezime"));
        tbcKupac.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.1));
        tbcAvansPor.setCellValueFactory(new PropertyValueFactory<Ponuda, BigDecimal>("avans"));
        tbcAvansPor.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.15));
        tbcStatus.setCellValueFactory(new PropertyValueFactory<Ponuda, String>("status"));
        tbcStatus.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.15));
        tbcTip.setCellValueFactory(new PropertyValueFactory<Ponuda, String>("tip"));
        tbcTip.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.1));
        tbcZakljuceno.setCellValueFactory(new PropertyValueFactory<Ponuda, String>("isplacena"));
        tbcZakljuceno.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.1));
        tbcUkupno.setCellValueFactory(new PropertyValueFactory<Ponuda, BigDecimal>("ukupno"));
        tbcUkupno.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.15));
        tbcDatumPor.setCellValueFactory(new PropertyValueFactory<Ponuda, Date>("datum"));
        tbcDatumPor.prefWidthProperty().bind(tblPorudzbine.widthProperty().multiply(0.1));
        tbcDatumPor.setCellFactory(new Callback<TableColumn<Ponuda, Date>, TableCell<Ponuda, Date>>() {
            @Override
            public TableCell<Ponuda, Date> call(TableColumn<Ponuda, Date> p) {
                return new TableCell<Ponuda, Date>() {
                    @Override
                    protected void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item != null) {
                            setText((String) sdf.format(item));
                        } else {
                            setText(null);
                        }
                    }
                };
            }
        });

        tbcStatus.setCellFactory(new Callback<TableColumn<Ponuda, String>, TableCell<Ponuda, String>>() {
            @Override
            public TableCell<Ponuda, String> call(TableColumn<Ponuda, String> p) {
                return new TableCell<Ponuda, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        for (StatusPonude dir : StatusPonude.values()) {
                            this.getTableRow().getStyleClass().remove(dir.toString());
                            getStyleClass().remove(dir.toString());
                        }

                        if (!isEmpty()) {
                            if (stilTabele.equals(StilTabele.BOJA_REDA)) {
                                this.getTableRow().getStyleClass().add(item);
                            } else if (stilTabele.equals(StilTabele.BOJA_CELIJE)) {
                                getStyleClass().add(item);
                            }
                        }
                        setText(item);

                    }
                };
            }
        });
        tblPorudzbine.setItems(porudzbinaFilter);
    }

    private void porudzbinaTableController() {
        porudzbinaFilter.addAll(porudzbinaAll);

        tbxFilter.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {

                updateFilteredPonuda();
            }
        });
        tblPorudzbine.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblPorudzbine.getSelectionModel().getSelectedItem() != null) {
                    selectedPonuda = tblPorudzbine.getSelectionModel().getSelectedItem();
                    if (selectedPonuda.getStatus().equals(StatusPonude.NA_CEKANJU_KUPCA.toString())) {
                        btnIzmeniPonudu.setDisable(false);
                    } else {
                        btnIzmeniPonudu.setDisable(true);
                    }
                    tblPorudzbine.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent t) {
                            if (t.getButton() == MouseButton.SECONDARY) {
                                if (selectedPonuda.getStatus().equals(StatusPonude.NA_CEKANJU_KUPCA.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(true);
                                    cmItemPonudaOdbijena.setVisible(true);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.PRIHVACENA_PONUDA.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(true);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.NA_CEKANJU_DOBAVLJACA.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(true);
                                    cmItemOdbiliDobavljaci.setVisible(true);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.DOSTAVLJA_SE.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(true);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.ROBA_PRIMLJENA.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(true);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.ROBA_NA_SIVENJU.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(true);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.ROBA_SASIVENA.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(true);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.ROBA_NA_MONTAZI.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(true);
                                    cmItemUplate.setVisible(true);
                                    cmItemStorniraj.setVisible(true);
                                } else if (selectedPonuda.getStatus().equals(StatusPonude.STORNIRANO.toString())) {

                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(false);
                                    cmItemStorniraj.setVisible(false);

                                } else {
                                    cmItemPonudaPrihvacena.setVisible(false);
                                    cmItemPonudaOdbijena.setVisible(false);
                                    cmItemPosaljiDobavljacima.setVisible(false);
                                    cmItemPotvrdiliDobavljaci.setVisible(false);
                                    cmItemOdbiliDobavljaci.setVisible(false);
                                    cmItemMontaza.setVisible(false);
                                    cmItemPrimljenaRoba.setVisible(false);
                                    cmItemRobaSasivena.setVisible(false);
                                    cmItemPosaljiNaSivenje.setVisible(false);
                                    cmItemZavrsenaMontaza.setVisible(false);
                                    cmItemUplate.setVisible(false);
                                    cmItemStorniraj.setVisible(true);

                                }
                                cmPonuda.show(stage, t.getScreenX(), t.getScreenY());
                            }

                        }
                    });
                } else {
                    btnIzmeniPonudu.setDisable(true);
                }
            }
        });
    }

    private void updateFilteredPonuda() {

        porudzbinaFilter.clear();

        for (Ponuda s : porudzbinaAll) {

            if (s.getIdponuda().toString().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getAdresa().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getAvans().toString().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getDatum().toString().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getStatus().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getKorisnikidkorisnik().getIme().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getKorisnikidkorisnik().getPrezime().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || sdf.format(s.getDatum()).toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getKupacidkupac().getIme().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getKupacidkupac().getPrezime().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getTip().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getIsplacena().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                porudzbinaFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyPonudaTableSortOrder();
    }

    private void reapplyPonudaTableSortOrder() {

        ArrayList<TableColumn<Ponuda, ?>> sortOrder = new ArrayList<>(tblPorudzbine.getSortOrder());
        tblPorudzbine.getSortOrder().clear();
        tblPorudzbine.getSortOrder().addAll(sortOrder);
    }

    private void kreirajPorudzbine(Ponuda ponuda) {

        List<StavkaPonude> stavkaPonudes = (List<StavkaPonude>) ponuda.getStavkaPonudeCollection();
        HashMap<Long, Dobavljac> idDobavljaca = new HashMap<>();
        for (StavkaPonude stavkaPonude : stavkaPonudes) {
            if (stavkaPonude.getArtikl().getTip().equals(TipArtikla.ROBA.toString())) {
                if (!idDobavljaca.containsKey(stavkaPonude.getArtikl().getKolekcijaIdkolekcija().getDobavljac().getIddobavljac())) {
                    idDobavljaca.put(stavkaPonude.getArtikl().getKolekcijaIdkolekcija().getDobavljac().getIddobavljac(), stavkaPonude.getArtikl().getKolekcijaIdkolekcija().getDobavljac());
                }
            }

        }

        String directoryFile = fokusirajSeUFolder(ponuda);

        ArrayList<Long> idPorudzbine = new ArrayList<>();
        for (Map.Entry<Long, Dobavljac> entry : idDobavljaca.entrySet()) {
            Porudzbina porudzbina = new Porudzbina();
            porudzbina.setDobavljaciddobavljac(entry.getValue());
            porudzbina.setDobavljacNaziv(entry.getValue().getNaziv());
            porudzbina.setDobavljacPib(entry.getValue().getPib());
            porudzbina.setDatumKreiranja(new Date());
            porudzbina.setPonudaidponuda(ponuda);
            porudzbina.setStatus(StatusPorudzbine.NA_CEKANJU.toString());
            porudzbina.setDatumDostave(ponuda.getDatum());
            porudzbina.setIdDeleted(false);
            porudzbina.setKorekcija(false);
            porudzbina.setKorekcijaText("");
            porudzbinaJpaController.create(porudzbina);
            for (StavkaPonude stavkaPonude : stavkaPonudes) {
                if (stavkaPonude.getArtikl().getTip().equals(TipArtikla.ROBA.toString())) {
                    if (stavkaPonude.getArtikl().getKolekcijaIdkolekcija().getDobavljac().getIddobavljac() == entry.getKey()) {
                        createStavkaPorudzbine(porudzbina, stavkaPonude);
                    }
                }
            }

            idPorudzbine.add(porudzbina.getIdPorudzbina());

        }

        kreirajPDFZaPorudzbine(idPorudzbine, directoryFile);

    }

    private void kreirajPDFZaPorudzbine(ArrayList<Long> idPorudzbine, String directoryFile) {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            //Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/firangeoffice", "root", "admin");
//
            //String jasperFileName = "D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\Porudzbina.jasper";
            String jrxmlFileName = MainFrameController.porudzbinaJasperPath;//"D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\Porudzbina.jrxml";

            JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFileName);

            for (Long id : idPorudzbine) {
                Map map = new HashMap();
                map.put("idPor", id);

                JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, map, conn);

                String fileName = "/Porudzbina_" + id + ".pdf";
                JasperExportManager.exportReportToPdfFile(jprint, directoryFile + fileName);

            }

        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajPDFZaPorudzbine()", this.getClass().getSimpleName());
        }

    }

    private String fokusirajSeUFolder(Ponuda ponuda) {
        String directoryName = MainFrameController.rootPath + kupciPath;
        crateFileIfNOtExsist(directoryName);
        String kupacFolder = "/" + ponuda.getKupacidkupac().getIdkupac() + "_"
                + ponuda.getKupacidkupac().getIme() + "_" + ponuda.getKupacidkupac().getPrezime();
        directoryName += kupacFolder;
        crateFileIfNOtExsist(directoryName);

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String reportDate = df.format(ponuda.getDatum());
        String ponudaFolder = "/Ponuda_" + ponuda.getIdponuda() + "_" + reportDate;
        directoryName += ponudaFolder;
        crateFileIfNOtExsist(directoryName);

        directoryName += "/Porudzbine";
        crateFileIfNOtExsist(directoryName);

        return directoryName;
    }

    private void crateFileIfNOtExsist(String directoryName) {
        File directory = new File(directoryName);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    private void createStavkaPorudzbine(Porudzbina porudzbina, StavkaPonude stavkaPonude) {
        StavkaPorudzbine stavkaPorudzbine = new StavkaPorudzbine();
        stavkaPorudzbine.setPorudzbinaidPorudzbina(porudzbina);
        stavkaPorudzbine.setStavkaIdstavka(stavkaPonude);
        stavkaPorudzbine.setKolicina(stavkaPonude.getKolicina());
        stavkaPorudzbine.setNazivStavke(stavkaPonude.getNazivStavke());
        stavkaPorudzbine.setSifraStavke(stavkaPonude.getSifraStavke());
        stavkaPorudzbine.setIdDeleted(false);
        int daniZaDostavu = stavkaPonude.getArtikl().getKolekcijaIdkolekcija().getDaniIsporuke();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, daniZaDostavu);
        stavkaPorudzbine.setDatumDostave(c.getTime());
        stavkaPorudzbineJpaController.create(stavkaPorudzbine);
    }

    @FXML
    private void dodajPorudzbinu(ActionEvent event) throws IOException {
        setModalDialog("/view/Ponuda.fxml");
        PonudaController t = (PonudaController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Ponuda");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initPonuda();
            String putanja = kreirajPutanjuPonude(t.getPonuda());
            kreirajJasperPonude(putanja, t.getPonuda());
        }
//        initFizickaLice();

    }

    private String kreirajPutanjuPonude(Ponuda ponuda) {
        String directoryName = MainFrameController.rootPath + kupciPath;
        crateFileIfNOtExsist(directoryName);
        String kupacFolder = "/" + ponuda.getKupacidkupac().getIdkupac() + "_"
                + ponuda.getKupacidkupac().getIme() + "_" + ponuda.getKupacidkupac().getPrezime();
        directoryName += kupacFolder;
        crateFileIfNOtExsist(directoryName);

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String reportDate = df.format(ponuda.getDatum());
        String ponudaFolder = "/Ponuda_" + ponuda.getIdponuda() + "_" + reportDate;
        directoryName += ponudaFolder;
        crateFileIfNOtExsist(directoryName);
        return directoryName;
    }

    private void kreirajJasperPonude(String directoryName, Ponuda ponuda) {
        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/firangeoffice", "root", "admin");
//
            //String jasperFileName = "D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\ponuda.jasper";
            String jrxmlFileName = MainFrameController.ponudaJasperPath;//"D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\ponuda.jrxml";

            JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFileName);

            Map map = new HashMap();
            map.put("idPon", ponuda.getIdponuda());

            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, map, conn);

            String fileName = "/Ponuda" + ponuda.getIdponuda() + ".pdf";
            JasperExportManager.exportReportToPdfFile(jprint, directoryName + fileName);

        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajJasperPonude()", this.getClass().getSimpleName());
        }
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    private void initStageController() {
        stage.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {

//                  if(t1){
//                      initFizickaLice();
//                      initPonuda();
//                  }
            }
        });
    }

    @FXML
    private void evidencijaUsluga(ActionEvent event) throws IOException {
        setModalDialog("/view/Usluge.fxml");
        UslugeController t = (UslugeController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Usluge");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
    }

    @FXML
    private void evidencijaDobavljaca(ActionEvent event) throws IOException {
        setModalDialog("/view/Dobavljaci.fxml");
        DobavljaciController t = (DobavljaciController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dobavljaci");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
    }

    @FXML
    private void evidencijaFizickihLica(ActionEvent event) throws IOException {
        setModalDialog("/view/FizickaLica.fxml");
        FizickaLicaController t = (FizickaLicaController) fl.getController();
        t.setStage(mf);
        t.initialize(true, null);
        mf.setTitle("Fizička Lica");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
    }

    @FXML
    private void evidencijaJediniceMere(ActionEvent event) throws IOException {
        setModalDialog("/view/JediniceMere.fxml");
        JediniceMereController t = (JediniceMereController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Jedinice mere");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
    }

    @FXML
    private void osveziPonude(ActionEvent event) {
        tbxFilter.setText("");
        initPonuda();
    }

    @FXML
    private void evidencijaPorudzbina(ActionEvent event) {
        try {
            setModalDialog("/view/Porudzbina.fxml");
        } catch (IOException ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "evidencijaPorudzbina()", this.getClass().getSimpleName());
        }
        PorudzbinaControler tt1 = (PorudzbinaControler) fl.getController();
        tt1.setStage(mf);
        tt1.initialize(false, null);
        mf.setTitle("Porudzbina");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (tt1.isOkClicked()) {
            initPonuda();
        }
    }

    @FXML
    private void izmeniPonudu(ActionEvent event) throws IOException {
        if (selectedPonuda == null) {
            return;
        }

        setModalDialog("/view/Ponuda.fxml");
        PonudaController t = (PonudaController) fl.getController();
        t.setStage(mf);
        t.initialize(true, selectedPonuda);
        mf.setTitle("Ponuda");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initPonuda();
            String putanja = kreirajPutanjuPonude(selectedPonuda);
            kreirajJasperPonude(putanja, selectedPonuda);
        }
    }

    @FXML
    private void registracijaKorisnika(ActionEvent event) {
    }

    @FXML
    private void privilegijeKorisnika(ActionEvent event) {
    }

    @FXML
    private void prikaziBojuReda(ActionEvent event) {
        chbPrikaziBojuCelije.setSelected(false);
        chbBezBoje.setSelected(false);
        if (chbPrikaziBojuReda.isSelected()) {
            stilTabele = StilTabele.BOJA_REDA;
        }

        if (!chbBezBoje.isSelected() && !chbPrikaziBojuCelije.isSelected() && !chbPrikaziBojuReda.isSelected()) {
            chbBezBoje.setSelected(true);
            chbPrikaziBojuCelije.setSelected(false);
            chbPrikaziBojuReda.setSelected(false);
            stilTabele = StilTabele.BEZ_BOJE;
        }
        initPonuda();

    }

    @FXML
    private void prikaziBojuCelije(ActionEvent event) {
        chbBezBoje.setSelected(false);
        chbPrikaziBojuReda.setSelected(false);
        if (chbPrikaziBojuCelije.isSelected()) {
            stilTabele = StilTabele.BOJA_CELIJE;
        }

        if (!chbBezBoje.isSelected() && !chbPrikaziBojuCelije.isSelected() && !chbPrikaziBojuReda.isSelected()) {
            chbBezBoje.setSelected(true);
            chbPrikaziBojuCelije.setSelected(false);
            chbPrikaziBojuReda.setSelected(false);
            stilTabele = StilTabele.BEZ_BOJE;
        }
        initPonuda();
    }

    @FXML
    private void bezBoje(ActionEvent event) {
        chbPrikaziBojuReda.setSelected(false);
        chbPrikaziBojuCelije.setSelected(false);
        if (chbBezBoje.isSelected()) {
            stilTabele = StilTabele.BEZ_BOJE;
        }

        if (!chbBezBoje.isSelected() && !chbPrikaziBojuCelije.isSelected() && !chbPrikaziBojuReda.isSelected()) {
            chbBezBoje.setSelected(true);
            chbPrikaziBojuCelije.setSelected(false);
            chbPrikaziBojuReda.setSelected(false);
            stilTabele = StilTabele.BEZ_BOJE;
        }
        initPonuda();

    }

}
