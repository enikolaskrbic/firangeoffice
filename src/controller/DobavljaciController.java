/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import entity.Dobavljac;
import entity.Kolekcija;
import entity.OvlascenoLice;
import java.io.IOException;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import jpa.DobavljacJpaController;
import jpa.KolekcijaJpaController;
import jpa.OvlascenoLiceJpaController;
import loger.LoggerUtil;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DobavljaciController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TableView<Dobavljac> tblDobavljaci;
    @FXML
    private TableColumn<Dobavljac, String> tbcNaziv;
    @FXML
    private TableColumn<Dobavljac, String> tbcAdresa;
    @FXML
    private TableColumn<Dobavljac, String> tbcMesto;
    @FXML
    private TextField tbxFilter;
    @FXML
    private TableView<OvlascenoLice> tblOvlascenaLica;
    @FXML
    private TableColumn<OvlascenoLice, String> tbcImeLica;
    @FXML
    private TableColumn<OvlascenoLice, String> tbcPrezimeLica;
    @FXML
    private Label lblPib;
    @FXML
    private Label lblTelefon;
    @FXML
    private Label lblEmail;
    @FXML
    private TableView<Kolekcija> tblKolekcije;
    @FXML
    private TableColumn<Kolekcija, String> tbcNazivKolekcije;
    @FXML
    private TableColumn<Kolekcija, Integer> tbcIsporuka;
    @FXML
    private TableColumn<Kolekcija, String> tbcOpis;
    @FXML
    private Label lblDobavljac;

    @FXML
    private Button btnDodajDobavljaca;
    @FXML
    private Button btnIzmeniDobavljaca;
    @FXML
    private Button btnObrisiDobavljaca;
    @FXML
    private Button btnDodajLice;
    @FXML
    private Button BtnIzmeniLice;
    @FXML
    private Button btnObrisiLice;
    @FXML
    private Button btnDodajKolekciju;
    @FXML
    private Button btnIzmeniKolekciju;
    @FXML
    private Button btnObrisiKolekciju;

    private DobavljacJpaController dobavljacJpa;
    private KolekcijaJpaController kolekcijaJpa;
    private OvlascenoLiceJpaController ovlascenoLiceJpa;
    private ArtiklJpaController artiklJpaController;

    private ObservableList<Dobavljac> dobavljacAll = FXCollections.observableArrayList();
    private ObservableList<Dobavljac> dobavljacFilter = FXCollections.observableArrayList();
    private ObservableList<OvlascenoLice> licaAll = FXCollections.observableArrayList();
    private ObservableList<OvlascenoLice> licaFilter = FXCollections.observableArrayList();
    private ObservableList<Kolekcija> kolekcijaAll = FXCollections.observableArrayList();
    private ObservableList<Kolekcija> kolekcijaFilter = FXCollections.observableArrayList();

    private Dobavljac selectedDobavljac;
    private OvlascenoLice selectedOvlascenoLice;
    private Kolekcija selectedKolekcija;

    final ContextMenu cmPonuda = new ContextMenu();
    private MenuItem cmItemPDF = new MenuItem("PDF");

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        dobavljacJpa = new DobavljacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        ovlascenoLiceJpa = new OvlascenoLiceJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        kolekcijaJpa = new KolekcijaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        artiklJpaController = new ArtiklJpaController(Persistence.createEntityManagerFactory("FirangePU"));

        initDobavljac();
        initOvlascenoLice();
        initKolekcija();
        setButtons(false);

    }

    private void initKolekcija() {
        getKolekcija();
        kolekcijaTableController();
        initKolekcijaTable();
    }

    private void getKolekcija() {
        kolekcijaAll.clear();
        kolekcijaFilter.clear();
        try {
            List<Kolekcija> list = kolekcijaJpa.findKolekcijaByDeleted(false);
            kolekcijaAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje kolekcija iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "getKolekcija()", this.getClass().getSimpleName());

        }
    }

    private void kolekcijaTableController() {
        tblKolekcije.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblKolekcije.getSelectionModel().getSelectedItem() != null) {
                    selectedKolekcija = tblKolekcije.getSelectionModel().getSelectedItem();
                } else {
                    selectedKolekcija = null;
                }
            }
        });
    }

    private void initKolekcijaTable() {
        tbcNazivKolekcije.setCellValueFactory(new PropertyValueFactory<Kolekcija, String>("naziv"));
        tbcIsporuka.setCellValueFactory(new PropertyValueFactory<Kolekcija, Integer>("daniIsporuke"));
        tbcOpis.setCellValueFactory(new PropertyValueFactory<Kolekcija, String>("opis"));
        tblKolekcije.setItems(kolekcijaFilter);

    }

    private void updateFilteredKolekcija() {
        kolekcijaFilter.clear();
        for (Kolekcija k : kolekcijaAll) {
            if (k.getDobavljac().getIddobavljac() == selectedDobavljac.getIddobavljac()) {
                kolekcijaFilter.add(k);
            }
        }
        initKolekcijaTable();
    }

    private void initOvlascenoLice() {
        getOvlascenoLice();
        ovlascenoLiceTableController();
        initOvlascenoLiceTable();
    }

    private void getOvlascenoLice() {
        licaAll.clear();
        licaFilter.clear();
        try {
            List<OvlascenoLice> list = ovlascenoLiceJpa.findOvlascenoLiceByDeleted(Boolean.FALSE);
            licaAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje ovlašćenih lica iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "getOvlascenoLice()", this.getClass().getSimpleName());
        }
    }

    private void ovlascenoLiceTableController() {
        //dobavljacFilter.addAll(dobavljacAll);

        tblOvlascenaLica.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblOvlascenaLica.getSelectionModel().getSelectedItem() != null) {
                    selectedOvlascenoLice = tblOvlascenaLica.getSelectionModel().getSelectedItem();
                } else {
                    selectedOvlascenoLice = null;
                }
            }
        });
    }

    private void initOvlascenoLiceTable() {
        tbcImeLica.setCellValueFactory(new PropertyValueFactory<OvlascenoLice, String>("ime"));
        tbcPrezimeLica.setCellValueFactory(new PropertyValueFactory<OvlascenoLice, String>("prezime"));

        tblOvlascenaLica.setItems(licaFilter);
    }

    private void updateFilteredOvlascenaLica() {
        licaFilter.clear();
        for (OvlascenoLice o : licaAll) {
            if (o.getDobavljac().getIddobavljac() == selectedDobavljac.getIddobavljac()) {
                licaFilter.add(o);
            }
        }
        initOvlascenoLiceTable();
    }

    private void initDobavljac() {
        getDobavljac();
        dobavljacTableController();
        initTabelDobavljac();
    }

    private void getDobavljac() {

        dobavljacAll.clear();
        dobavljacFilter.clear();
        try {
            List<Dobavljac> list = dobavljacJpa.findDobavljacByDeleted(false);
            dobavljacAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje dobavljača iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "getDobavljac()", this.getClass().getSimpleName());
        }
    }

    private void setButtons(boolean temp) {

        btnIzmeniDobavljaca.setDisable(!temp);
        btnObrisiDobavljaca.setDisable(!temp);

        btnDodajKolekciju.setDisable(!temp);
        btnIzmeniKolekciju.setDisable(!temp);
        btnObrisiKolekciju.setDisable(!temp);

        btnDodajLice.setDisable(!temp);
        BtnIzmeniLice.setDisable(!temp);
        btnObrisiLice.setDisable(!temp);

    }

    private void dobavljacTableController() {
        dobavljacFilter.addAll(dobavljacAll);

        tbxFilter.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {

                updateFilteredDobavljac();
            }

        });
        tblDobavljaci.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblDobavljaci.getSelectionModel().getSelectedItem() != null) {
                    Dobavljac p = tblDobavljaci.getSelectionModel().getSelectedItem();
                    selectedDobavljac = p;
                    //set labels
                    lblDobavljac.setText(p.getNaziv());
                    lblEmail.setText(p.getEmail());
                    lblPib.setText(p.getPib());
                    lblTelefon.setText(p.getTelefon());

                    //set tables 
                    updateFilteredOvlascenaLica();
                    updateFilteredKolekcija();

                    //set buttons
                    setButtons(true);
                    tblDobavljaci.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent t) {

                        }
                    });
                } else {
                    //clear labels
                    lblDobavljac.setText("");
                    lblEmail.setText("");
                    lblPib.setText("");
                    lblTelefon.setText("");
                    selectedDobavljac = null;
                    //clear lists
                    licaFilter.clear();
                    kolekcijaFilter.clear();

                    //set buttons
                    setButtons(false);
                }
            }
        });
    }

    private void initTabelDobavljac() {
        tbcNaziv.setCellValueFactory(new PropertyValueFactory<Dobavljac, String>("naziv"));
        tbcMesto.setCellValueFactory(new PropertyValueFactory<Dobavljac, String>("mesto"));
        tbcAdresa.setCellValueFactory(new PropertyValueFactory<Dobavljac, String>("adresa"));
        tblDobavljaci.setItems(dobavljacFilter);
    }

    private void updateFilteredDobavljac() {

        dobavljacFilter.clear();

        for (Dobavljac s : dobavljacAll) {

            if (s.getNaziv().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getAdresa().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getMesto().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                dobavljacFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyDobavljacTableSortOrder();
    }

    private void reapplyDobavljacTableSortOrder() {

        ArrayList<TableColumn<Dobavljac, ?>> sortOrder = new ArrayList<>(tblDobavljaci.getSortOrder());
        tblDobavljaci.getSortOrder().clear();
        tblDobavljaci.getSortOrder().addAll(sortOrder);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void zatvori(ActionEvent event) {
        setOkClicked(false);
        this.stage.close();
    }

    @FXML
    private void dodajDobavljača(ActionEvent event) throws IOException {
        setModalDialog("/view/DodajDobavljaca.fxml");
        DodajDobavljacaController t = (DodajDobavljacaController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dodaj dobavljača");
        
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initDobavljac();
            tblDobavljaci.getSelectionModel().select(dobavljacAll.get(dobavljacAll.size() - 1));
            tblDobavljaci.layout();
            tblDobavljaci.scrollTo(dobavljacAll.get(dobavljacAll.size() - 1));
        }
    }

    @FXML
    private void izmeniDobavljaca(ActionEvent event) throws IOException {
        if (selectedDobavljac == null) {
            return;
        }
        setModalDialog("/view/DodajDobavljaca.fxml");
        DodajDobavljacaController t = (DodajDobavljacaController) fl.getController();
        t.setStage(mf);
        t.initialize(true, selectedDobavljac);
        mf.setTitle("Dodaj dobavljača");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initDobavljac();
            tblDobavljaci.getSelectionModel().select(dobavljacAll.get(dobavljacAll.size() - 1));
            tblDobavljaci.layout();
            tblDobavljaci.scrollTo(dobavljacAll.get(dobavljacAll.size() - 1));
        }
    }

    @FXML
    private void obrisiDobavljaca(ActionEvent event) {
        if (selectedDobavljac == null) {
            return;
        }

        AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Brisanje dobavljača",
                "Brisanje: " + selectedDobavljac.getNaziv(),
                "Da li ste sigurni?",
                AlertType.CONFIRMATION);
        if (abstractAlertDialog.getResult().get() != ButtonType.OK) {
            return;
        }

        selectedDobavljac.setIdDeleted(true);
        try {
            dobavljacJpa.edit(selectedDobavljac);
            initDobavljac();
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "obrisiDobavljaca()", this.getClass().getSimpleName());
        }
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba.",
                    Alert.AlertType.ERROR);
             LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    @FXML
    private void dodajLice(ActionEvent event) throws IOException {
        setModalDialog("/view/DodajOvlascenoLice.fxml");
        DodajOvlascenoLiceController t = (DodajOvlascenoLiceController) fl.getController();
        t.setStage(mf);
        t.initialize(false, selectedDobavljac);
        mf.setTitle("Dodaj ovlašteno lice");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initOvlascenoLice();
            updateFilteredOvlascenaLica();
            tblOvlascenaLica.getSelectionModel().select(licaFilter.get(licaFilter.size() - 1));
            tblOvlascenaLica.layout();
            tblOvlascenaLica.scrollTo(licaFilter.get(licaFilter.size() - 1));
            tblDobavljaci.getSelectionModel().select(selectedDobavljac);
        }
    }

    @FXML
    private void izmeniLice(ActionEvent event) throws IOException {
        if (selectedOvlascenoLice == null) {
            return;
        }
        setModalDialog("/view/DodajOvlascenoLice.fxml");
        DodajOvlascenoLiceController t = (DodajOvlascenoLiceController) fl.getController();
        t.setStage(mf);
        t.initialize(true, selectedOvlascenoLice);
        mf.setTitle("Izmeni ovlašteno lice");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initOvlascenoLice();
            updateFilteredOvlascenaLica();
            tblOvlascenaLica.getSelectionModel().select(licaFilter.get(licaFilter.size() - 1));
            tblOvlascenaLica.layout();
            tblOvlascenaLica.scrollTo(licaFilter.get(licaFilter.size() - 1));
            tblDobavljaci.getSelectionModel().select(selectedDobavljac);
            tblOvlascenaLica.getSelectionModel().select(selectedOvlascenoLice);
        }
    }

    @FXML
    private void obrisiLice(ActionEvent event) {
        if (selectedOvlascenoLice == null) {
            return;
        }

        AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Brisanje ovlašćenog lica",
                "Brisanje: " + selectedOvlascenoLice.getIme() + " " + selectedOvlascenoLice.getPrezime(),
                "Da li ste sigurni?",
                AlertType.CONFIRMATION);
        if (abstractAlertDialog.getResult().get() != ButtonType.OK) {
            return;
        }

        selectedOvlascenoLice.setIdDeleted(true);
        try {
            ovlascenoLiceJpa.edit(selectedOvlascenoLice);
            initOvlascenoLice();
            updateFilteredOvlascenaLica();
            tblDobavljaci.getSelectionModel().select(selectedDobavljac);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "obrisiLice()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void dodajKolekciju(ActionEvent event) throws IOException {
        setModalDialog("/view/DodajKolekciju.fxml");
        DodajKolekcijuController t = (DodajKolekcijuController) fl.getController();
        t.setStage(mf);
        t.initialize(false, selectedDobavljac);
        mf.setTitle("Dodaj kolekciju");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initKolekcija();
            updateFilteredKolekcija();
            tblKolekcije.getSelectionModel().select(kolekcijaFilter.get(kolekcijaFilter.size() - 1));
            tblKolekcije.layout();
            tblKolekcije.scrollTo(kolekcijaFilter.get(kolekcijaFilter.size() - 1));
            tblDobavljaci.getSelectionModel().select(selectedDobavljac);
        }
    }

    @FXML
    private void izmeniKolekciju(ActionEvent event) throws IOException {
        if (selectedKolekcija == null) {
            return;
        }
        setModalDialog("/view/DodajKolekciju.fxml");
        DodajKolekcijuController t = (DodajKolekcijuController) fl.getController();
        t.setStage(mf);
        t.initialize(true, selectedKolekcija);
        mf.setTitle("Izmeni kolekciju");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initKolekcija();
            updateFilteredKolekcija();
            tblKolekcije.getSelectionModel().select(kolekcijaFilter.get(kolekcijaFilter.size() - 1));
            tblKolekcije.layout();
            tblKolekcije.scrollTo(kolekcijaFilter.get(kolekcijaFilter.size() - 1));
            tblDobavljaci.getSelectionModel().select(selectedDobavljac);
        }
    }

    @FXML
    private void obrisiKolekciju(ActionEvent event) {
        if (selectedKolekcija == null) {
            return;
        }

        AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Brisanje kolekcije",
                "Brisanjem  " + selectedKolekcija.getNaziv() + "\nbrišete i sve artikle koji joj pripadaju.",
                "Da li ste sigurni?",
                AlertType.CONFIRMATION);
        if (abstractAlertDialog.getResult().get() != ButtonType.OK) {
            return;
        }

        Collection<Artikl> artikls = selectedKolekcija.getArtiklCollection();
        for (Artikl artikl : artikls) {
            artikl.setIdDeleted(true);
            try {
                artiklJpaController.edit(artikl);
            } catch (Exception e) {
                LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "obrisiKolekciju()", this.getClass().getSimpleName());
            }
        }

        selectedKolekcija.setIdDeleted(true);
        try {
            kolekcijaJpa.edit(selectedKolekcija);
            initKolekcija();
            updateFilteredKolekcija();
            tblDobavljaci.getSelectionModel().select(selectedDobavljac);

        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "obrisiKolekciju()", this.getClass().getSimpleName());
        }
    }

}
