/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Ponuda;
import entity.Porudzbina;
import entity.Uplata;
import java.math.BigDecimal;
import java.util.Date;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.PonudaJpaController;
import jpa.PorudzbinaJpaController;
import jpa.UplataJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajAvansController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxDodajAvans;
    @FXML
    private Label lblUkupno;
    @FXML
    private Label lblAvans;
    @FXML
    private Label lblSifraPorudzbine;

    private PonudaJpaController ponudaJpa;
    private UplataJpaController uplataJpaController;
    private Ponuda ponuda;
    private boolean valAvans;
    private BigDecimal avans;
    private boolean update;
    private Uplata uplata;
    private BigDecimal uplaceniAvans;
    private BigDecimal ukupnoUplaceno;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        ponudaJpa = new PonudaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        uplataJpaController = new UplataJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        if (update) {
            uplata = (Uplata) objToUpdate;
            ponuda = uplata.getIdPonuda();
            avans = uplata.getIznos();
            uplaceniAvans = ponuda.getAvans().subtract(uplata.getIznos());
        } else {
            ponuda = (Ponuda) objToUpdate;
            avans = BigDecimal.ZERO;
            uplaceniAvans = ponuda.getAvans();
        }
        initLabels();
        setAvansController();
        validationAvans();
    }

    private void initLabels() {
        lblSifraPorudzbine.setText(ponuda.getIdponuda().toString());
        lblUkupno.setText(ponuda.getUkupno().toString());
        lblAvans.setText(ponuda.getAvans().toString());
        tbxDodajAvans.setText(avans.toString());
    }

    public void validationAvans() {

        if (tbxDodajAvans.getText() == null || tbxDodajAvans.getText().trim().equals("")) {
            valAvans = false;
            avans = BigDecimal.ZERO;
            tbxDodajAvans.getStyleClass().removeAll("text-field");
            tbxDodajAvans.getStyleClass().add("text-field-error");
        } else {
            try {
                BigDecimal m = new BigDecimal(tbxDodajAvans.getText().replaceAll(",", ""));
                avans = m;
                if (avans.compareTo(BigDecimal.ZERO) == 0) {
                    valAvans = false;
                    tbxDodajAvans.getStyleClass().removeAll("text-field");
                    tbxDodajAvans.getStyleClass().add("text-field-error");
                } else {
                    valAvans = true;
                    tbxDodajAvans.getStyleClass().removeAll("text-field-error");
                    tbxDodajAvans.getStyleClass().add("text-field");
                }

            } catch (NumberFormatException nfe) {
                valAvans = false;
                tbxDodajAvans.getStyleClass().removeAll("text-field");
                tbxDodajAvans.getStyleClass().add("text-field-error");
            }
        }
    }

    private void setAvansController() {
        tbxDodajAvans.textProperty().addListener((observable, oldValue, newValue) -> {
            validationAvans();
        });
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        validationAvans();
        if (valAvans) {
            BigDecimal ukupniAvans = avans.add(uplaceniAvans);
            if (ukupniAvans.compareTo(ponuda.getUkupno()) == 1) {
                //prikaziAlert("Greška!", "Avans je veći od ukupne sume.", "Umanjite avans!");
                AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                        "Avans je veći od ukupne sume.",
                        "Umanjite avans!",
                        Alert.AlertType.ERROR);
                return;
            } else {
                ukupnoUplaceno = ukupniAvans;
                ponuda.setAvans(ukupnoUplaceno);
                if (ponuda.getAvans().compareTo(ponuda.getUkupno()) == 0) {
                    ponuda.setIsplacena("DA");
                }
                try {
                    ponudaJpa.edit(ponuda);
                } catch (Exception e) {
                    LoggerUtil.logError(e.getMessage(),
                            MainFrameController.korisnik.getKorisnickoIme(),
                            "sacuvaj()", this.getClass().getSimpleName());
                }

                if (!update) {
                    kreirajUplatu();
                } else {
                    izmeniUplatu();
                }

                okClicked = true;
                this.stage.close();

            }
        } else {
            // prikaziAlert("Greška!", "Nepravilno unet avans", "Avans mora biti unet kao decimalni broj!");
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Nepravilno unet avans.",
                    "Avans mora biti unet kao decimalni broj!",
                    Alert.AlertType.ERROR);
            return;
        }
    }

    private void kreirajUplatu() {
        Uplata uplata = new Uplata();
        uplata.setDatumUplate(new Date());
        uplata.setIdPonuda(ponuda);
        uplata.setIdDeleted(false);
        uplata.setIznos(avans);
        try {
            uplataJpaController.create(uplata);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajUplatu()", this.getClass().getSimpleName());
        }
    }

    private void izmeniUplatu() {
        uplata.setDatumUplate(new Date());
        uplata.setIznos(avans);
        try {
            uplataJpaController.edit(uplata);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniUplatu()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void odustani(ActionEvent event) {
        this.stage.close();
    }

    private void prikaziAlert(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();

        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/css/lawOfficeMetro.css").toExternalForm());
        dialogPane.getStyleClass().add("dialog-pane");
    }

    public Ponuda getPonuda() {
        return ponuda;
    }

    public Uplata getUplata() {
        return uplata;
    }

    public BigDecimal getUkupnoUplaceno() {
        return ukupnoUplaceno;
    }

}
