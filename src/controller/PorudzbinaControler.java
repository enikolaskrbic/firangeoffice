/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import static controller.MainFrameController.stilTabele;
import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import entity.Kupac;
import entity.Ponuda;
import entity.Porudzbina;
import entity.StavkaPonude;
import enums.StatusPonude;
import enums.StatusPorudzbine;
import enums.StilTabele;
import enums.TipPonude;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import jpa.PonudaJpaController;
import jpa.PorudzbinaJpaController;
import loger.LoggerUtil;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class PorudzbinaControler extends AbstracController {

    //stage
    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxFilter;
    @FXML
    private Label lblNaslov;
    @FXML
    private TableView<Porudzbina> tblPorudzbine;
    @FXML
    private TableColumn<Porudzbina, Long> tbcSifraPor;
    @FXML
    private TableColumn<Porudzbina, String> tbcDatumPor;
    @FXML
    private TableColumn<Porudzbina, String> tbcStatus;
    @FXML
    private TableColumn<Porudzbina, String> tbcNazivDobavljaca;
    @FXML
    private TableColumn<Porudzbina, String> tbcPibDobavljaca;
    @FXML
    private TableColumn<Porudzbina, String> tbcKorigovano;

    //lists
    private ObservableList<Porudzbina> porudzbinaAll = FXCollections.observableArrayList();
    private ObservableList<Porudzbina> porudzbinaFilter = FXCollections.observableArrayList();
    //temps
    private Ponuda ponuda;
    private Porudzbina selectedPorudzbina;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    //jpa
    private PorudzbinaJpaController porudzbinaJpaController;
    private PonudaJpaController ponudaJpaController;

    //popup menu
    final ContextMenu cmPonuda = new ContextMenu();
    private MenuItem cmItemPDF = new MenuItem("PDF");
    private MenuItem cmItemPotvrdi = new MenuItem("Potvrdi");
    private MenuItem cmItemKoriguj = new MenuItem("Koriguj");
    private boolean update;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        porudzbinaJpaController = new PorudzbinaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        ponudaJpaController = new PonudaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        if (objToUpdate instanceof Ponuda) {
            this.ponuda = (Ponuda) objToUpdate;
        }
        if (ponuda != null) {
            init();
        } else {
            initAll();
        }
    }

    private void initAll() {
        getPorudzbina();
        initPorudzbinaTabelController();
        initPorudzbinaTable();
        initPopupMenu();
    }

    private void init() {
        getPorudzbinaByPonuda();
        initPorudzbinaTabelController();
        initPorudzbinaTable();
        initPopupMenu();
    }

    private void initPopupMenu() {
        cmPonuda.getItems().add(cmItemPDF);
        cmPonuda.getItems().add(cmItemPotvrdi);
        cmPonuda.getItems().add(cmItemKoriguj);

        cmItemPDF.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPorudzbina != null) {
                    try {
//                        Class.forName("com.mysql.jdbc.Driver");
//                        Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/firangeoffice", "root", "admin");

                        //String jasperFileName = "D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\Porudzbina.jasper";
                        String jrxmlFileName = MainFrameController.porudzbinaJasperPath; //"D:\\netbeansWorkspace\\firangeOffice\\src\\reports\\Porudzbina.jrxml";
                        //JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
                        JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlFileName);
                        // JasperCompileManager.compileReportToFile("D:/netbeansWorkspace/firangeOffice/src/reports/porudzbina.jrxml", "D:/netbeansWorkspace/firangeOffice/src/reports/porudzbina.jrxml");
                        Map map = new HashMap();
                        map.put("idPor", selectedPorudzbina.getIdPorudzbina());

                        JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, map, MainFrameController.conn);

                        // JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, conn);
                        JasperViewer.viewReport(jprint, false);

                    } catch (Exception e) {
                        LoggerUtil.logError(e.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "initPopupMenu()", this.getClass().getSimpleName());
                    }
                }

            }
        });

        cmItemKoriguj.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPorudzbina != null) {
                    try {
                        setModalDialog("/view/Korekcija.fxml");
                    } catch (IOException ex) {
                        LoggerUtil.logError(ex.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "initPopupMenu()", this.getClass().getSimpleName());
                    }
                    KorekcijaController t1 = (KorekcijaController) fl.getController();
                    t1.setStage(mf);
                    t1.initialize(false, selectedPorudzbina);
                    mf.setTitle("Korekcija");
                    mf.getIcons().add(new Image("/images/firangeIcon.png"));
                    mf.setScene(scene);
                    mf.showAndWait();
                    if (t1.isOkClicked()) {
                        if (update) {
                            init();
                        } else {
                            initAll();
                        }
                    }
                }

            }
        });
        cmItemPotvrdi.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (selectedPorudzbina != null) {
                    selectedPorudzbina.setStatus(StatusPorudzbine.PRIHVACENA.toString());
                    try {
                        porudzbinaJpaController.edit(selectedPorudzbina);
                    } catch (Exception e) {
                        LoggerUtil.logError(e.getMessage(),
                                MainFrameController.korisnik.getKorisnickoIme(),
                                "initPopupMenu()", this.getClass().getSimpleName());
                    }
                    ponuda = ponudaJpaController.findPonuda(selectedPorudzbina.getPonudaidponuda().getIdponuda());
                    if (update) {
                        init();
                    } else {
                        initAll();
                    }
                    boolean updatePonuda = true;
                    for (Porudzbina porudzbina : ponuda.getPorudzbinaCollection()) {
                        if (porudzbina.getStatus().equals(StatusPorudzbine.NA_CEKANJU.toString())) {
                            updatePonuda = false;
                            break;
                        }
                    }
                    if (updatePonuda) {
                        try {
                            ponuda.setStatus(StatusPonude.DOSTAVLJA_SE.toString());
                            ponudaJpaController.edit(ponuda);
                        } catch (Exception e) {
                            LoggerUtil.logError(e.getMessage(),
                                    MainFrameController.korisnik.getKorisnickoIme(),
                                    "initPopupMenu()", this.getClass().getSimpleName());
                        }
                    }
                    okClicked = true;
                }

            }
        });
    }

    private void getPorudzbinaByPonuda() {
        porudzbinaAll.clear();
        porudzbinaFilter.clear();
        try {
            List<Porudzbina> list = porudzbinaJpaController.findPorudzbinaEntities();
            for (Porudzbina porudzbina : list) {
                if (porudzbina.getPonudaidponuda().getIdponuda() == ponuda.getIdponuda()) {
                    porudzbinaAll.add(porudzbina);
                }
            }
            //porudzbinaAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje porudžbina iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getPorudzbinaByPonuda()", this.getClass().getSimpleName());
        }
    }

    private void getPorudzbina() {
        porudzbinaAll.clear();
        porudzbinaFilter.clear();
        try {
            List<Porudzbina> list = porudzbinaJpaController.findPorudzbinaEntities();
            porudzbinaAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje porudžbina iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getPorudzbina()", this.getClass().getSimpleName());
        }
    }

    private void initPorudzbinaTabelController() {
        porudzbinaFilter.addAll(porudzbinaAll);
        tbxFilter.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {

                updateFilteredPorudzbina();
            }
        });

        tblPorudzbine.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label

                selectedPorudzbina = tblPorudzbine.getSelectionModel().getSelectedItem();

                tblPorudzbine.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {

                        if (event.getButton() == MouseButton.SECONDARY) {
                            if (selectedPorudzbina != null) {

                                if (selectedPorudzbina.getPonudaidponuda().getStatus().equals(StatusPonude.DOBAVLJAC_ODBIO.toString())
                                        || selectedPorudzbina.getPonudaidponuda().getStatus().equals(StatusPonude.KUPAC_ODBIO.toString())
                                        || selectedPorudzbina.getPonudaidponuda().getStatus().equals(StatusPonude.STORNIRANO.toString())) {
                                    cmItemKoriguj.setVisible(false);
                                    cmItemPotvrdi.setVisible(false);

                                } else {
                                    cmItemKoriguj.setVisible(true);
                                    if (selectedPorudzbina.getStatus().equals(StatusPorudzbine.NA_CEKANJU.toString())) {
                                        cmItemPotvrdi.setVisible(true);
                                    } else {
                                        cmItemPotvrdi.setVisible(false);
                                    }
                                }
                                cmPonuda.show(stage, event.getScreenX(), event.getScreenY());
                            }
                        }

                    }

                });

            }
        });

    }

    private void initPorudzbinaTable() {
        tbcSifraPor.setCellValueFactory(new PropertyValueFactory<Porudzbina, Long>("idPorudzbina"));
        //tbcDatumPor.setCellValueFactory(new PropertyValueFactory<Porudzbina, DateTime>("datumKreiranja"));
        tbcStatus.setCellValueFactory(new PropertyValueFactory<Porudzbina, String>("status"));
        tbcNazivDobavljaca.setCellValueFactory(new PropertyValueFactory<Porudzbina, String>("dobavljacNaziv"));
        tbcPibDobavljaca.setCellValueFactory(new PropertyValueFactory<Porudzbina, String>("dobavljacPib"));
        tbcKorigovano.setCellValueFactory(cellData -> {
            Boolean korekcija = cellData.getValue().getKorekcija();
            String genderAsString;
            if (korekcija == true) {
                genderAsString = "DA";
            } else {
                genderAsString = "NE";
            }

            return new ReadOnlyStringWrapper(genderAsString);
        });

        tbcDatumPor.setCellValueFactory(cellData -> {
            Date datetime = cellData.getValue().getDatumKreiranja();
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy.");
            String ret = "";
            try {
                ret = dateFormat.format(datetime);
            } catch (Exception e) {
                LoggerUtil.logError(e.getMessage(),
                        MainFrameController.korisnik.getKorisnickoIme(),
                        "initPorudzbinaTable()", this.getClass().getSimpleName());
            }

            return new ReadOnlyStringWrapper(ret);
        });
        tbcStatus.setCellFactory(new Callback<TableColumn<Porudzbina, String>, TableCell<Porudzbina, String>>() {
            @Override
            public TableCell<Porudzbina, String> call(TableColumn<Porudzbina, String> p) {
                return new TableCell<Porudzbina, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        for (StatusPorudzbine dir : StatusPorudzbine.values()) {
                            this.getTableRow().getStyleClass().remove(dir.toString());
                            getStyleClass().remove(dir.toString());
                        }
                        if (!isEmpty()) {
                            if (MainFrameController.stilTabele.equals(StilTabele.BOJA_REDA)) {
                                this.getTableRow().getStyleClass().add(item);
                            } else if (MainFrameController.stilTabele.equals(StilTabele.BOJA_CELIJE)) {
                                getStyleClass().add(item);
                            }
                        }
                        setText(item);

                    }
                };
            }
        });
        tblPorudzbine.setItems(porudzbinaFilter);
    }

    @Override

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    private void updateFilteredPorudzbina() {

        porudzbinaFilter.clear();

        for (Porudzbina s : porudzbinaAll) {
            String korekcija = s.getKorekcija() ? "DA" : "NE";
            if (s.getDobavljacNaziv().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getDobavljacPib().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || sdf.format(s.getDatumKreiranja()).toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || korekcija.toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getStatus().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                porudzbinaFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyArtiklTableSortOrder();
    }

    private void reapplyArtiklTableSortOrder() {

        ArrayList<TableColumn<Porudzbina, ?>> sortOrder = new ArrayList<>(tblPorudzbine.getSortOrder());
        tblPorudzbine.getSortOrder().clear();
        tblPorudzbine.getSortOrder().addAll(sortOrder);
    }

}
