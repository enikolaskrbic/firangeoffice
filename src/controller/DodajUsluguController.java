/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import enums.TipArtikla;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajUsluguController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;
    
    @FXML
    private TextField tbxSifra;
    @FXML
    private TextField tbxNaziv;
    @FXML
    private TextField tbxCena;
    private Artikl artikl;
    private boolean valCena;
    private BigDecimal cena;
    private boolean update;
    private ArtiklJpaController artiklJpa;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        artiklJpa = new ArtiklJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        okClicked=false;
        this.update = update;
        if(update){
            artikl = (Artikl)objToUpdate;
            initLabel();
        }
        setCenaController();
        validationCena();
        
    }
    
    
    
    private void initLabel(){
        tbxSifra.setText(artikl.getSifra());
        tbxNaziv.setText(artikl.getNaziv());
        tbxCena.setText(artikl.getCena().toString());
    }

    public void validationCena() {

        if (tbxCena.getText() == null || tbxCena.getText().trim().equals("")) {
            valCena = false;
            cena = BigDecimal.ZERO;
            tbxCena.getStyleClass().removeAll("text-field");
            tbxCena.getStyleClass().add("text-field-error");
        } else {
            try {
                BigDecimal m = new BigDecimal(tbxCena.getText().replaceAll(",", ""));
                cena = m;
                if (cena.compareTo(BigDecimal.ZERO) == 0) {
                    valCena = false;
                    tbxCena.getStyleClass().removeAll("text-field");
                    tbxCena.getStyleClass().add("text-field-error");
                } else {
                    valCena = true;
                    tbxCena.getStyleClass().removeAll("text-field-error");
                    tbxCena.getStyleClass().add("text-field");
                }

            } catch (NumberFormatException nfe) {
                valCena = false;
                tbxCena.getStyleClass().removeAll("text-field");
                tbxCena.getStyleClass().add("text-field-error");
            }
        }
    }
    
    private void setCenaController() {
        tbxCena.textProperty().addListener((observable, oldValue, newValue) -> {
            validationCena();
        });
    }
    
    private boolean validation(){
        if(tbxNaziv.getText().equals("") ||
                tbxSifra.getText().equals("") ||
                tbxCena.getText().equals("")){
            return false;
        }
        validationCena();
        
        return valCena;
    }
    
    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        if(validation()){
            if(update){
                izmeniUslugu();
            }else{
                kreirajUslugu();
            }
            
        }
    }
    
    private void izmeniUslugu(){
        artikl.setSifra(tbxSifra.getText());
        artikl.setNaziv(tbxNaziv.getText());
        artikl.setCena(cena);
        artikl.setIdDeleted(false);
        try {
            artiklJpa.edit(artikl);
        } catch (Exception e) {
           LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniUslugu()", this.getClass().getSimpleName());
        }
        okClicked =true;
        this.stage.close();
    }

    private void kreirajUslugu(){
        artikl = new Artikl();
        artikl.setSifra(tbxSifra.getText());
        artikl.setNaziv(tbxNaziv.getText());
        artikl.setCena(cena);
        artikl.setStatus(TipArtikla.USLUGA.toString());
        artikl.setTip(TipArtikla.USLUGA.toString());
        artikl.setIdDeleted(false);
        try {
            artiklJpa.create(artikl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        okClicked=true;
        this.stage.close();
    }
    @FXML
    private void odustani(ActionEvent event) {
        this.stage.close();
    }
    
    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

   
    
}
