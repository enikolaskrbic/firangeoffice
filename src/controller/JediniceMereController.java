/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import entity.JedinicaMere;
import entity.Kupac;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import jpa.JedinicaMereJpaController;
import jpa.KupacJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class JediniceMereController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxFilter;
    @FXML
    private Label lblNaslov;
    @FXML
    private TableView<JedinicaMere> tblJedinicaMere;
    @FXML
    private TableColumn<JedinicaMere, String> tbcNaziv;
    @FXML
    private TableColumn<JedinicaMere, String> tbcSkraceniNaziv;
    @FXML
    private Button btnJedinicu;
    @FXML
    private Button btnIzmeniJedinicu;
    @FXML
    private Button btnObrisiJedinicu;

    private JedinicaMere jedinicaMere;
    private JedinicaMereJpaController jedinicaMereJpaController;
     private ArtiklJpaController artiklJpaController;
    private ObservableList<JedinicaMere> jedinicaMereAll = FXCollections.observableArrayList();
    private ObservableList<JedinicaMere> jedinicaMereFilter = FXCollections.observableArrayList();

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        jedinicaMereJpaController = new JedinicaMereJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        artiklJpaController = new ArtiklJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        initJedinicaMere();
        btnIzmeniJedinicu.setDisable(true);
        btnObrisiJedinicu.setDisable(true);
    }

    private void initJedinicaMere() {
        getJedinicaMere();
        jedinicaMereTableController();
        initTabel();
    }

    private void getJedinicaMere() {
        jedinicaMereAll.clear();
        jedinicaMereFilter.clear();
        try {
            List<JedinicaMere> list = jedinicaMereJpaController.findJedinicaMereByDeleted(false);
            jedinicaMereAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje jedinice mere iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
             LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(), 
                    "getJedinicaMere()", this.getClass().getSimpleName());
        }
    }

    private void initTabel() {
        tbcNaziv.setCellValueFactory(new PropertyValueFactory<JedinicaMere, String>("naziv"));
        tbcSkraceniNaziv.setCellValueFactory(new PropertyValueFactory<JedinicaMere, String>("skraceniNaziv"));
        tblJedinicaMere.setItems(jedinicaMereFilter);
    }

    private void jedinicaMereTableController() {

        jedinicaMereFilter.addAll(jedinicaMereAll);
        tbxFilter.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                updateFilteredKupac();
            }
        });
        tblJedinicaMere.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if (tblJedinicaMere.getSelectionModel().getSelectedItem() != null) {
                    jedinicaMere = tblJedinicaMere.getSelectionModel().getSelectedItem();
                    btnIzmeniJedinicu.setDisable(false);
                   
                    btnObrisiJedinicu.setDisable(false);
                } else {
                    jedinicaMere = null;
                    btnIzmeniJedinicu.setDisable(true);
                    btnObrisiJedinicu.setDisable(true);

                }
            }
        });

    }
    
    private void updateFilteredKupac() {

        jedinicaMereFilter.clear();

        for (JedinicaMere s : jedinicaMereAll) {
            if (s.getNaziv().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getSkraceniNaziv().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    ) {
                jedinicaMereFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyPonudaTableSortOrder();
    }

    private void reapplyPonudaTableSortOrder() {

        ArrayList<TableColumn<JedinicaMere, ?>> sortOrder = new ArrayList<>(tblJedinicaMere.getSortOrder());
        tblJedinicaMere.getSortOrder().clear();
        tblJedinicaMere.getSortOrder().addAll(sortOrder);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    @FXML
    private void dodajJedinicu(ActionEvent event) throws IOException {
        setModalDialog("/view/DodajJedinicuMere.fxml");
        DodajJedinicuMereController t = (DodajJedinicuMereController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dodaj jedinicu mere");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initJedinicaMere();
        }
    }

    @FXML
    private void izmeniJedinicu(ActionEvent event) throws IOException {
        if(jedinicaMere==null)
            return;
        setModalDialog("/view/DodajJedinicuMere.fxml");
        DodajJedinicuMereController t = (DodajJedinicuMereController) fl.getController();
        t.setStage(mf);
        t.initialize(true, jedinicaMere);
        mf.setTitle("Izmeni jedinicu mere");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initJedinicaMere();
        }
    }

    @FXML
    private void obrisiJedinicu(ActionEvent event) {
        if(jedinicaMere==null)
            return;
        AbstractAlertDialog a= new AbstractAlertDialog("Upozorenje", "Da li ste sigurni?", "Prilikom birsanja jedinice mere\nbrišete sve artikle sa ovo jedinicom mere.", Alert.AlertType.CONFIRMATION);
       if (a.getResult().get() != ButtonType.OK) {
            return;
        }
       Collection<Artikl> artikls = jedinicaMere.getArtiklCollection();
       for (Artikl artikl : artikls) {
            artikl.setIdDeleted(true);
            try {
                artiklJpaController.edit(artikl);
            } catch (Exception e) {
                LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "obrisiJedinicu()", this.getClass().getSimpleName());
            }
        }
       jedinicaMere.setIdDeleted(true);
        try {
            jedinicaMereJpaController.edit(jedinicaMere);
            initJedinicaMere();
        } catch (Exception e) {
             LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "obrisiJedinicu()", this.getClass().getSimpleName());
        }
    }

}
