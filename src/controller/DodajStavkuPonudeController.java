/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import entity.Dobavljac;
import entity.JedinicaMere;
import entity.Kolekcija;
import entity.StavkaPonude;
import enums.TipArtikla;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import jpa.DobavljacJpaController;
import jpa.JedinicaMereJpaController;
import jpa.KolekcijaJpaController;
import jpa.exceptions.NonexistentEntityException;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajStavkuPonudeController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private Label lblPdvStopa;
    @FXML
    private TextField tbxKolicina;
    @FXML
    private CheckBox chxPdv;
    @FXML
    private Label lblIznos;
    @FXML
    private TextField tbxSifraArtikla;
    @FXML
    private TextField tbxNazivArtikla;
    @FXML
    private TextField tbxCena;
    @FXML
    private ComboBox<Dobavljac> cbxDobavljac;
    @FXML
    private ComboBox<Kolekcija> cbxKolekcija;
    @FXML
    private ComboBox<JedinicaMere> cbxJedinicaMere;
    @FXML
    private TextField tbxRabat;

    private boolean valKolicina;
    private boolean valDostavljac;
    private boolean valKolekcija;
    private boolean valCena;
    private boolean valRabat;
    private boolean checkPdv;
    private boolean valJedinicaMere;
    private BigDecimal kolicina;
    private BigDecimal cena;
    private BigDecimal cenaSaRabatom;
    private BigDecimal poreskaOsnovica;
    private BigDecimal poreskaStopa;
    private BigDecimal iznosPDV;
    private BigDecimal rabat;
    private BigDecimal rabatStopa;

    private BigDecimal iznos;

    private Artikl artikl;
    private StavkaPonude stavka;
    private boolean addArticle = false;
    private Dobavljac selectedDobavljac;
    private Kolekcija selectedKolekcija;
    private JedinicaMere selectedJedinicaMere;
    private DecimalFormat df = new DecimalFormat("#,###.00");

    //jpa kontrolers
    private DobavljacJpaController dobavljacJpaController;
    private KolekcijaJpaController kolekcijaJpaController;
    private JedinicaMereJpaController jedinicaMereJpaController;
    private ArtiklJpaController artiklJpaController;

    //lists 
    private ObservableList<Dobavljac> dobavljacAll = FXCollections.observableArrayList();
    private ObservableList<JedinicaMere> jedinicaMereAll = FXCollections.observableArrayList();
    private ObservableList<Kolekcija> kolekcijaAll = FXCollections.observableArrayList();
    private ObservableList<Kolekcija> kolekcijaFilter = FXCollections.observableArrayList();

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        dobavljacJpaController = new DobavljacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        kolekcijaJpaController = new KolekcijaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        jedinicaMereJpaController = new JedinicaMereJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        artiklJpaController = new ArtiklJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        initVariable();
        if (objToUpdate instanceof StavkaPonude) {
            stavka = (StavkaPonude) objToUpdate;
            artikl = stavka.getArtikl();
            kolicina = stavka.getKolicina();
            cena = stavka.getCenaBezRabat();
            cenaSaRabatom = stavka.getCenaRabat();

            rabatStopa = stavka.getRabatStopa();
            poreskaOsnovica = stavka.getPoreskaOsonovica();
            poreskaStopa = stavka.getPoreskaStopa();
            iznosPDV = stavka.getPdv();
            iznos = stavka.getIznos();
            if (artikl.getTip().equals(TipArtikla.ROBA.toString())) {
                selectedDobavljac = stavka.getArtikl().getKolekcijaIdkolekcija().getDobavljac();
                selectedKolekcija = stavka.getArtikl().getKolekcijaIdkolekcija();
                selectedJedinicaMere = stavka.getArtikl().getJedinicaMereidJedinicaMere();
            }

        } else if (objToUpdate instanceof Artikl) {
            artikl = (Artikl) objToUpdate;

            if (artikl.getTip().equals(TipArtikla.ROBA.toString())) {
                selectedJedinicaMere = artikl.getJedinicaMereidJedinicaMere();
                selectedDobavljac = artikl.getKolekcijaIdkolekcija().getDobavljac();
                selectedKolekcija = artikl.getKolekcijaIdkolekcija();
            }

            cena = artikl.getCena();
            kolicina = BigDecimal.ONE;

        }

        initCombo();
        if (artikl != null) {
            if (stavka == null) {
                initLabelsArtikl(artikl);
            } else {
                initLabelStavka();
            }
        } else {
            addArticle = true;
        }
        changeTbxLisener();
        validationCena();
        validationKolicina();
        validationRabat();
        izracunajCenu();
        chxPdv.setSelected(true);
        chxPdv.setDisable(true);
    }

    private void disableLabels() {
        tbxSifraArtikla.setDisable(true);
        tbxNazivArtikla.setDisable(true);
        //tbxCena.setDisable(true);
        cbxDobavljac.setDisable(true);
        cbxJedinicaMere.setDisable(true);
        cbxKolekcija.setDisable(true);
    }

    private void initLabelStavka() {
        tbxSifraArtikla.setText(artikl.getSifra());
        tbxNazivArtikla.setText(artikl.getNaziv());
        tbxCena.setText(df.format(artikl.getCena()));
        tbxKolicina.setText(df.format(stavka.getKolicina()));
        tbxRabat.setText(df.format(stavka.getRabatStopa()));
        if (stavka.getPdv().compareTo(BigDecimal.ZERO) != 0) {
            chxPdv.setSelected(true);
        }
        setCombo();
        disableLabels();
    }

    private void initLabelsArtikl(Artikl a) {
        tbxSifraArtikla.setText(a.getSifra());
        tbxNazivArtikla.setText(a.getNaziv());
        tbxCena.setText(df.format(a.getCena()));
        lblIznos.setText("0.00");
        setCombo();
        disableLabels();
    }

    private void setCombo() {
//        for (Dobavljac dobavljac : dobavljacAll) {
//            if(dobavljac.getIddobavljac()==selectedDobavljac.getIddobavljac()){
//                cbxDobavljac.getSelectionModel().select(dobavljac);
//            }
//        }
        cbxDobavljac.getSelectionModel().select(selectedDobavljac);
        cbxKolekcija.getSelectionModel().select(selectedKolekcija);
        cbxJedinicaMere.getSelectionModel().select(selectedJedinicaMere);
        valKolekcija = true;
        valDostavljac = true;
        valJedinicaMere = true;
    }

    private void initCombo() {
        getDobavljac();
        getKolekcija();
        getJedinicaMere();
        cbxDobavljac.setItems(dobavljacAll);
        cbxJedinicaMere.setItems(jedinicaMereAll);
    }

    private void getDobavljac() {
        dobavljacAll.clear();
        try {
            List<Dobavljac> list = dobavljacJpaController.findDobavljacByDeleted(false);
            dobavljacAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje dobavljača iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getDobavljac()", this.getClass().getSimpleName());
        }
    }

    private void getKolekcija() {
        kolekcijaAll.clear();
        try {
            List<Kolekcija> list = kolekcijaJpaController.findKolekcijaByDeleted(false);
            kolekcijaAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje kolekcija iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getKolekcija()", this.getClass().getSimpleName());
        }
    }

    private void getJedinicaMere() {
        jedinicaMereAll.clear();
        try {
            List<JedinicaMere> list = jedinicaMereJpaController.findJedinicaMereByDeleted(false);
            jedinicaMereAll = FXCollections.observableList(list);
        } catch (Exception e) {
           AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje jedinice mere iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
           LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getJedinicaMere()", this.getClass().getSimpleName());
        }
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;

    }

    private void initVariable() {
        cena = BigDecimal.ZERO;
        kolicina = BigDecimal.ZERO;
        cenaSaRabatom = BigDecimal.ZERO;
        rabat = BigDecimal.ZERO;
        rabatStopa = BigDecimal.ZERO;
        poreskaOsnovica = BigDecimal.ZERO;
        poreskaStopa = BigDecimal.ZERO;
        iznosPDV = BigDecimal.ZERO;
        iznos = BigDecimal.ZERO;
    }

    private void scaleFields() {
        cena = cena.setScale(2, RoundingMode.CEILING);
        kolicina = kolicina.setScale(2, RoundingMode.CEILING);
        cenaSaRabatom = cenaSaRabatom.setScale(2, RoundingMode.CEILING);
        rabat = rabat.setScale(2, RoundingMode.CEILING);
        rabatStopa = rabatStopa.setScale(2, RoundingMode.CEILING);
        poreskaOsnovica = poreskaOsnovica.setScale(2, RoundingMode.CEILING);
        poreskaStopa = poreskaStopa.setScale(2, RoundingMode.CEILING);
        iznosPDV = iznosPDV.setScale(2, RoundingMode.CEILING);
        iznos = iznos.setScale(2, RoundingMode.CEILING);
    }

    public void validationKolicina() {

        if (tbxKolicina.getText() == null || tbxKolicina.getText().trim().equals("")) {
            valKolicina = false;
            //kolicina = BigDecimal.ONE;
            tbxKolicina.getStyleClass().removeAll("text-field");
            tbxKolicina.getStyleClass().add("text-field-error");
        } else {
            try {
                BigDecimal m = new BigDecimal(tbxKolicina.getText().replaceAll(",", ""));
                kolicina = m;
                if (kolicina.compareTo(BigDecimal.ZERO) != 1) {
                    valKolicina = false;
                    tbxKolicina.getStyleClass().removeAll("text-field");
                    tbxKolicina.getStyleClass().add("text-field-error");
                } else {
                    valKolicina = true;
                    tbxKolicina.getStyleClass().removeAll("text-field-error");
                    tbxKolicina.getStyleClass().add("text-field");
                }
            } catch (NumberFormatException nfe) {
                valKolicina = false;
                tbxKolicina.getStyleClass().removeAll("text-field");
                tbxKolicina.getStyleClass().add("text-field-error");
            }
        }
    }

    public void validationCena() {

        if (tbxCena.getText() == null || tbxCena.getText().trim().equals("")) {
            valCena = false;
            //kolicina = BigDecimal.ONE;
            tbxCena.getStyleClass().removeAll("text-field");
            tbxCena.getStyleClass().add("text-field-error");
        } else {
            try {
                BigDecimal m = new BigDecimal(tbxCena.getText().replaceAll(",", ""));
                cena = m;
                if (cena.compareTo(BigDecimal.ZERO) != 1) {
                    valCena = false;
                    tbxCena.getStyleClass().removeAll("text-field");
                    tbxCena.getStyleClass().add("text-field-error");
                } else {
                    valCena = true;
                    tbxCena.getStyleClass().removeAll("text-field-error");
                    tbxCena.getStyleClass().add("text-field");
                }

            } catch (NumberFormatException nfe) {
                valCena = false;
                tbxCena.getStyleClass().removeAll("text-field");
                tbxCena.getStyleClass().add("text-field-error");
            }
        }
    }

    public void validationRabat() {

        if (tbxRabat.getText() == null || tbxRabat.getText().trim().equals("")) {
            valRabat = true;
            rabatStopa = BigDecimal.ZERO;
            tbxRabat.getStyleClass().removeAll("text-field-error");
            tbxRabat.getStyleClass().add("text-field");
        } else {
            try {
                BigDecimal m = new BigDecimal(tbxRabat.getText().replaceAll(",", ""));
                rabatStopa = m;
                BigDecimal sto = new BigDecimal(100);
                if (rabatStopa.compareTo(BigDecimal.ZERO) == -1 || sto.compareTo(rabatStopa) != 1) {
                    valRabat = false;
                    tbxRabat.getStyleClass().removeAll("text-field");
                    tbxRabat.getStyleClass().add("text-field-error");
                } else {
                    valRabat = true;
                    tbxRabat.getStyleClass().removeAll("text-field-error");
                    tbxRabat.getStyleClass().add("text-field");
                }
            } catch (NumberFormatException nfe) {
                valRabat = false;
                tbxRabat.getStyleClass().removeAll("text-field");
                tbxRabat.getStyleClass().add("text-field-error");
            }
        }
    }

    private void changeTbxLisener() {
        tbxKolicina.textProperty().addListener((observable, oldValue, newValue) -> {
            //System.out.println("textfield changed from " + oldValue + " to " + newValue);
            validateAll();
            izracunajCenu();

        });

        tbxCena.textProperty().addListener((observable, oldValue, newValue) -> {
            //System.out.println("textfield changed from " + oldValue + " to " + newValue);
            validateAll();
            izracunajCenu();

        });

        tbxRabat.textProperty().addListener((observable, oldValue, newValue) -> {
            validateAll();
            izracunajCenu();

        });

        chxPdv.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                validateAll();
                izracunajCenu();
            }
        });

        cbxDobavljac.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                selectedDobavljac = (Dobavljac) t1;
                selectedKolekcija = null;
                cbxKolekcija.setItems(FXCollections.observableArrayList());
                valKolekcija = false;
                if (selectedDobavljac != null) {
                    filterKolekcija();
                    cbxKolekcija.setItems(kolekcijaFilter);
                }
            }
        });
        cbxKolekcija.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                selectedKolekcija = (Kolekcija) t1;
                if (selectedKolekcija != null) {
                    valKolekcija = true;
                } else {
                    valKolekcija = false;
                }
            }
        });

        cbxJedinicaMere.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                selectedJedinicaMere = (JedinicaMere) t1;
                if (selectedJedinicaMere != null) {
                    valJedinicaMere = true;
                } else {
                    valJedinicaMere = false;
                }
            }
        });

    }

    private void filterKolekcija() {
        kolekcijaFilter.clear();
        for (Kolekcija kolekcija : kolekcijaAll) {
            if (kolekcija.getDobavljac().getIddobavljac() == selectedDobavljac.getIddobavljac()) {
                kolekcijaFilter.add(kolekcija);
            }
        }
    }

    private void validateAll() {
        validationKolicina();
        validationCena();
        validationRabat();
    }

    private void izracunajCenu() {
        if (!valCena || !valKolicina || !valRabat) {
            lblIznos.setText("0.00");
            return;
        }
        rabat = rabatStopa.divide(new BigDecimal(100));
        cenaSaRabatom = cena.subtract(cena.multiply(rabat));
        poreskaOsnovica = cenaSaRabatom.multiply(kolicina);
        iznosPDV = BigDecimal.ZERO;
//        if (chxPdv.isSelected()) {
//            iznosPDV = poreskaOsnovica.multiply(new BigDecimal(0.2));
//        }
        iznos = poreskaOsnovica.add(iznosPDV);
        //format ispisa iznosa
        scaleFields();
        DecimalFormat df = new DecimalFormat("#,###.00");
        lblIznos.setText(df.format(iznos));

    }

    private void createArtikl() {
        artikl = new Artikl();
        artikl.setCena(cena);
        artikl.setNaziv(tbxNazivArtikla.getText());
        artikl.setSifra(tbxSifraArtikla.getText());
        artikl.setStatus(TipArtikla.ROBA.toString());
        artikl.setTip(TipArtikla.ROBA.toString());
        artikl.setKolekcijaIdkolekcija(selectedKolekcija);
        artikl.setJedinicaMereidJedinicaMere(selectedJedinicaMere);
        artikl.setIdDeleted(false);
        artiklJpaController.create(artikl);

    }
    
    private void updateArtikl() {
        artikl.setCena(cena);
        try {
            artiklJpaController.edit(artikl);
        } catch (NonexistentEntityException ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "updateArtikl()", this.getClass().getSimpleName());
        } catch (Exception ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "updateArtikl()", this.getClass().getSimpleName());
        }
    }

    private void createStavka() {
        if (stavka == null) {
            stavka = new StavkaPonude();
        }
        stavka.setSifraStavke(artikl.getSifra());
        stavka.setNazivStavke(artikl.getNaziv());
        stavka.setKolicina(kolicina);
        stavka.setCenaBezRabat(cena);
        stavka.setCenaRabat(cenaSaRabatom);
        stavka.setRabatStopa(rabatStopa);
        stavka.setPoreskaOsonovica(poreskaOsnovica);
        if (iznosPDV.compareTo(BigDecimal.ZERO) == 0) {
            stavka.setPoreskaStopa(BigDecimal.ZERO);
        } else {
            stavka.setPoreskaStopa(new BigDecimal(20));
        }

        stavka.setPdv(iznosPDV);
        stavka.setIznos(iznos);
        if (artikl.getTip().equals(TipArtikla.ROBA.toString())) {
            stavka.setJedMere(artikl.getJedinicaMereidJedinicaMere().getNaziv());
            stavka.setJedMereSkraceno(artikl.getJedinicaMereidJedinicaMere().getSkraceniNaziv());
        }

        stavka.setIdDeleted(false);
    }

    public Artikl getArtikl() {
        return artikl;
    }

    public StavkaPonude getStavkaPonude() {
        return stavka;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        validateAll();
        if (valKolicina && valCena && valRabat && valKolekcija && valJedinicaMere) {
            if (addArticle) {
                createArtikl();
            }else{
                updateArtikl();
            }
            createStavka();
            setOkClicked(true);
            this.stage.close();
        }
    }

    @FXML
    private void odustani(ActionEvent event) {
        setOkClicked(false);
        this.stage.close();
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {

            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    public BigDecimal getKolicina() {
        return kolicina;
    }

}
