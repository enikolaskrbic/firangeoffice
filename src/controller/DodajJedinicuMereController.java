/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.JedinicaMere;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.JedinicaMereJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajJedinicuMereController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxNaziv;
    @FXML
    private TextField tbxSkraceniNaziv;

    private JedinicaMere jedinicaMere;
    private boolean update;
    private JedinicaMereJpaController jedinicaMereJpaController;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        jedinicaMereJpaController = new JedinicaMereJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        okClicked = false;
        if (update) {
            jedinicaMere = (JedinicaMere) objToUpdate;
            initLabel();
        }
    }

    private void initLabel() {
        tbxNaziv.setText(jedinicaMere.getNaziv());
        tbxSkraceniNaziv.setText(jedinicaMere.getSkraceniNaziv());
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private boolean validate() {
        if (tbxNaziv.getText().equals("")
                || tbxSkraceniNaziv.getText().equals("")) {
            return false;
        }
        return true;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        if (validate()) {
            if (update) {
                izmeniJedinicu();
            } else {
                kreirajJedinicu();
            }
            okClicked = true;
            this.stage.close();
        } else {
            //AbstractAlertDialog a= new AbstractAlertDialog("Greška", "Greška unosa!", "Morate uneti sva polja.", Alert.AlertType.ERROR);
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neispravan unos!",
                    "Morate da popunite sva polja.",
                    Alert.AlertType.ERROR);
        }
    }

    private void izmeniJedinicu() {
        jedinicaMere.setNaziv(tbxNaziv.getText());
        jedinicaMere.setSkraceniNaziv(tbxSkraceniNaziv.getText());
        try {
            jedinicaMereJpaController.edit(jedinicaMere);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniJedinicu()", this.getClass().getSimpleName());
        }
    }

    private void kreirajJedinicu() {
        jedinicaMere = new JedinicaMere();
        jedinicaMere.setNaziv(tbxNaziv.getText());
        jedinicaMere.setSkraceniNaziv(tbxSkraceniNaziv.getText());
        jedinicaMere.setIdDeleted(false);
        try {
            jedinicaMereJpaController.create(jedinicaMere);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajJedinicu()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void odustani(ActionEvent event) {
        this.stage.close();
    }

}
