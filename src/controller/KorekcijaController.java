/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import entity.Porudzbina;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.JedinicaMereJpaController;
import jpa.PorudzbinaJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class KorekcijaController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextArea tbxKorekcija;
    private Porudzbina porudzbina;
    private PorudzbinaJpaController porudzbinaJpaController;

    public void initialize(boolean update, Object objToUpdate) {
        porudzbinaJpaController = new PorudzbinaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        porudzbina = (Porudzbina) objToUpdate;
        initLabels();
    }

    private void initLabels(){
        tbxKorekcija.setText(porudzbina.getKorekcijaText());
    }
    
    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        kreirajKorekciju();
        okClicked=true;
        this.stage.close();
        
    }

    private void kreirajKorekciju() {
        if(tbxKorekcija.getText().equals("")){
            porudzbina.setKorekcija(false);
            porudzbina.setKorekcijaText("");
        }else{
            porudzbina.setKorekcija(true);
            porudzbina.setKorekcijaText(tbxKorekcija.getText());
        }
        
        try {
            porudzbinaJpaController.edit(porudzbina);
        } catch (Exception e) {
             LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajKorekciju()", this.getClass().getSimpleName());
        }
    }


    @FXML
    private void odustani(ActionEvent event) {
        okClicked = false;
        this.stage.close();
    }

}
