/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.filters.AutoCompleteComboBoxListener;
import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Artikl;
import entity.Kupac;
import entity.Ponuda;
import entity.StavkaPonude;
import enums.StatusPonude;
import enums.TipArtikla;
import enums.TipPonude;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.persistence.Persistence;
import jpa.ArtiklJpaController;
import jpa.KupacJpaController;
import jpa.KorisnikJpaController;
import jpa.PonudaJpaController;
import jpa.PorudzbinaJpaController;
import jpa.StavkaPonudeJpaController;
import jpa.exceptions.NonexistentEntityException;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class PonudaController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TableView<StavkaPonude> tblItems;
    @FXML
    private TableColumn<StavkaPonude, Integer> tbcRedniBroj;
    @FXML
    private TableColumn<StavkaPonude, String> tbcSifra;
    @FXML
    private TableColumn<StavkaPonude, String> tbcNaziv;
    @FXML
    private TableColumn<StavkaPonude, BigDecimal> tbcKolicina;
    @FXML
    private TableColumn<StavkaPonude, BigDecimal> tbcUkupno;
    @FXML
    private TableColumn<StavkaPonude, BigDecimal> tbcCenaBecRabata;
    @FXML
    private TableColumn<StavkaPonude, BigDecimal> tbcRabat;
    @FXML
    private TableColumn<StavkaPonude, BigDecimal> tbcCenaSaRabatom;
//    private TableColumn<StavkaPonude, BigDecimal> tbcPorOsnovica;
//    private TableColumn<StavkaPonude, BigDecimal> tbcPorStopa;
//    private TableColumn<StavkaPonude, BigDecimal> tbcPdv;
    @FXML
    private TableColumn<StavkaPonude, String> tbcJedinicaMere;
    @FXML
    private TextArea tbxKomentar;
    @FXML
    private GridPane downGrid;
    @FXML
    private Label lblUkupno;
    @FXML
    private Button btnObrisi;
    @FXML
    private TableView<Artikl> tblArtikli;
    @FXML
    private TableColumn<Artikl, String> tbcSifraArikl;
    @FXML
    private TableColumn<Artikl, BigDecimal> tbcCenaArtikla;
    @FXML
    private TableColumn<Artikl, String> tbcNazivArtikl;
    @FXML
    private TableColumn<Artikl, String> tbcTipArtikla;
    @FXML
    private TextField tbxFilter;
    @FXML
    private Label lblImeKupca;
    @FXML
    private Label lblAdresaKupca;
    @FXML
    private Label lblTelefonKupca;
    @FXML
    private Label lblEmailKupca;
    @FXML
    private Button btnDodajKupca;

    @FXML
    private TextField tbxAvans;
    @FXML
    private Button btnDodajStavku;
    @FXML
    private Button btnIzaberiKupca;

    private ArtiklJpaController artiklJpa;
    private PonudaJpaController ponudaJpa;
    private StavkaPonudeJpaController stavkaPonudeJpaController;
    private KupacJpaController fizickoLiceJpa;
    private Artikl selectedArtikl;
    private StavkaPonude selectedStavka;
    private Kupac selectedKupac;
    private Long redniBrojStavke = 0l;
    private BigDecimal ukupno = BigDecimal.ZERO;
    private BigDecimal avans = BigDecimal.ZERO;
    boolean valAvans;
    private Ponuda ponuda;
    private DecimalFormat df = new DecimalFormat("#,###.00");

    private ObservableList<Artikl> artiklAll = FXCollections.observableArrayList();
    private ObservableList<Artikl> artiklFilter = FXCollections.observableArrayList();
    private ObservableList<StavkaPonude> stavkaAll = FXCollections.observableArrayList();
    private ObservableList<StavkaPonude> stavkaFilter = FXCollections.observableArrayList();
    private ObservableList<StavkaPonude> listOfEditStavka = FXCollections.observableArrayList();
    //private ObservableList<Kupac> fizickoLiceAll = FXCollections.observableArrayList();
    //private ObservableList<Kupac> fizickoLiceFilter = FXCollections.observableArrayList();
    private ObservableList<Ponuda> porudzbinaAll = FXCollections.observableArrayList();

    private boolean update;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        artiklJpa = new ArtiklJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        ponudaJpa = new PonudaJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        stavkaPonudeJpaController = new StavkaPonudeJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        fizickoLiceJpa = new KupacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        tbxAvans.setVisible(false);

        if (update) {
            ponuda = (Ponuda) objToUpdate;
            getStavka();
            initInfoPonuda();
        } else {

        }
        initArtikl();
        initStavka();
        refreshUkupno();

//        setAvansController();
    }

    private void initInfoPonuda() {
        selectedKupac = ponuda.getKupacidkupac();
        popuniLabele(selectedKupac);
        btnIzaberiKupca.setDisable(true);
        btnDodajKupca.setDisable(true);
        tbxKomentar.setText(ponuda.getNapomena());
    }

    private void initArtikl() {
        getArtikl();
        initArtiklTableControllor();
        initArtiklTable();
    }

    private void initStavka() {
        initStavkaTableControllor();
        initStavkaTable();
    }

    private void popuniLabele(Kupac f) {
        lblImeKupca.setText(f.getIme() + " " + f.getPrezime());
        lblAdresaKupca.setText(f.getAdresa());
        lblEmailKupca.setText(f.getEmail());
        lblTelefonKupca.setText(f.getTelefon());
    }

    private void getStavka() {
        stavkaAll.clear();
        stavkaFilter.clear();
        try {
            List<StavkaPonude> list = stavkaPonudeJpaController.findByDelete(false);
            for (StavkaPonude stavkaPonude : list) {
                if (stavkaPonude.getPonuda().getIdponuda() == ponuda.getIdponuda()) {
                    StavkaPonude s = createStavkaFromStavka(stavkaPonude);
                    redniBrojStavke++;
                    s.setIdstavka(redniBrojStavke);
                    stavkaAll.add(s);
                    listOfEditStavka.add(stavkaPonude);
                }
            }

        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje stavki iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getStavka()", this.getClass().getSimpleName());
        }
    }

    private StavkaPonude createStavkaFromStavka(StavkaPonude stavkaPonude) {
        StavkaPonude newStavka = new StavkaPonude();
        newStavka.setArtikl(stavkaPonude.getArtikl());
        newStavka.setSifraStavke(stavkaPonude.getSifraStavke());
        newStavka.setNazivStavke(stavkaPonude.getNazivStavke());
        newStavka.setKolicina(stavkaPonude.getKolicina());
        newStavka.setCenaBezRabat(stavkaPonude.getCenaBezRabat());
        newStavka.setRabatStopa(stavkaPonude.getRabatStopa());
        newStavka.setCenaRabat(stavkaPonude.getCenaRabat());
        newStavka.setPoreskaOsonovica(stavkaPonude.getPoreskaOsonovica());
        newStavka.setPoreskaStopa(stavkaPonude.getPoreskaStopa());
        newStavka.setPdv(stavkaPonude.getPdv());
        newStavka.setIznos(stavkaPonude.getIznos());
        newStavka.setJedMereSkraceno(stavkaPonude.getJedMereSkraceno());
        newStavka.setJedMere(stavkaPonude.getJedMere());
        newStavka.setIdDeleted(false);

        return newStavka;
    }

    private void initStavkaTable() {
        tbcRedniBroj.setCellValueFactory(new PropertyValueFactory<StavkaPonude, Integer>("idstavka"));
        tbcSifra.setCellValueFactory(new PropertyValueFactory<StavkaPonude, String>("sifraStavke"));
        tbcNaziv.setCellValueFactory(new PropertyValueFactory<StavkaPonude, String>("nazivStavke"));
        tbcKolicina.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("kolicina"));
        tbcJedinicaMere.setCellValueFactory(new PropertyValueFactory<StavkaPonude, String>("jedMereSkraceno"));
        tbcCenaBecRabata.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("cenaBezRabat"));
        tbcRabat.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("rabatStopa"));
        tbcCenaSaRabatom.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("cenaRabat"));
//        tbcPorOsnovica.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("poreskaOsonovica"));
//        tbcPorStopa.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("poreskaStopa"));
//        tbcPdv.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("pdv"));
        tbcUkupno.setCellValueFactory(new PropertyValueFactory<StavkaPonude, BigDecimal>("iznos"));
        tblItems.setItems(stavkaAll);
    }

    private void initStavkaTableControllor() {
        stavkaFilter.addAll(stavkaAll);
        tblItems.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label

                StavkaPonude p = tblItems.getSelectionModel().getSelectedItem();
                selectedStavka = p;

                tblItems.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {

                        if (event.getClickCount() > 1) {
                            if (selectedStavka != null) {
                                // otvoriDijalogDodajStavku(selectedStavka);
                            }
                        }

                    }

                });

            }
        });
        tblItems.setRowFactory(new Callback<TableView<StavkaPonude>, TableRow<StavkaPonude>>() {
            @Override
            public TableRow<StavkaPonude> call(TableView<StavkaPonude> tableView2) {
                final TableRow<StavkaPonude> row = new TableRow<>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index > tblItems.getItems().size()) {
                            tblItems.getSelectionModel().clearSelection();
                            event.consume();
                            selectedStavka = null;
                        }
                    }
                });
                return row;
            }
        });
    }

    private void getArtikl() {
        artiklAll.clear();
        artiklFilter.clear();
        try {
            List<Artikl> list = artiklJpa.findByActive(false);
            artiklAll = FXCollections.observableList(list);
        } catch (Exception e) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspelo čitanje iz baze.",
                    "Neuspelo čitanje artikla iz baze, kontaktiraj te administratora",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "getArtikl()", this.getClass().getSimpleName());
        }
    }

    private void initArtiklTable() {
        tbcSifraArikl.setCellValueFactory(new PropertyValueFactory<Artikl, String>("sifra"));
        tbcNazivArtikl.setCellValueFactory(new PropertyValueFactory<Artikl, String>("naziv"));
        tbcCenaArtikla.setCellValueFactory(new PropertyValueFactory<Artikl, BigDecimal>("cena"));
        tbcTipArtikla.setCellValueFactory(new PropertyValueFactory<Artikl, String>("tip"));
        tblArtikli.setItems(artiklFilter);
    }

    private void initArtiklTableControllor() {
        artiklFilter.addAll(artiklAll);
        tbxFilter.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                updateFilteredArtikl();
            }
        });
        tblArtikli.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                Artikl p = tblArtikli.getSelectionModel().getSelectedItem();
                selectedArtikl = p;
                tblArtikli.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() > 0) {
                            if (selectedArtikl != null) {
                                if (!proveriPostojanjeStavke(selectedArtikl)) {
                                    otvoriDijalogDodajStavku(selectedArtikl);
                                }
                            }

                        }

                    }

                });

            }
        });

        tblArtikli.setRowFactory(new Callback<TableView<Artikl>, TableRow<Artikl>>() {
            @Override
            public TableRow<Artikl> call(TableView<Artikl> tableView2) {
                final TableRow<Artikl> row = new TableRow<>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index > tblArtikli.getItems().size()) {
                            tblArtikli.getSelectionModel().clearSelection();
                            event.consume();
                            selectedArtikl = null;
                        }
                    }
                });
                return row;
            }
        });
    }

    private boolean proveriPostojanjeStavke(Artikl a) {
        for (StavkaPonude s : stavkaAll) {
            if (a.getIdArtikl() == s.getArtikl().getIdArtikl()) {
                otvoriDijalogDodajStavku(s);
                return true;
            }
        }
        return false;
    }

    private void otvoriDijalogDodajStavku(Object o) {
        StavkaPonude s = null;
        Artikl a = null;

        try {
            setModalDialog("/view/DodajStavkuPonude.fxml");
        } catch (IOException ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "otvoriDijalogDodajStavku()", this.getClass().getSimpleName());
        }

        DodajStavkuPonudeController t = (DodajStavkuPonudeController) fl.getController();
        t.setStage(mf);
        if (o instanceof StavkaPonude) {
            s = (StavkaPonude) o;
            t.initialize(true, s);
        } else if (o instanceof Artikl) {
            a = (Artikl) o;
            t.initialize(false, a);
        }
        mf.setTitle("Izmeni artikl");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            if (o instanceof StavkaPonude) {
                izmeniStavku(t.getStavkaPonude());
            } else if (o instanceof Artikl) {
                kreirajDodajStavku(t.getStavkaPonude(), selectedArtikl);
            }
            initArtikl();
        }
    }

    private void kreirajDodajStavku(StavkaPonude stavkaPonude, Artikl artikl) {
        redniBrojStavke++;
        stavkaPonude.setArtikl(artikl);
        stavkaPonude.setIdstavka(redniBrojStavke);
        stavkaAll.add(stavkaPonude);
        refreshUkupno();
        initStavkaTable();
    }

    private void izmeniStavku(StavkaPonude stavkaPonude) {
        for (StavkaPonude s : stavkaAll) {
            if (s.getArtikl().getIdArtikl() == s.getArtikl().getIdArtikl()) {

                s.setKolicina(stavkaPonude.getKolicina());
                s.setCenaRabat(stavkaPonude.getCenaRabat());
                s.setCenaBezRabat(stavkaPonude.getCenaBezRabat());
                s.setRabatStopa(stavkaPonude.getRabatStopa());
                s.setPoreskaOsonovica(stavkaPonude.getPoreskaOsonovica());
                s.setPdv(stavkaPonude.getPdv());
                s.setIznos(stavkaPonude.getIznos());
                break;
            }
        }
        refreshUkupno();
        tblItems.refresh();
    }

    private void refreshUkupno() {
        ukupno = BigDecimal.ZERO;
        for (StavkaPonude s : stavkaAll) {
            ukupno = ukupno.add(s.getIznos());
        }
        lblUkupno.setText("Ukupno: " + df.format(ukupno));
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void save(ActionEvent event) throws IOException, Exception {
        if (tblItems.getItems().size() > 0) {
            if (selectedKupac != null) {
                if (update) {
                    izmeniPonudu();
                } else {
                    sacuvajPonudu();
                }

                okClicked = true;
                this.stage.close();
            } else {

                AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                        "Nije izabrano fizičko lice!",
                        "Morate uneti fizičko lice!",
                        Alert.AlertType.ERROR);
                return;
            }

        } else {

            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Nema stavki!",
                    "Morate uneti bar jednu stavku!",
                    Alert.AlertType.ERROR);
            return;
        }

    }

    private void izmeniPonudu() {
        ponuda.setAdresa(selectedKupac.getAdresa());
        ponuda.setKupacidkupac(selectedKupac);
        ponuda.setKupacImePrezime(selectedKupac.getIme() + " " + selectedKupac.getPrezime());
        ponuda.setUkupno(ukupno);
        ponuda.setTip(TipPonude.PONUDA.toString());
        ponuda.setNapomena(tbxKomentar.getText());
        try {
            ponudaJpa.edit(ponuda);
        } catch (NonexistentEntityException ex) {
             LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniPonudu()", this.getClass().getSimpleName());
        } catch (Exception ex) {
              LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniPonudu()", this.getClass().getSimpleName());
        }
        //brisemo stare stavke
        for (StavkaPonude stavkaPonude : listOfEditStavka) {
            try {
                stavkaPonudeJpaController.destroy(stavkaPonude.getIdstavka());
            } catch (Exception e) {
                LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniPonudu()", this.getClass().getSimpleName());
            }
        }

        for (StavkaPonude s : stavkaAll) {
            s.setIdstavka(null);
            s.setPonuda(ponuda);
            if (s.getArtikl().getTip().equals(TipArtikla.USLUGA.toString())) {
                s.setJedMere("");
                s.setJedMereSkraceno("");
            }
            stavkaPonudeJpaController.create(s);
        }

    }

    private void sacuvajPonudu() throws Exception {
        ponuda = new Ponuda();
        ponuda.setAdresa(selectedKupac.getAdresa());
        ponuda.setAvans(BigDecimal.ZERO);
        ponuda.setDatum(new Date());
        ponuda.setKupacidkupac(selectedKupac);
        ponuda.setKorisnikidkorisnik(MainFrameController.korisnik);
        ponuda.setIsDeleted(false);
        ponuda.setKorisnikImePrezime(MainFrameController.korisnik.getIme() + " " + MainFrameController.korisnik.getPrezime());
        ponuda.setStatus(StatusPonude.NA_CEKANJU_KUPCA.toString());
        ponuda.setKupacImePrezime(selectedKupac.getIme() + " " + selectedKupac.getPrezime());
        ponuda.setUkupno(ukupno);
        ponuda.setTip(TipPonude.PONUDA.toString());
        ponuda.setIsplacena("NE");
        ponuda.setIdDeleted(false);
        ponuda.setNapomena(tbxKomentar.getText());
        try {
            ponudaJpa.create(ponuda);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "sacuvajPonudu()", this.getClass().getSimpleName());
        }

        for (StavkaPonude s : stavkaAll) {
            s.setIdstavka(null);
            s.setPonuda(ponuda);
            if (s.getArtikl().getTip().equals(TipArtikla.USLUGA.toString())) {
                s.setJedMere("");
                s.setJedMereSkraceno("");
            }
            stavkaPonudeJpaController.create(s);

        }
    }

   

    @FXML
    private void odustani(ActionEvent event) {
        setOkClicked(false);
        this.stage.close();
    }

    @FXML
    private void obrisi(ActionEvent event) {
        if (selectedStavka != null) {
            for (StavkaPonude s : stavkaAll) {
                if (s.getIdstavka() == selectedStavka.getIdstavka()) {
                    stavkaAll.remove(s);
                    break;
                }
            }
            redniBrojStavke = 1l;
            for (StavkaPonude s : stavkaAll) {
                s.setIdstavka(redniBrojStavke);
                redniBrojStavke++;
            }
            redniBrojStavke--;
            initStavkaTable();
            refreshUkupno();
        }
    }

    public void setModalDialog(String name) throws IOException {
        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));

        try {
            fl.load();
        } catch (IOException ex) {
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neuspešno prikazivanje dialoga.",
                    "Dialog nije otvoren kako treba!",
                    Alert.AlertType.ERROR);
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "setModalDialog()", this.getClass().getSimpleName());
        }
        Parent root = fl.getRoot();
        mf = new Stage(StageStyle.DECORATED);
        mf.initModality(Modality.WINDOW_MODAL);
        mf.initOwner(stage);
        mf.setResizable(false);
        scene = new Scene(root);
    }

    private void updateFilteredArtikl() {

        artiklFilter.clear();

        for (Artikl s : artiklAll) {
            if (s.getNaziv().toString().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getSifra().toLowerCase().contains(tbxFilter.getText().toLowerCase())
                    || s.getTip().toLowerCase().contains(tbxFilter.getText().toLowerCase())) {
                artiklFilter.add(s);

            }

        }
        // Must re-sort table after items changed
        reapplyArtiklTableSortOrder();
    }

    private void reapplyArtiklTableSortOrder() {

        ArrayList<TableColumn<Artikl, ?>> sortOrder = new ArrayList<>(tblArtikli.getSortOrder());
        tblArtikli.getSortOrder().clear();
        tblArtikli.getSortOrder().addAll(sortOrder);
    }

    @FXML
    private void dodajKupca(ActionEvent event) {
        try {
            setModalDialog("/view/DodajFizickoLice.fxml");
        } catch (IOException ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "dodajKupca()", this.getClass().getSimpleName());
        }

        DodajFizickoLiceController t = (DodajFizickoLiceController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dodaj fizicko lice");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            //dodajNovoFLuCBX(t.getFizickoLice());
            selectedKupac = t.getFizickoLice();
            popuniLabele(selectedKupac);
        }
    }

    private void dodajNovoFLuCBX(Kupac f) {
//        fizickoLiceAll.add(f);
//        cbxFizickaLica.getItems().removeAll();
//        cbxFizickaLica.getItems().addAll(fizickoLiceAll);
//        cbxFizickaLica.getSelectionModel().select(f);
//        popuniLabele(f);
    }

    private void prikaziAlert(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();

        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/css/lawOfficeMetro.css").toExternalForm());
        dialogPane.getStyleClass().add("dialog-pane");
    }

    public void validationAvans() {

        if (tbxAvans.getText() == null || tbxAvans.getText().trim().equals("")) {
            valAvans = true;
            avans = BigDecimal.ZERO;
            tbxAvans.getStyleClass().removeAll("text-field-error");
            tbxAvans.getStyleClass().add("text-field");
        } else {
            try {
                BigDecimal m = new BigDecimal(tbxAvans.getText().replaceAll(",", ""));
                avans = m;
                valAvans = true;
                tbxAvans.getStyleClass().removeAll("text-field-error");
                tbxAvans.getStyleClass().add("text-field");
            } catch (NumberFormatException nfe) {
                valAvans = false;
                tbxAvans.getStyleClass().removeAll("text-field");
                tbxAvans.getStyleClass().add("text-field-error");
            }
        }
    }

    private void setAvansController() {
//        tbxAvans.textProperty().addListener((observable, oldValue, newValue) -> {
//            validationAvans();
//
//        });
    }

    @FXML
    private void dodajNovuStavkuArtikl(ActionEvent event) {
        try {
            setModalDialog("/view/DodajStavkuPonude.fxml");
        } catch (IOException ex) {
            LoggerUtil.logError(ex.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "dodajNovuStavkuArtikl()", this.getClass().getSimpleName());
        }

        DodajStavkuPonudeController t = (DodajStavkuPonudeController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Dodaj stavku");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            initArtikl();
            selectedArtikl = t.getArtikl();
            kreirajDodajStavku(t.getStavkaPonude(), selectedArtikl);
        }
    }

    public Ponuda getPonuda() {
        return ponuda;
    }

    @FXML
    private void izaberiKupca(ActionEvent event) throws IOException {
        setModalDialog("/view/FizickaLica.fxml");
        FizickaLicaController t = (FizickaLicaController) fl.getController();
        t.setStage(mf);
        t.initialize(false, null);
        mf.setTitle("Fizička Lica");
        mf.getIcons().add(new Image("/images/firangeIcon.png"));
        mf.setScene(scene);
        mf.showAndWait();
        if (t.isOkClicked()) {
            selectedKupac = t.getSelectedKupac();
            popuniLabele(selectedKupac);
        }
    }

}
