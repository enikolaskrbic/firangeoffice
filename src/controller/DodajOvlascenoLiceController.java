/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Dobavljac;
import entity.OvlascenoLice;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.OvlascenoLiceJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajOvlascenoLiceController extends AbstracController  {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;
    
    @FXML
    private TextField tbxIme;
    @FXML
    private TextField tbxPrezime;
    
    private OvlascenoLiceJpaController ovlascenoLiceJpa;
    private Dobavljac dobavljac;
    private OvlascenoLice ovlascenoLice;
    
    private boolean update;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        ovlascenoLiceJpa = new OvlascenoLiceJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        if(update){
            ovlascenoLice = (OvlascenoLice) objToUpdate;
            initLabels();
        }else{
            dobavljac = (Dobavljac) objToUpdate;
        }
        
             
    }
    
    private void initLabels(){
        tbxIme.setText(ovlascenoLice.getIme());
        tbxPrezime.setText(ovlascenoLice.getPrezime());
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        if(!tbxIme.getText().trim().equals("") &&
            !tbxPrezime.getText().trim().equals("")
                ){
                if(update){
                    izmeniOvlascenoLice();
                }else{
                    kreirajOvlastenoLice();
                }
                
                okClicked = true;
                this.stage.close();
                
        }else{
            AbstractAlertDialog abstractAlertDialog = new AbstractAlertDialog("Greška", 
                    "Neispravan unos!", 
                    "Morate da popunite sva polja.",
                    AlertType.ERROR);
        }
    }
    
    private void izmeniOvlascenoLice(){
        ovlascenoLice.setIme(tbxIme.getText());
        ovlascenoLice.setPrezime(tbxPrezime.getText());
        try {
            ovlascenoLiceJpa.edit(ovlascenoLice);
        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "izmeniOvlascenoLice()", this.getClass().getSimpleName());
        }
    }
    
    private void kreirajOvlastenoLice(){
        OvlascenoLice o = new OvlascenoLice();
        o.setIme(tbxIme.getText());
        o.setPrezime(tbxPrezime.getText());
        o.setDobavljac(dobavljac);
        o.setIdDeleted(false);
        try {
            ovlascenoLiceJpa.create(o);
        } catch (Exception e) {
           LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajOvlastenoLice()", this.getClass().getSimpleName());
        }
    }

    @FXML
    private void odustani(ActionEvent event) {
        okClicked = false;
        this.stage.close();
        
    }

  
    
}
