/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import custom.AbstracController;
import custom.AbstractAlertDialog;
import entity.Dobavljac;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.Persistence;
import jpa.DobavljacJpaController;
import loger.LoggerUtil;

/**
 * FXML Controller class
 *
 * @author Skrbic
 */
public class DodajDobavljacaController extends AbstracController {

    private FXMLLoader fl;
    private Stage mf;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField tbxNaziv;
    @FXML
    private TextField tbxAdresa;
    @FXML
    private TextField tbxMesto;
    @FXML
    private TextField tbxPib;
    @FXML
    private TextField tbxTelefon;
    @FXML
    private TextField tbxEmail;

    private DobavljacJpaController dobavljacJpa;
    private Dobavljac dobavljac;

    private boolean update;

    @Override
    public void initialize(boolean update, Object objToUpdate) {
        dobavljacJpa = new DobavljacJpaController(Persistence.createEntityManagerFactory("FirangePU"));
        this.update = update;
        if (update) {
            dobavljac = (Dobavljac) objToUpdate;
            initLabels();
        }
        setTextFieldChange();
    }

    private void initLabels() {
        tbxAdresa.setText(dobavljac.getAdresa());
        tbxEmail.setText(dobavljac.getEmail());
        tbxMesto.setText(dobavljac.getMesto());
        tbxNaziv.setText(dobavljac.getNaziv());
        tbxPib.setText(dobavljac.getPib());
        tbxTelefon.setText(dobavljac.getTelefon());
    }

    private void setTextFieldChange() {
//        tbxTelefon.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                if (!newValue.matches("\\d*")) {
//                    tbxTelefon.setText(newValue.replaceAll("[^\\d]", ""));
//                }
//            }
//        });
//
//        tbxPib.textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                if (!newValue.matches("\\d*")) {
//                    tbxPib.setText(newValue.replaceAll("[^\\d]", ""));
//                }
//            }
//        });
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void sacuvaj(ActionEvent event) {
        if (!tbxNaziv.getText().trim().equals("")
                && !tbxAdresa.getText().trim().equals("")
                && !tbxEmail.getText().trim().equals("")
                && !tbxPib.getText().trim().equals("")
                && !tbxMesto.getText().trim().equals("")
                && !tbxTelefon.getText().trim().equals("")) {

            kreirajIzmeniDobavljaca(update);
            okClicked = true;
            this.stage.close();

        } else {
//            Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setTitle("Greska");
//            alert.setHeaderText("Neispravan unos!");
//            alert.setContentText("Morate da popunite sva polja.");
//            alert.showAndWait();
            AbstractAlertDialog a = new AbstractAlertDialog("Greška",
                    "Neispravan unos!",
                    "Morate da popunite sva polja.",
                    Alert.AlertType.ERROR);
        }
    }

    private void kreirajIzmeniDobavljaca(boolean value) {
        if (!value) {
            dobavljac = new Dobavljac();
        }
        dobavljac.setAdresa(tbxAdresa.getText());
        dobavljac.setEmail(tbxEmail.getText());
        dobavljac.setMesto(tbxMesto.getText());
        dobavljac.setNaziv(tbxNaziv.getText());
        dobavljac.setPib(tbxPib.getText());
        dobavljac.setTelefon(tbxTelefon.getText());
        //dobavljac.setIdDeleted(false);
        dobavljac.setIdDeleted(false);
        try {
            if (!value) {
                dobavljacJpa.create(dobavljac);
            } else {
                dobavljacJpa.edit(dobavljac);
            }

        } catch (Exception e) {
            LoggerUtil.logError(e.getMessage(),
                    MainFrameController.korisnik.getKorisnickoIme(),
                    "kreirajIzmeniDobavljaca()", this.getClass().getSimpleName());
        }

    }

    @FXML
    private void odustani(ActionEvent event) {
        okClicked = false;
        this.stage.close();
    }

    public Dobavljac getDobavljac() {
        return dobavljac;
    }

}
