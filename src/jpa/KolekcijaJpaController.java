/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Dobavljac;
import entity.Artikl;
import entity.Kolekcija;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class KolekcijaJpaController implements Serializable {

    public KolekcijaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Kolekcija kolekcija) {
        if (kolekcija.getArtiklCollection() == null) {
            kolekcija.setArtiklCollection(new ArrayList<Artikl>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dobavljac dobavljac = kolekcija.getDobavljac();
            if (dobavljac != null) {
                dobavljac = em.getReference(dobavljac.getClass(), dobavljac.getIddobavljac());
                kolekcija.setDobavljac(dobavljac);
            }
            Collection<Artikl> attachedArtiklCollection = new ArrayList<Artikl>();
            for (Artikl artiklCollectionArtiklToAttach : kolekcija.getArtiklCollection()) {
                artiklCollectionArtiklToAttach = em.getReference(artiklCollectionArtiklToAttach.getClass(), artiklCollectionArtiklToAttach.getIdArtikl());
                attachedArtiklCollection.add(artiklCollectionArtiklToAttach);
            }
            kolekcija.setArtiklCollection(attachedArtiklCollection);
            em.persist(kolekcija);
            if (dobavljac != null) {
                dobavljac.getKolekcijaCollection().add(kolekcija);
                dobavljac = em.merge(dobavljac);
            }
            for (Artikl artiklCollectionArtikl : kolekcija.getArtiklCollection()) {
                Kolekcija oldKolekcijaIdkolekcijaOfArtiklCollectionArtikl = artiklCollectionArtikl.getKolekcijaIdkolekcija();
                artiklCollectionArtikl.setKolekcijaIdkolekcija(kolekcija);
                artiklCollectionArtikl = em.merge(artiklCollectionArtikl);
                if (oldKolekcijaIdkolekcijaOfArtiklCollectionArtikl != null) {
                    oldKolekcijaIdkolekcijaOfArtiklCollectionArtikl.getArtiklCollection().remove(artiklCollectionArtikl);
                    oldKolekcijaIdkolekcijaOfArtiklCollectionArtikl = em.merge(oldKolekcijaIdkolekcijaOfArtiklCollectionArtikl);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Kolekcija kolekcija) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Kolekcija persistentKolekcija = em.find(Kolekcija.class, kolekcija.getIdkolekcija());
            Dobavljac dobavljacOld = persistentKolekcija.getDobavljac();
            Dobavljac dobavljacNew = kolekcija.getDobavljac();
            Collection<Artikl> artiklCollectionOld = persistentKolekcija.getArtiklCollection();
            Collection<Artikl> artiklCollectionNew = kolekcija.getArtiklCollection();
            if (dobavljacNew != null) {
                dobavljacNew = em.getReference(dobavljacNew.getClass(), dobavljacNew.getIddobavljac());
                kolekcija.setDobavljac(dobavljacNew);
            }
            Collection<Artikl> attachedArtiklCollectionNew = new ArrayList<Artikl>();
            for (Artikl artiklCollectionNewArtiklToAttach : artiklCollectionNew) {
                artiklCollectionNewArtiklToAttach = em.getReference(artiklCollectionNewArtiklToAttach.getClass(), artiklCollectionNewArtiklToAttach.getIdArtikl());
                attachedArtiklCollectionNew.add(artiklCollectionNewArtiklToAttach);
            }
            artiklCollectionNew = attachedArtiklCollectionNew;
            kolekcija.setArtiklCollection(artiklCollectionNew);
            kolekcija = em.merge(kolekcija);
            if (dobavljacOld != null && !dobavljacOld.equals(dobavljacNew)) {
                dobavljacOld.getKolekcijaCollection().remove(kolekcija);
                dobavljacOld = em.merge(dobavljacOld);
            }
            if (dobavljacNew != null && !dobavljacNew.equals(dobavljacOld)) {
                dobavljacNew.getKolekcijaCollection().add(kolekcija);
                dobavljacNew = em.merge(dobavljacNew);
            }
            for (Artikl artiklCollectionOldArtikl : artiklCollectionOld) {
                if (!artiklCollectionNew.contains(artiklCollectionOldArtikl)) {
                    artiklCollectionOldArtikl.setKolekcijaIdkolekcija(null);
                    artiklCollectionOldArtikl = em.merge(artiklCollectionOldArtikl);
                }
            }
            for (Artikl artiklCollectionNewArtikl : artiklCollectionNew) {
                if (!artiklCollectionOld.contains(artiklCollectionNewArtikl)) {
                    Kolekcija oldKolekcijaIdkolekcijaOfArtiklCollectionNewArtikl = artiklCollectionNewArtikl.getKolekcijaIdkolekcija();
                    artiklCollectionNewArtikl.setKolekcijaIdkolekcija(kolekcija);
                    artiklCollectionNewArtikl = em.merge(artiklCollectionNewArtikl);
                    if (oldKolekcijaIdkolekcijaOfArtiklCollectionNewArtikl != null && !oldKolekcijaIdkolekcijaOfArtiklCollectionNewArtikl.equals(kolekcija)) {
                        oldKolekcijaIdkolekcijaOfArtiklCollectionNewArtikl.getArtiklCollection().remove(artiklCollectionNewArtikl);
                        oldKolekcijaIdkolekcijaOfArtiklCollectionNewArtikl = em.merge(oldKolekcijaIdkolekcijaOfArtiklCollectionNewArtikl);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = kolekcija.getIdkolekcija();
                if (findKolekcija(id) == null) {
                    throw new NonexistentEntityException("The kolekcija with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Kolekcija kolekcija;
            try {
                kolekcija = em.getReference(Kolekcija.class, id);
                kolekcija.getIdkolekcija();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The kolekcija with id " + id + " no longer exists.", enfe);
            }
            Dobavljac dobavljac = kolekcija.getDobavljac();
            if (dobavljac != null) {
                dobavljac.getKolekcijaCollection().remove(kolekcija);
                dobavljac = em.merge(dobavljac);
            }
            Collection<Artikl> artiklCollection = kolekcija.getArtiklCollection();
            for (Artikl artiklCollectionArtikl : artiklCollection) {
                artiklCollectionArtikl.setKolekcijaIdkolekcija(null);
                artiklCollectionArtikl = em.merge(artiklCollectionArtikl);
            }
            em.remove(kolekcija);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Kolekcija> findKolekcijaEntities() {
        return findKolekcijaEntities(true, -1, -1);
    }

    public List<Kolekcija> findKolekcijaEntities(int maxResults, int firstResult) {
        return findKolekcijaEntities(false, maxResults, firstResult);
    }

    private List<Kolekcija> findKolekcijaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Kolekcija.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Kolekcija findKolekcija(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Kolekcija.class, id);
        } finally {
            em.close();
        }
    }

    public int getKolekcijaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Kolekcija> rt = cq.from(Kolekcija.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Kolekcija> findKolekcijaByDeleted(boolean deleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Kolekcija.findByIdDeleted")
                    .setParameter("idDeleted", deleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
}
