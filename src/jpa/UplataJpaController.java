/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Ponuda;
import entity.Uplata;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class UplataJpaController implements Serializable {

    public UplataJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Uplata uplata) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ponuda idPonuda = uplata.getIdPonuda();
            if (idPonuda != null) {
                idPonuda = em.getReference(idPonuda.getClass(), idPonuda.getIdponuda());
                uplata.setIdPonuda(idPonuda);
            }
            em.persist(uplata);
            if (idPonuda != null) {
                idPonuda.getUplataCollection().add(uplata);
                idPonuda = em.merge(idPonuda);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Uplata uplata) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Uplata persistentUplata = em.find(Uplata.class, uplata.getIdUplata());
            Ponuda idPonudaOld = persistentUplata.getIdPonuda();
            Ponuda idPonudaNew = uplata.getIdPonuda();
            if (idPonudaNew != null) {
                idPonudaNew = em.getReference(idPonudaNew.getClass(), idPonudaNew.getIdponuda());
                uplata.setIdPonuda(idPonudaNew);
            }
            uplata = em.merge(uplata);
            if (idPonudaOld != null && !idPonudaOld.equals(idPonudaNew)) {
                idPonudaOld.getUplataCollection().remove(uplata);
                idPonudaOld = em.merge(idPonudaOld);
            }
            if (idPonudaNew != null && !idPonudaNew.equals(idPonudaOld)) {
                idPonudaNew.getUplataCollection().add(uplata);
                idPonudaNew = em.merge(idPonudaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = uplata.getIdUplata();
                if (findUplata(id) == null) {
                    throw new NonexistentEntityException("The uplata with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Uplata uplata;
            try {
                uplata = em.getReference(Uplata.class, id);
                uplata.getIdUplata();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The uplata with id " + id + " no longer exists.", enfe);
            }
            Ponuda idPonuda = uplata.getIdPonuda();
            if (idPonuda != null) {
                idPonuda.getUplataCollection().remove(uplata);
                idPonuda = em.merge(idPonuda);
            }
            em.remove(uplata);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Uplata> findUplataEntities() {
        return findUplataEntities(true, -1, -1);
    }

    public List<Uplata> findUplataEntities(int maxResults, int firstResult) {
        return findUplataEntities(false, maxResults, firstResult);
    }

    private List<Uplata> findUplataEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Uplata.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Uplata findUplata(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Uplata.class, id);
        } finally {
            em.close();
        }
    }

    public int getUplataCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Uplata> rt = cq.from(Uplata.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Uplata> findByActive(boolean deleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Uplata.findByIdDeleted")
                    .setParameter("idDeleted", deleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
}
