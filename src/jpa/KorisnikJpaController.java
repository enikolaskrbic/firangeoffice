/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Firma;
import entity.Korisnik;
import entity.Ponuda;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class KorisnikJpaController implements Serializable {

    public KorisnikJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Korisnik korisnik) {
        if (korisnik.getPonudaCollection() == null) {
            korisnik.setPonudaCollection(new ArrayList<Ponuda>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Firma firmaidfirma = korisnik.getFirmaidfirma();
            if (firmaidfirma != null) {
                firmaidfirma = em.getReference(firmaidfirma.getClass(), firmaidfirma.getIdfirma());
                korisnik.setFirmaidfirma(firmaidfirma);
            }
            Collection<Ponuda> attachedPonudaCollection = new ArrayList<Ponuda>();
            for (Ponuda ponudaCollectionPonudaToAttach : korisnik.getPonudaCollection()) {
                ponudaCollectionPonudaToAttach = em.getReference(ponudaCollectionPonudaToAttach.getClass(), ponudaCollectionPonudaToAttach.getIdponuda());
                attachedPonudaCollection.add(ponudaCollectionPonudaToAttach);
            }
            korisnik.setPonudaCollection(attachedPonudaCollection);
            em.persist(korisnik);
            if (firmaidfirma != null) {
                firmaidfirma.getKorisnikCollection().add(korisnik);
                firmaidfirma = em.merge(firmaidfirma);
            }
            for (Ponuda ponudaCollectionPonuda : korisnik.getPonudaCollection()) {
                Korisnik oldKorisnikidkorisnikOfPonudaCollectionPonuda = ponudaCollectionPonuda.getKorisnikidkorisnik();
                ponudaCollectionPonuda.setKorisnikidkorisnik(korisnik);
                ponudaCollectionPonuda = em.merge(ponudaCollectionPonuda);
                if (oldKorisnikidkorisnikOfPonudaCollectionPonuda != null) {
                    oldKorisnikidkorisnikOfPonudaCollectionPonuda.getPonudaCollection().remove(ponudaCollectionPonuda);
                    oldKorisnikidkorisnikOfPonudaCollectionPonuda = em.merge(oldKorisnikidkorisnikOfPonudaCollectionPonuda);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Korisnik korisnik) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Korisnik persistentKorisnik = em.find(Korisnik.class, korisnik.getIdKorisnik());
            Firma firmaidfirmaOld = persistentKorisnik.getFirmaidfirma();
            Firma firmaidfirmaNew = korisnik.getFirmaidfirma();
            Collection<Ponuda> ponudaCollectionOld = persistentKorisnik.getPonudaCollection();
            Collection<Ponuda> ponudaCollectionNew = korisnik.getPonudaCollection();
            List<String> illegalOrphanMessages = null;
            for (Ponuda ponudaCollectionOldPonuda : ponudaCollectionOld) {
                if (!ponudaCollectionNew.contains(ponudaCollectionOldPonuda)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ponuda " + ponudaCollectionOldPonuda + " since its korisnikidkorisnik field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (firmaidfirmaNew != null) {
                firmaidfirmaNew = em.getReference(firmaidfirmaNew.getClass(), firmaidfirmaNew.getIdfirma());
                korisnik.setFirmaidfirma(firmaidfirmaNew);
            }
            Collection<Ponuda> attachedPonudaCollectionNew = new ArrayList<Ponuda>();
            for (Ponuda ponudaCollectionNewPonudaToAttach : ponudaCollectionNew) {
                ponudaCollectionNewPonudaToAttach = em.getReference(ponudaCollectionNewPonudaToAttach.getClass(), ponudaCollectionNewPonudaToAttach.getIdponuda());
                attachedPonudaCollectionNew.add(ponudaCollectionNewPonudaToAttach);
            }
            ponudaCollectionNew = attachedPonudaCollectionNew;
            korisnik.setPonudaCollection(ponudaCollectionNew);
            korisnik = em.merge(korisnik);
            if (firmaidfirmaOld != null && !firmaidfirmaOld.equals(firmaidfirmaNew)) {
                firmaidfirmaOld.getKorisnikCollection().remove(korisnik);
                firmaidfirmaOld = em.merge(firmaidfirmaOld);
            }
            if (firmaidfirmaNew != null && !firmaidfirmaNew.equals(firmaidfirmaOld)) {
                firmaidfirmaNew.getKorisnikCollection().add(korisnik);
                firmaidfirmaNew = em.merge(firmaidfirmaNew);
            }
            for (Ponuda ponudaCollectionNewPonuda : ponudaCollectionNew) {
                if (!ponudaCollectionOld.contains(ponudaCollectionNewPonuda)) {
                    Korisnik oldKorisnikidkorisnikOfPonudaCollectionNewPonuda = ponudaCollectionNewPonuda.getKorisnikidkorisnik();
                    ponudaCollectionNewPonuda.setKorisnikidkorisnik(korisnik);
                    ponudaCollectionNewPonuda = em.merge(ponudaCollectionNewPonuda);
                    if (oldKorisnikidkorisnikOfPonudaCollectionNewPonuda != null && !oldKorisnikidkorisnikOfPonudaCollectionNewPonuda.equals(korisnik)) {
                        oldKorisnikidkorisnikOfPonudaCollectionNewPonuda.getPonudaCollection().remove(ponudaCollectionNewPonuda);
                        oldKorisnikidkorisnikOfPonudaCollectionNewPonuda = em.merge(oldKorisnikidkorisnikOfPonudaCollectionNewPonuda);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = korisnik.getIdKorisnik();
                if (findKorisnik(id) == null) {
                    throw new NonexistentEntityException("The korisnik with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Korisnik korisnik;
            try {
                korisnik = em.getReference(Korisnik.class, id);
                korisnik.getIdKorisnik();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The korisnik with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Ponuda> ponudaCollectionOrphanCheck = korisnik.getPonudaCollection();
            for (Ponuda ponudaCollectionOrphanCheckPonuda : ponudaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Korisnik (" + korisnik + ") cannot be destroyed since the Ponuda " + ponudaCollectionOrphanCheckPonuda + " in its ponudaCollection field has a non-nullable korisnikidkorisnik field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Firma firmaidfirma = korisnik.getFirmaidfirma();
            if (firmaidfirma != null) {
                firmaidfirma.getKorisnikCollection().remove(korisnik);
                firmaidfirma = em.merge(firmaidfirma);
            }
            em.remove(korisnik);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Korisnik> findKorisnikEntities() {
        return findKorisnikEntities(true, -1, -1);
    }

    public List<Korisnik> findKorisnikEntities(int maxResults, int firstResult) {
        return findKorisnikEntities(false, maxResults, firstResult);
    }

    private List<Korisnik> findKorisnikEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Korisnik.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Korisnik findKorisnik(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Korisnik.class, id);
        } finally {
            em.close();
        }
    }

    public int getKorisnikCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Korisnik> rt = cq.from(Korisnik.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
