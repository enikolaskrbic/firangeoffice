/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Dobavljac;
import entity.OvlascenoLice;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class OvlascenoLiceJpaController implements Serializable {

    public OvlascenoLiceJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OvlascenoLice ovlascenoLice) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dobavljac dobavljac = ovlascenoLice.getDobavljac();
            if (dobavljac != null) {
                dobavljac = em.getReference(dobavljac.getClass(), dobavljac.getIddobavljac());
                ovlascenoLice.setDobavljac(dobavljac);
            }
            em.persist(ovlascenoLice);
            if (dobavljac != null) {
                dobavljac.getOvlascenoLiceCollection().add(ovlascenoLice);
                dobavljac = em.merge(dobavljac);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OvlascenoLice ovlascenoLice) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OvlascenoLice persistentOvlascenoLice = em.find(OvlascenoLice.class, ovlascenoLice.getIdovlascenoLice());
            Dobavljac dobavljacOld = persistentOvlascenoLice.getDobavljac();
            Dobavljac dobavljacNew = ovlascenoLice.getDobavljac();
            if (dobavljacNew != null) {
                dobavljacNew = em.getReference(dobavljacNew.getClass(), dobavljacNew.getIddobavljac());
                ovlascenoLice.setDobavljac(dobavljacNew);
            }
            ovlascenoLice = em.merge(ovlascenoLice);
            if (dobavljacOld != null && !dobavljacOld.equals(dobavljacNew)) {
                dobavljacOld.getOvlascenoLiceCollection().remove(ovlascenoLice);
                dobavljacOld = em.merge(dobavljacOld);
            }
            if (dobavljacNew != null && !dobavljacNew.equals(dobavljacOld)) {
                dobavljacNew.getOvlascenoLiceCollection().add(ovlascenoLice);
                dobavljacNew = em.merge(dobavljacNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ovlascenoLice.getIdovlascenoLice();
                if (findOvlascenoLice(id) == null) {
                    throw new NonexistentEntityException("The ovlascenoLice with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OvlascenoLice ovlascenoLice;
            try {
                ovlascenoLice = em.getReference(OvlascenoLice.class, id);
                ovlascenoLice.getIdovlascenoLice();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ovlascenoLice with id " + id + " no longer exists.", enfe);
            }
            Dobavljac dobavljac = ovlascenoLice.getDobavljac();
            if (dobavljac != null) {
                dobavljac.getOvlascenoLiceCollection().remove(ovlascenoLice);
                dobavljac = em.merge(dobavljac);
            }
            em.remove(ovlascenoLice);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OvlascenoLice> findOvlascenoLiceEntities() {
        return findOvlascenoLiceEntities(true, -1, -1);
    }

    public List<OvlascenoLice> findOvlascenoLiceEntities(int maxResults, int firstResult) {
        return findOvlascenoLiceEntities(false, maxResults, firstResult);
    }

    private List<OvlascenoLice> findOvlascenoLiceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OvlascenoLice.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OvlascenoLice findOvlascenoLice(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OvlascenoLice.class, id);
        } finally {
            em.close();
        }
    }

    public int getOvlascenoLiceCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OvlascenoLice> rt = cq.from(OvlascenoLice.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<OvlascenoLice> findOvlascenoLiceByDeleted(Boolean deleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("OvlascenoLice.findByIdDeleted")
                    .setParameter("idDeleted", deleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
}
