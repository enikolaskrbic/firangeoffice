/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import entity.Dobavljac;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Kolekcija;
import java.util.ArrayList;
import java.util.Collection;
import entity.Porudzbina;
import entity.OvlascenoLice;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class DobavljacJpaController implements Serializable {

    public DobavljacJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Dobavljac dobavljac) {
        if (dobavljac.getKolekcijaCollection() == null) {
            dobavljac.setKolekcijaCollection(new ArrayList<Kolekcija>());
        }
        if (dobavljac.getPorudzbinaCollection() == null) {
            dobavljac.setPorudzbinaCollection(new ArrayList<Porudzbina>());
        }
        if (dobavljac.getOvlascenoLiceCollection() == null) {
            dobavljac.setOvlascenoLiceCollection(new ArrayList<OvlascenoLice>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Kolekcija> attachedKolekcijaCollection = new ArrayList<Kolekcija>();
            for (Kolekcija kolekcijaCollectionKolekcijaToAttach : dobavljac.getKolekcijaCollection()) {
                kolekcijaCollectionKolekcijaToAttach = em.getReference(kolekcijaCollectionKolekcijaToAttach.getClass(), kolekcijaCollectionKolekcijaToAttach.getIdkolekcija());
                attachedKolekcijaCollection.add(kolekcijaCollectionKolekcijaToAttach);
            }
            dobavljac.setKolekcijaCollection(attachedKolekcijaCollection);
            Collection<Porudzbina> attachedPorudzbinaCollection = new ArrayList<Porudzbina>();
            for (Porudzbina porudzbinaCollectionPorudzbinaToAttach : dobavljac.getPorudzbinaCollection()) {
                porudzbinaCollectionPorudzbinaToAttach = em.getReference(porudzbinaCollectionPorudzbinaToAttach.getClass(), porudzbinaCollectionPorudzbinaToAttach.getIdPorudzbina());
                attachedPorudzbinaCollection.add(porudzbinaCollectionPorudzbinaToAttach);
            }
            dobavljac.setPorudzbinaCollection(attachedPorudzbinaCollection);
            Collection<OvlascenoLice> attachedOvlascenoLiceCollection = new ArrayList<OvlascenoLice>();
            for (OvlascenoLice ovlascenoLiceCollectionOvlascenoLiceToAttach : dobavljac.getOvlascenoLiceCollection()) {
                ovlascenoLiceCollectionOvlascenoLiceToAttach = em.getReference(ovlascenoLiceCollectionOvlascenoLiceToAttach.getClass(), ovlascenoLiceCollectionOvlascenoLiceToAttach.getIdovlascenoLice());
                attachedOvlascenoLiceCollection.add(ovlascenoLiceCollectionOvlascenoLiceToAttach);
            }
            dobavljac.setOvlascenoLiceCollection(attachedOvlascenoLiceCollection);
            em.persist(dobavljac);
            for (Kolekcija kolekcijaCollectionKolekcija : dobavljac.getKolekcijaCollection()) {
                Dobavljac oldDobavljacOfKolekcijaCollectionKolekcija = kolekcijaCollectionKolekcija.getDobavljac();
                kolekcijaCollectionKolekcija.setDobavljac(dobavljac);
                kolekcijaCollectionKolekcija = em.merge(kolekcijaCollectionKolekcija);
                if (oldDobavljacOfKolekcijaCollectionKolekcija != null) {
                    oldDobavljacOfKolekcijaCollectionKolekcija.getKolekcijaCollection().remove(kolekcijaCollectionKolekcija);
                    oldDobavljacOfKolekcijaCollectionKolekcija = em.merge(oldDobavljacOfKolekcijaCollectionKolekcija);
                }
            }
            for (Porudzbina porudzbinaCollectionPorudzbina : dobavljac.getPorudzbinaCollection()) {
                Dobavljac oldDobavljaciddobavljacOfPorudzbinaCollectionPorudzbina = porudzbinaCollectionPorudzbina.getDobavljaciddobavljac();
                porudzbinaCollectionPorudzbina.setDobavljaciddobavljac(dobavljac);
                porudzbinaCollectionPorudzbina = em.merge(porudzbinaCollectionPorudzbina);
                if (oldDobavljaciddobavljacOfPorudzbinaCollectionPorudzbina != null) {
                    oldDobavljaciddobavljacOfPorudzbinaCollectionPorudzbina.getPorudzbinaCollection().remove(porudzbinaCollectionPorudzbina);
                    oldDobavljaciddobavljacOfPorudzbinaCollectionPorudzbina = em.merge(oldDobavljaciddobavljacOfPorudzbinaCollectionPorudzbina);
                }
            }
            for (OvlascenoLice ovlascenoLiceCollectionOvlascenoLice : dobavljac.getOvlascenoLiceCollection()) {
                Dobavljac oldDobavljacOfOvlascenoLiceCollectionOvlascenoLice = ovlascenoLiceCollectionOvlascenoLice.getDobavljac();
                ovlascenoLiceCollectionOvlascenoLice.setDobavljac(dobavljac);
                ovlascenoLiceCollectionOvlascenoLice = em.merge(ovlascenoLiceCollectionOvlascenoLice);
                if (oldDobavljacOfOvlascenoLiceCollectionOvlascenoLice != null) {
                    oldDobavljacOfOvlascenoLiceCollectionOvlascenoLice.getOvlascenoLiceCollection().remove(ovlascenoLiceCollectionOvlascenoLice);
                    oldDobavljacOfOvlascenoLiceCollectionOvlascenoLice = em.merge(oldDobavljacOfOvlascenoLiceCollectionOvlascenoLice);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Dobavljac dobavljac) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dobavljac persistentDobavljac = em.find(Dobavljac.class, dobavljac.getIddobavljac());
            Collection<Kolekcija> kolekcijaCollectionOld = persistentDobavljac.getKolekcijaCollection();
            Collection<Kolekcija> kolekcijaCollectionNew = dobavljac.getKolekcijaCollection();
            Collection<Porudzbina> porudzbinaCollectionOld = persistentDobavljac.getPorudzbinaCollection();
            Collection<Porudzbina> porudzbinaCollectionNew = dobavljac.getPorudzbinaCollection();
            Collection<OvlascenoLice> ovlascenoLiceCollectionOld = persistentDobavljac.getOvlascenoLiceCollection();
            Collection<OvlascenoLice> ovlascenoLiceCollectionNew = dobavljac.getOvlascenoLiceCollection();
            List<String> illegalOrphanMessages = null;
            for (Kolekcija kolekcijaCollectionOldKolekcija : kolekcijaCollectionOld) {
                if (!kolekcijaCollectionNew.contains(kolekcijaCollectionOldKolekcija)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Kolekcija " + kolekcijaCollectionOldKolekcija + " since its dobavljac field is not nullable.");
                }
            }
            for (Porudzbina porudzbinaCollectionOldPorudzbina : porudzbinaCollectionOld) {
                if (!porudzbinaCollectionNew.contains(porudzbinaCollectionOldPorudzbina)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Porudzbina " + porudzbinaCollectionOldPorudzbina + " since its dobavljaciddobavljac field is not nullable.");
                }
            }
            for (OvlascenoLice ovlascenoLiceCollectionOldOvlascenoLice : ovlascenoLiceCollectionOld) {
                if (!ovlascenoLiceCollectionNew.contains(ovlascenoLiceCollectionOldOvlascenoLice)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OvlascenoLice " + ovlascenoLiceCollectionOldOvlascenoLice + " since its dobavljac field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Kolekcija> attachedKolekcijaCollectionNew = new ArrayList<Kolekcija>();
            for (Kolekcija kolekcijaCollectionNewKolekcijaToAttach : kolekcijaCollectionNew) {
                kolekcijaCollectionNewKolekcijaToAttach = em.getReference(kolekcijaCollectionNewKolekcijaToAttach.getClass(), kolekcijaCollectionNewKolekcijaToAttach.getIdkolekcija());
                attachedKolekcijaCollectionNew.add(kolekcijaCollectionNewKolekcijaToAttach);
            }
            kolekcijaCollectionNew = attachedKolekcijaCollectionNew;
            dobavljac.setKolekcijaCollection(kolekcijaCollectionNew);
            Collection<Porudzbina> attachedPorudzbinaCollectionNew = new ArrayList<Porudzbina>();
            for (Porudzbina porudzbinaCollectionNewPorudzbinaToAttach : porudzbinaCollectionNew) {
                porudzbinaCollectionNewPorudzbinaToAttach = em.getReference(porudzbinaCollectionNewPorudzbinaToAttach.getClass(), porudzbinaCollectionNewPorudzbinaToAttach.getIdPorudzbina());
                attachedPorudzbinaCollectionNew.add(porudzbinaCollectionNewPorudzbinaToAttach);
            }
            porudzbinaCollectionNew = attachedPorudzbinaCollectionNew;
            dobavljac.setPorudzbinaCollection(porudzbinaCollectionNew);
            Collection<OvlascenoLice> attachedOvlascenoLiceCollectionNew = new ArrayList<OvlascenoLice>();
            for (OvlascenoLice ovlascenoLiceCollectionNewOvlascenoLiceToAttach : ovlascenoLiceCollectionNew) {
                ovlascenoLiceCollectionNewOvlascenoLiceToAttach = em.getReference(ovlascenoLiceCollectionNewOvlascenoLiceToAttach.getClass(), ovlascenoLiceCollectionNewOvlascenoLiceToAttach.getIdovlascenoLice());
                attachedOvlascenoLiceCollectionNew.add(ovlascenoLiceCollectionNewOvlascenoLiceToAttach);
            }
            ovlascenoLiceCollectionNew = attachedOvlascenoLiceCollectionNew;
            dobavljac.setOvlascenoLiceCollection(ovlascenoLiceCollectionNew);
            dobavljac = em.merge(dobavljac);
            for (Kolekcija kolekcijaCollectionNewKolekcija : kolekcijaCollectionNew) {
                if (!kolekcijaCollectionOld.contains(kolekcijaCollectionNewKolekcija)) {
                    Dobavljac oldDobavljacOfKolekcijaCollectionNewKolekcija = kolekcijaCollectionNewKolekcija.getDobavljac();
                    kolekcijaCollectionNewKolekcija.setDobavljac(dobavljac);
                    kolekcijaCollectionNewKolekcija = em.merge(kolekcijaCollectionNewKolekcija);
                    if (oldDobavljacOfKolekcijaCollectionNewKolekcija != null && !oldDobavljacOfKolekcijaCollectionNewKolekcija.equals(dobavljac)) {
                        oldDobavljacOfKolekcijaCollectionNewKolekcija.getKolekcijaCollection().remove(kolekcijaCollectionNewKolekcija);
                        oldDobavljacOfKolekcijaCollectionNewKolekcija = em.merge(oldDobavljacOfKolekcijaCollectionNewKolekcija);
                    }
                }
            }
            for (Porudzbina porudzbinaCollectionNewPorudzbina : porudzbinaCollectionNew) {
                if (!porudzbinaCollectionOld.contains(porudzbinaCollectionNewPorudzbina)) {
                    Dobavljac oldDobavljaciddobavljacOfPorudzbinaCollectionNewPorudzbina = porudzbinaCollectionNewPorudzbina.getDobavljaciddobavljac();
                    porudzbinaCollectionNewPorudzbina.setDobavljaciddobavljac(dobavljac);
                    porudzbinaCollectionNewPorudzbina = em.merge(porudzbinaCollectionNewPorudzbina);
                    if (oldDobavljaciddobavljacOfPorudzbinaCollectionNewPorudzbina != null && !oldDobavljaciddobavljacOfPorudzbinaCollectionNewPorudzbina.equals(dobavljac)) {
                        oldDobavljaciddobavljacOfPorudzbinaCollectionNewPorudzbina.getPorudzbinaCollection().remove(porudzbinaCollectionNewPorudzbina);
                        oldDobavljaciddobavljacOfPorudzbinaCollectionNewPorudzbina = em.merge(oldDobavljaciddobavljacOfPorudzbinaCollectionNewPorudzbina);
                    }
                }
            }
            for (OvlascenoLice ovlascenoLiceCollectionNewOvlascenoLice : ovlascenoLiceCollectionNew) {
                if (!ovlascenoLiceCollectionOld.contains(ovlascenoLiceCollectionNewOvlascenoLice)) {
                    Dobavljac oldDobavljacOfOvlascenoLiceCollectionNewOvlascenoLice = ovlascenoLiceCollectionNewOvlascenoLice.getDobavljac();
                    ovlascenoLiceCollectionNewOvlascenoLice.setDobavljac(dobavljac);
                    ovlascenoLiceCollectionNewOvlascenoLice = em.merge(ovlascenoLiceCollectionNewOvlascenoLice);
                    if (oldDobavljacOfOvlascenoLiceCollectionNewOvlascenoLice != null && !oldDobavljacOfOvlascenoLiceCollectionNewOvlascenoLice.equals(dobavljac)) {
                        oldDobavljacOfOvlascenoLiceCollectionNewOvlascenoLice.getOvlascenoLiceCollection().remove(ovlascenoLiceCollectionNewOvlascenoLice);
                        oldDobavljacOfOvlascenoLiceCollectionNewOvlascenoLice = em.merge(oldDobavljacOfOvlascenoLiceCollectionNewOvlascenoLice);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = dobavljac.getIddobavljac();
                if (findDobavljac(id) == null) {
                    throw new NonexistentEntityException("The dobavljac with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dobavljac dobavljac;
            try {
                dobavljac = em.getReference(Dobavljac.class, id);
                dobavljac.getIddobavljac();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dobavljac with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Kolekcija> kolekcijaCollectionOrphanCheck = dobavljac.getKolekcijaCollection();
            for (Kolekcija kolekcijaCollectionOrphanCheckKolekcija : kolekcijaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Dobavljac (" + dobavljac + ") cannot be destroyed since the Kolekcija " + kolekcijaCollectionOrphanCheckKolekcija + " in its kolekcijaCollection field has a non-nullable dobavljac field.");
            }
            Collection<Porudzbina> porudzbinaCollectionOrphanCheck = dobavljac.getPorudzbinaCollection();
            for (Porudzbina porudzbinaCollectionOrphanCheckPorudzbina : porudzbinaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Dobavljac (" + dobavljac + ") cannot be destroyed since the Porudzbina " + porudzbinaCollectionOrphanCheckPorudzbina + " in its porudzbinaCollection field has a non-nullable dobavljaciddobavljac field.");
            }
            Collection<OvlascenoLice> ovlascenoLiceCollectionOrphanCheck = dobavljac.getOvlascenoLiceCollection();
            for (OvlascenoLice ovlascenoLiceCollectionOrphanCheckOvlascenoLice : ovlascenoLiceCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Dobavljac (" + dobavljac + ") cannot be destroyed since the OvlascenoLice " + ovlascenoLiceCollectionOrphanCheckOvlascenoLice + " in its ovlascenoLiceCollection field has a non-nullable dobavljac field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(dobavljac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Dobavljac> findDobavljacEntities() {
        return findDobavljacEntities(true, -1, -1);
    }

    public List<Dobavljac> findDobavljacEntities(int maxResults, int firstResult) {
        return findDobavljacEntities(false, maxResults, firstResult);
    }

    private List<Dobavljac> findDobavljacEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Dobavljac.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Dobavljac findDobavljac(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Dobavljac.class, id);
        } finally {
            em.close();
        }
    }

    public int getDobavljacCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Dobavljac> rt = cq.from(Dobavljac.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Dobavljac> findDobavljacByDeleted(boolean deleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Dobavljac.findByIdDeleted")
                    .setParameter("idDeleted", deleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
}
