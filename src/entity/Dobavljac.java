/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "dobavljac")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dobavljac.findAll", query = "SELECT d FROM Dobavljac d"),
    @NamedQuery(name = "Dobavljac.findByIddobavljac", query = "SELECT d FROM Dobavljac d WHERE d.iddobavljac = :iddobavljac"),
    @NamedQuery(name = "Dobavljac.findByNaziv", query = "SELECT d FROM Dobavljac d WHERE d.naziv = :naziv"),
    @NamedQuery(name = "Dobavljac.findByAdresa", query = "SELECT d FROM Dobavljac d WHERE d.adresa = :adresa"),
    @NamedQuery(name = "Dobavljac.findByMesto", query = "SELECT d FROM Dobavljac d WHERE d.mesto = :mesto"),
    @NamedQuery(name = "Dobavljac.findByPib", query = "SELECT d FROM Dobavljac d WHERE d.pib = :pib"),
    @NamedQuery(name = "Dobavljac.findByTelefon", query = "SELECT d FROM Dobavljac d WHERE d.telefon = :telefon"),
    @NamedQuery(name = "Dobavljac.findByEmail", query = "SELECT d FROM Dobavljac d WHERE d.email = :email"),
    @NamedQuery(name = "Dobavljac.findByIdDeleted", query = "SELECT d FROM Dobavljac d WHERE d.idDeleted = :idDeleted")})
public class Dobavljac implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddobavljac")
    private Long iddobavljac;
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "adresa")
    private String adresa;
    @Column(name = "mesto")
    private String mesto;
    @Column(name = "pib")
    private String pib;
    @Column(name = "telefon")
    private String telefon;
    @Column(name = "email")
    private String email;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dobavljac")
    private Collection<Kolekcija> kolekcijaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dobavljaciddobavljac")
    private Collection<Porudzbina> porudzbinaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dobavljac")
    private Collection<OvlascenoLice> ovlascenoLiceCollection;

    public Dobavljac() {
    }

    public Dobavljac(Long iddobavljac) {
        this.iddobavljac = iddobavljac;
    }

    public Long getIddobavljac() {
        return iddobavljac;
    }

    public void setIddobavljac(Long iddobavljac) {
        this.iddobavljac = iddobavljac;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    @XmlTransient
    public Collection<Kolekcija> getKolekcijaCollection() {
        return kolekcijaCollection;
    }

    public void setKolekcijaCollection(Collection<Kolekcija> kolekcijaCollection) {
        this.kolekcijaCollection = kolekcijaCollection;
    }

    @XmlTransient
    public Collection<Porudzbina> getPorudzbinaCollection() {
        return porudzbinaCollection;
    }

    public void setPorudzbinaCollection(Collection<Porudzbina> porudzbinaCollection) {
        this.porudzbinaCollection = porudzbinaCollection;
    }

    @XmlTransient
    public Collection<OvlascenoLice> getOvlascenoLiceCollection() {
        return ovlascenoLiceCollection;
    }

    public void setOvlascenoLiceCollection(Collection<OvlascenoLice> ovlascenoLiceCollection) {
        this.ovlascenoLiceCollection = ovlascenoLiceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddobavljac != null ? iddobavljac.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dobavljac)) {
            return false;
        }
        Dobavljac other = (Dobavljac) object;
        if ((this.iddobavljac == null && other.iddobavljac != null) || (this.iddobavljac != null && !this.iddobavljac.equals(other.iddobavljac))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naziv + " ["+pib+"]";
    }
    
}
