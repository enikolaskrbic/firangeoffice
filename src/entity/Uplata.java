/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "uplata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Uplata.findAll", query = "SELECT u FROM Uplata u"),
    @NamedQuery(name = "Uplata.findByIdUplata", query = "SELECT u FROM Uplata u WHERE u.idUplata = :idUplata"),
    @NamedQuery(name = "Uplata.findByIznos", query = "SELECT u FROM Uplata u WHERE u.iznos = :iznos"),
    @NamedQuery(name = "Uplata.findByDatumUplate", query = "SELECT u FROM Uplata u WHERE u.datumUplate = :datumUplate"),
    @NamedQuery(name = "Uplata.findByIdDeleted", query = "SELECT u FROM Uplata u WHERE u.idDeleted = :idDeleted")})
public class Uplata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUplata")
    private Long idUplata;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "iznos")
    private BigDecimal iznos;
    @Column(name = "datum_uplate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumUplate;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @JoinColumn(name = "id_ponuda", referencedColumnName = "idponuda")
    @ManyToOne(optional = false)
    private Ponuda idPonuda;

    public Uplata() {
    }

    public Uplata(Long idUplata) {
        this.idUplata = idUplata;
    }

    public Long getIdUplata() {
        return idUplata;
    }

    public void setIdUplata(Long idUplata) {
        this.idUplata = idUplata;
    }

    public BigDecimal getIznos() {
        return iznos;
    }

    public void setIznos(BigDecimal iznos) {
        this.iznos = iznos;
    }

    public Date getDatumUplate() {
        return datumUplate;
    }

    public void setDatumUplate(Date datumUplate) {
        this.datumUplate = datumUplate;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public Ponuda getIdPonuda() {
        return idPonuda;
    }

    public void setIdPonuda(Ponuda idPonuda) {
        this.idPonuda = idPonuda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUplata != null ? idUplata.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uplata)) {
            return false;
        }
        Uplata other = (Uplata) object;
        if ((this.idUplata == null && other.idUplata != null) || (this.idUplata != null && !this.idUplata.equals(other.idUplata))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Uplata[ idUplata=" + idUplata + " ]";
    }
    
}
