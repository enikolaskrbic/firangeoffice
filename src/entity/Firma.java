/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "firma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Firma.findAll", query = "SELECT f FROM Firma f"),
    @NamedQuery(name = "Firma.findByIdfirma", query = "SELECT f FROM Firma f WHERE f.idfirma = :idfirma"),
    @NamedQuery(name = "Firma.findByNaziv", query = "SELECT f FROM Firma f WHERE f.naziv = :naziv"),
    @NamedQuery(name = "Firma.findByAdresa", query = "SELECT f FROM Firma f WHERE f.adresa = :adresa"),
    @NamedQuery(name = "Firma.findByMesto", query = "SELECT f FROM Firma f WHERE f.mesto = :mesto"),
    @NamedQuery(name = "Firma.findByTelefon", query = "SELECT f FROM Firma f WHERE f.telefon = :telefon"),
    @NamedQuery(name = "Firma.findByEmail", query = "SELECT f FROM Firma f WHERE f.email = :email"),
    @NamedQuery(name = "Firma.findByWebSajt", query = "SELECT f FROM Firma f WHERE f.webSajt = :webSajt"),
    @NamedQuery(name = "Firma.findByTekuciRacun", query = "SELECT f FROM Firma f WHERE f.tekuciRacun = :tekuciRacun"),
    @NamedQuery(name = "Firma.findByImeBanke", query = "SELECT f FROM Firma f WHERE f.imeBanke = :imeBanke"),
    @NamedQuery(name = "Firma.findByAdresaBanke", query = "SELECT f FROM Firma f WHERE f.adresaBanke = :adresaBanke"),
    @NamedQuery(name = "Firma.findByMaticniBroj", query = "SELECT f FROM Firma f WHERE f.maticniBroj = :maticniBroj"),
    @NamedQuery(name = "Firma.findBySifraDelatnosti", query = "SELECT f FROM Firma f WHERE f.sifraDelatnosti = :sifraDelatnosti"),
    @NamedQuery(name = "Firma.findByPib", query = "SELECT f FROM Firma f WHERE f.pib = :pib"),
    @NamedQuery(name = "Firma.findByPepdv", query = "SELECT f FROM Firma f WHERE f.pepdv = :pepdv"),
    @NamedQuery(name = "Firma.findByRegBr", query = "SELECT f FROM Firma f WHERE f.regBr = :regBr"),
    @NamedQuery(name = "Firma.findByIdDeleted", query = "SELECT f FROM Firma f WHERE f.idDeleted = :idDeleted")})
public class Firma implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfirma")
    private Long idfirma;
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "adresa")
    private String adresa;
    @Column(name = "mesto")
    private String mesto;
    @Column(name = "telefon")
    private String telefon;
    @Column(name = "email")
    private String email;
    @Column(name = "web_sajt")
    private String webSajt;
    @Column(name = "tekuci_racun")
    private String tekuciRacun;
    @Column(name = "ime_banke")
    private String imeBanke;
    @Column(name = "adresa_banke")
    private String adresaBanke;
    @Column(name = "maticni_broj")
    private String maticniBroj;
    @Column(name = "sifra_delatnosti")
    private String sifraDelatnosti;
    @Column(name = "pib")
    private String pib;
    @Column(name = "pepdv")
    private String pepdv;
    @Column(name = "reg_br")
    private String regBr;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "firmaidfirma")
    private Collection<Korisnik> korisnikCollection;

    public Firma() {
    }

    public Firma(Long idfirma) {
        this.idfirma = idfirma;
    }

    public Long getIdfirma() {
        return idfirma;
    }

    public void setIdfirma(Long idfirma) {
        this.idfirma = idfirma;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSajt() {
        return webSajt;
    }

    public void setWebSajt(String webSajt) {
        this.webSajt = webSajt;
    }

    public String getTekuciRacun() {
        return tekuciRacun;
    }

    public void setTekuciRacun(String tekuciRacun) {
        this.tekuciRacun = tekuciRacun;
    }

    public String getImeBanke() {
        return imeBanke;
    }

    public void setImeBanke(String imeBanke) {
        this.imeBanke = imeBanke;
    }

    public String getAdresaBanke() {
        return adresaBanke;
    }

    public void setAdresaBanke(String adresaBanke) {
        this.adresaBanke = adresaBanke;
    }

    public String getMaticniBroj() {
        return maticniBroj;
    }

    public void setMaticniBroj(String maticniBroj) {
        this.maticniBroj = maticniBroj;
    }

    public String getSifraDelatnosti() {
        return sifraDelatnosti;
    }

    public void setSifraDelatnosti(String sifraDelatnosti) {
        this.sifraDelatnosti = sifraDelatnosti;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getPepdv() {
        return pepdv;
    }

    public void setPepdv(String pepdv) {
        this.pepdv = pepdv;
    }

    public String getRegBr() {
        return regBr;
    }

    public void setRegBr(String regBr) {
        this.regBr = regBr;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    @XmlTransient
    public Collection<Korisnik> getKorisnikCollection() {
        return korisnikCollection;
    }

    public void setKorisnikCollection(Collection<Korisnik> korisnikCollection) {
        this.korisnikCollection = korisnikCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfirma != null ? idfirma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Firma)) {
            return false;
        }
        Firma other = (Firma) object;
        if ((this.idfirma == null && other.idfirma != null) || (this.idfirma != null && !this.idfirma.equals(other.idfirma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Firma[ idfirma=" + idfirma + " ]";
    }
    
}
