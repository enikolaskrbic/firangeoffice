/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "stavka_ponude")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StavkaPonude.findAll", query = "SELECT s FROM StavkaPonude s"),
    @NamedQuery(name = "StavkaPonude.findByIdstavka", query = "SELECT s FROM StavkaPonude s WHERE s.idstavka = :idstavka"),
    @NamedQuery(name = "StavkaPonude.findBySifraStavke", query = "SELECT s FROM StavkaPonude s WHERE s.sifraStavke = :sifraStavke"),
    @NamedQuery(name = "StavkaPonude.findByNazivStavke", query = "SELECT s FROM StavkaPonude s WHERE s.nazivStavke = :nazivStavke"),
    @NamedQuery(name = "StavkaPonude.findByKolicina", query = "SELECT s FROM StavkaPonude s WHERE s.kolicina = :kolicina"),
    @NamedQuery(name = "StavkaPonude.findByCenaBezRabat", query = "SELECT s FROM StavkaPonude s WHERE s.cenaBezRabat = :cenaBezRabat"),
    @NamedQuery(name = "StavkaPonude.findByRabatStopa", query = "SELECT s FROM StavkaPonude s WHERE s.rabatStopa = :rabatStopa"),
    @NamedQuery(name = "StavkaPonude.findByCenaRabat", query = "SELECT s FROM StavkaPonude s WHERE s.cenaRabat = :cenaRabat"),
    @NamedQuery(name = "StavkaPonude.findByPoreskaOsonovica", query = "SELECT s FROM StavkaPonude s WHERE s.poreskaOsonovica = :poreskaOsonovica"),
    @NamedQuery(name = "StavkaPonude.findByPoreskaStopa", query = "SELECT s FROM StavkaPonude s WHERE s.poreskaStopa = :poreskaStopa"),
    @NamedQuery(name = "StavkaPonude.findByPdv", query = "SELECT s FROM StavkaPonude s WHERE s.pdv = :pdv"),
    @NamedQuery(name = "StavkaPonude.findByIznos", query = "SELECT s FROM StavkaPonude s WHERE s.iznos = :iznos"),
    @NamedQuery(name = "StavkaPonude.findByJedMereSkraceno", query = "SELECT s FROM StavkaPonude s WHERE s.jedMereSkraceno = :jedMereSkraceno"),
    @NamedQuery(name = "StavkaPonude.findByJedMere", query = "SELECT s FROM StavkaPonude s WHERE s.jedMere = :jedMere"),
    @NamedQuery(name = "StavkaPonude.findByIdDeleted", query = "SELECT s FROM StavkaPonude s WHERE s.idDeleted = :idDeleted"),
    @NamedQuery(name = "StavkaPonude.findByVersion", query = "SELECT s FROM StavkaPonude s WHERE s.version = :version")})
public class StavkaPonude implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idstavka")
    private Long idstavka;
    @Column(name = "sifra_stavke")
    private String sifraStavke;
    @Column(name = "naziv_stavke")
    private String nazivStavke;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "kolicina")
    private BigDecimal kolicina;
    @Column(name = "cena_bez_rabat")
    private BigDecimal cenaBezRabat;
    @Column(name = "rabat_stopa")
    private BigDecimal rabatStopa;
    @Column(name = "cena_rabat")
    private BigDecimal cenaRabat;
    @Column(name = "poreska_osonovica")
    private BigDecimal poreskaOsonovica;
    @Column(name = "poreska_stopa")
    private BigDecimal poreskaStopa;
    @Column(name = "pdv")
    private BigDecimal pdv;
    @Column(name = "iznos")
    private BigDecimal iznos;
    @Column(name = "jed_mere_skraceno")
    private String jedMereSkraceno;
    @Column(name = "jed_mere")
    private String jedMere;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @Version
    @Column(name = "version")
    private Integer version;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stavkaIdstavka")
    private Collection<StavkaPorudzbine> stavkaPorudzbineCollection;
    @JoinColumn(name = "artikl", referencedColumnName = "id_artikl")
    @ManyToOne(optional = false)
    private Artikl artikl;
    @JoinColumn(name = "ponuda", referencedColumnName = "idponuda")
    @ManyToOne(optional = false)
    private Ponuda ponuda;

    public StavkaPonude() {
    }

    public StavkaPonude(Long idstavka) {
        this.idstavka = idstavka;
    }

    public Long getIdstavka() {
        return idstavka;
    }

    public void setIdstavka(Long idstavka) {
        this.idstavka = idstavka;
    }

    public String getSifraStavke() {
        return sifraStavke;
    }

    public void setSifraStavke(String sifraStavke) {
        this.sifraStavke = sifraStavke;
    }

    public String getNazivStavke() {
        return nazivStavke;
    }

    public void setNazivStavke(String nazivStavke) {
        this.nazivStavke = nazivStavke;
    }

    public BigDecimal getKolicina() {
        return kolicina;
    }

    public void setKolicina(BigDecimal kolicina) {
        this.kolicina = kolicina;
    }

    public BigDecimal getCenaBezRabat() {
        return cenaBezRabat;
    }

    public void setCenaBezRabat(BigDecimal cenaBezRabat) {
        this.cenaBezRabat = cenaBezRabat;
    }

    public BigDecimal getRabatStopa() {
        return rabatStopa;
    }

    public void setRabatStopa(BigDecimal rabatStopa) {
        this.rabatStopa = rabatStopa;
    }

    public BigDecimal getCenaRabat() {
        return cenaRabat;
    }

    public void setCenaRabat(BigDecimal cenaRabat) {
        this.cenaRabat = cenaRabat;
    }

    public BigDecimal getPoreskaOsonovica() {
        return poreskaOsonovica;
    }

    public void setPoreskaOsonovica(BigDecimal poreskaOsonovica) {
        this.poreskaOsonovica = poreskaOsonovica;
    }

    public BigDecimal getPoreskaStopa() {
        return poreskaStopa;
    }

    public void setPoreskaStopa(BigDecimal poreskaStopa) {
        this.poreskaStopa = poreskaStopa;
    }

    public BigDecimal getPdv() {
        return pdv;
    }

    public void setPdv(BigDecimal pdv) {
        this.pdv = pdv;
    }

    public BigDecimal getIznos() {
        return iznos;
    }

    public void setIznos(BigDecimal iznos) {
        this.iznos = iznos;
    }

    public String getJedMereSkraceno() {
        return jedMereSkraceno;
    }

    public void setJedMereSkraceno(String jedMereSkraceno) {
        this.jedMereSkraceno = jedMereSkraceno;
    }

    public String getJedMere() {
        return jedMere;
    }

    public void setJedMere(String jedMere) {
        this.jedMere = jedMere;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @XmlTransient
    public Collection<StavkaPorudzbine> getStavkaPorudzbineCollection() {
        return stavkaPorudzbineCollection;
    }

    public void setStavkaPorudzbineCollection(Collection<StavkaPorudzbine> stavkaPorudzbineCollection) {
        this.stavkaPorudzbineCollection = stavkaPorudzbineCollection;
    }

    public Artikl getArtikl() {
        return artikl;
    }

    public void setArtikl(Artikl artikl) {
        this.artikl = artikl;
    }

    public Ponuda getPonuda() {
        return ponuda;
    }

    public void setPonuda(Ponuda ponuda) {
        this.ponuda = ponuda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idstavka != null ? idstavka.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StavkaPonude)) {
            return false;
        }
        StavkaPonude other = (StavkaPonude) object;
        if ((this.idstavka == null && other.idstavka != null) || (this.idstavka != null && !this.idstavka.equals(other.idstavka))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.StavkaPonude[ idstavka=" + idstavka + " ]";
    }
    
}
