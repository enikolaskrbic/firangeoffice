/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "jedinica_mere")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JedinicaMere.findAll", query = "SELECT j FROM JedinicaMere j"),
    @NamedQuery(name = "JedinicaMere.findByIdJedinicaMere", query = "SELECT j FROM JedinicaMere j WHERE j.idJedinicaMere = :idJedinicaMere"),
    @NamedQuery(name = "JedinicaMere.findByNaziv", query = "SELECT j FROM JedinicaMere j WHERE j.naziv = :naziv"),
    @NamedQuery(name = "JedinicaMere.findBySkraceniNaziv", query = "SELECT j FROM JedinicaMere j WHERE j.skraceniNaziv = :skraceniNaziv"),
    @NamedQuery(name = "JedinicaMere.findByIdDeleted", query = "SELECT j FROM JedinicaMere j WHERE j.idDeleted = :idDeleted")})
public class JedinicaMere implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idJedinicaMere")
    private Integer idJedinicaMere;
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "skraceni_naziv")
    private String skraceniNaziv;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @OneToMany(mappedBy = "jedinicaMereidJedinicaMere")
    private Collection<Artikl> artiklCollection;

    public JedinicaMere() {
    }

    public JedinicaMere(Integer idJedinicaMere) {
        this.idJedinicaMere = idJedinicaMere;
    }

    public Integer getIdJedinicaMere() {
        return idJedinicaMere;
    }

    public void setIdJedinicaMere(Integer idJedinicaMere) {
        this.idJedinicaMere = idJedinicaMere;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getSkraceniNaziv() {
        return skraceniNaziv;
    }

    public void setSkraceniNaziv(String skraceniNaziv) {
        this.skraceniNaziv = skraceniNaziv;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    @XmlTransient
    public Collection<Artikl> getArtiklCollection() {
        return artiklCollection;
    }

    public void setArtiklCollection(Collection<Artikl> artiklCollection) {
        this.artiklCollection = artiklCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJedinicaMere != null ? idJedinicaMere.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JedinicaMere)) {
            return false;
        }
        JedinicaMere other = (JedinicaMere) object;
        if ((this.idJedinicaMere == null && other.idJedinicaMere != null) || (this.idJedinicaMere != null && !this.idJedinicaMere.equals(other.idJedinicaMere))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  naziv + " ["+skraceniNaziv+"]";
    }
    
}
