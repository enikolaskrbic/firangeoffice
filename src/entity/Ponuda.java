/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "ponuda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ponuda.findAll", query = "SELECT p FROM Ponuda p"),
    @NamedQuery(name = "Ponuda.findByIdponuda", query = "SELECT p FROM Ponuda p WHERE p.idponuda = :idponuda"),
    @NamedQuery(name = "Ponuda.findByAvans", query = "SELECT p FROM Ponuda p WHERE p.avans = :avans"),
    @NamedQuery(name = "Ponuda.findByUkupno", query = "SELECT p FROM Ponuda p WHERE p.ukupno = :ukupno"),
    @NamedQuery(name = "Ponuda.findByStatus", query = "SELECT p FROM Ponuda p WHERE p.status = :status"),
    @NamedQuery(name = "Ponuda.findByDatum", query = "SELECT p FROM Ponuda p WHERE p.datum = :datum"),
    @NamedQuery(name = "Ponuda.findByAdresa", query = "SELECT p FROM Ponuda p WHERE p.adresa = :adresa"),
    @NamedQuery(name = "Ponuda.findByIsDeleted", query = "SELECT p FROM Ponuda p WHERE p.isDeleted = :isDeleted"),
    @NamedQuery(name = "Ponuda.findByIdDeleted", query = "SELECT p FROM Ponuda p WHERE p.idDeleted = :idDeleted"),
    @NamedQuery(name = "Ponuda.findByKorisnikImePrezime", query = "SELECT p FROM Ponuda p WHERE p.korisnikImePrezime = :korisnikImePrezime"),
    @NamedQuery(name = "Ponuda.findByKupacImePrezime", query = "SELECT p FROM Ponuda p WHERE p.kupacImePrezime = :kupacImePrezime"),
    @NamedQuery(name = "Ponuda.findByIsplacena", query = "SELECT p FROM Ponuda p WHERE p.isplacena = :isplacena"),
    @NamedQuery(name = "Ponuda.findByTip", query = "SELECT p FROM Ponuda p WHERE p.tip = :tip"),
    @NamedQuery(name = "Ponuda.findByVersion", query = "SELECT p FROM Ponuda p WHERE p.version = :version")})
public class Ponuda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idponuda")
    private Long idponuda;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "avans")
    private BigDecimal avans;
    @Column(name = "ukupno")
    private BigDecimal ukupno;
    @Column(name = "status")
    private String status;
    @Column(name = "datum")
    @Temporal(TemporalType.DATE)
    private Date datum;
    @Column(name = "adresa")
    private String adresa;
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @Column(name = "korisnik_ime_prezime")
    private String korisnikImePrezime;
    @Column(name = "kupac_ime_prezime")
    private String kupacImePrezime;
    @Column(name = "isplacena")
    private String isplacena;
    @Column(name = "tip")
    private String tip;
    @Lob
    @Column(name = "napomena")
    private String napomena;
    @Version
    @Column(name = "version")
    private Integer version;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ponudaidponuda")
    private Collection<Porudzbina> porudzbinaCollection;
    @JoinColumn(name = "Korisnik_id_korisnik", referencedColumnName = "id_korisnik")
    @ManyToOne(optional = false)
    private Korisnik korisnikidkorisnik;
    @JoinColumn(name = "Kupac_idkupac", referencedColumnName = "idkupac")
    @ManyToOne(optional = false)
    private Kupac kupacidkupac;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ponuda")
    private Collection<StavkaPonude> stavkaPonudeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPonuda")
    private Collection<Uplata> uplataCollection;

    public Ponuda() {
    }

    public Ponuda(Long idponuda) {
        this.idponuda = idponuda;
    }

    public Long getIdponuda() {
        return idponuda;
    }

    public void setIdponuda(Long idponuda) {
        this.idponuda = idponuda;
    }

    public BigDecimal getAvans() {
        return avans;
    }

    public void setAvans(BigDecimal avans) {
        this.avans = avans;
    }

    public BigDecimal getUkupno() {
        return ukupno;
    }

    public void setUkupno(BigDecimal ukupno) {
        this.ukupno = ukupno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public String getKorisnikImePrezime() {
        return korisnikImePrezime;
    }

    public void setKorisnikImePrezime(String korisnikImePrezime) {
        this.korisnikImePrezime = korisnikImePrezime;
    }

    public String getKupacImePrezime() {
        return kupacImePrezime;
    }

    public void setKupacImePrezime(String kupacImePrezime) {
        this.kupacImePrezime = kupacImePrezime;
    }

    public String getIsplacena() {
        return isplacena;
    }

    public void setIsplacena(String isplacena) {
        this.isplacena = isplacena;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @XmlTransient
    public Collection<Porudzbina> getPorudzbinaCollection() {
        return porudzbinaCollection;
    }

    public void setPorudzbinaCollection(Collection<Porudzbina> porudzbinaCollection) {
        this.porudzbinaCollection = porudzbinaCollection;
    }

    public Korisnik getKorisnikidkorisnik() {
        return korisnikidkorisnik;
    }

    public void setKorisnikidkorisnik(Korisnik korisnikidkorisnik) {
        this.korisnikidkorisnik = korisnikidkorisnik;
    }

    public Kupac getKupacidkupac() {
        return kupacidkupac;
    }

    public void setKupacidkupac(Kupac kupacidkupac) {
        this.kupacidkupac = kupacidkupac;
    }

    @XmlTransient
    public Collection<StavkaPonude> getStavkaPonudeCollection() {
        return stavkaPonudeCollection;
    }

    public void setStavkaPonudeCollection(Collection<StavkaPonude> stavkaPonudeCollection) {
        this.stavkaPonudeCollection = stavkaPonudeCollection;
    }

    @XmlTransient
    public Collection<Uplata> getUplataCollection() {
        return uplataCollection;
    }

    public void setUplataCollection(Collection<Uplata> uplataCollection) {
        this.uplataCollection = uplataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idponuda != null ? idponuda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ponuda)) {
            return false;
        }
        Ponuda other = (Ponuda) object;
        if ((this.idponuda == null && other.idponuda != null) || (this.idponuda != null && !this.idponuda.equals(other.idponuda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Ponuda[ idponuda=" + idponuda + " ]";
    }
    
}
