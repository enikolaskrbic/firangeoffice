/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "porudzbina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Porudzbina.findAll", query = "SELECT p FROM Porudzbina p"),
    @NamedQuery(name = "Porudzbina.findByIdPorudzbina", query = "SELECT p FROM Porudzbina p WHERE p.idPorudzbina = :idPorudzbina"),
    @NamedQuery(name = "Porudzbina.findByDatumKreiranja", query = "SELECT p FROM Porudzbina p WHERE p.datumKreiranja = :datumKreiranja"),
    @NamedQuery(name = "Porudzbina.findByDatumDostave", query = "SELECT p FROM Porudzbina p WHERE p.datumDostave = :datumDostave"),
    @NamedQuery(name = "Porudzbina.findByStatus", query = "SELECT p FROM Porudzbina p WHERE p.status = :status"),
    @NamedQuery(name = "Porudzbina.findByDobavljacNaziv", query = "SELECT p FROM Porudzbina p WHERE p.dobavljacNaziv = :dobavljacNaziv"),
    @NamedQuery(name = "Porudzbina.findByDobavljacPib", query = "SELECT p FROM Porudzbina p WHERE p.dobavljacPib = :dobavljacPib"),
    @NamedQuery(name = "Porudzbina.findByIdDeleted", query = "SELECT p FROM Porudzbina p WHERE p.idDeleted = :idDeleted"),
    @NamedQuery(name = "Porudzbina.findByKorekcija", query = "SELECT p FROM Porudzbina p WHERE p.korekcija = :korekcija"),
    @NamedQuery(name = "Porudzbina.findByVersion", query = "SELECT p FROM Porudzbina p WHERE p.version = :version")})
public class Porudzbina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPorudzbina")
    private Long idPorudzbina;
    @Column(name = "datum_kreiranja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumKreiranja;
    @Column(name = "datum_dostave")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumDostave;
    @Column(name = "status")
    private String status;
    @Column(name = "dobavljac_naziv")
    private String dobavljacNaziv;
    @Column(name = "dobavljac_pib")
    private String dobavljacPib;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @Column(name = "korekcija")
    private Boolean korekcija;
    @Lob
    @Column(name = "korekcijaText")
    private String korekcijaText;
    @Version
    @Column(name = "version")
    private Integer version;
    @JoinColumn(name = "Dobavljac_iddobavljac", referencedColumnName = "iddobavljac")
    @ManyToOne(optional = false)
    private Dobavljac dobavljaciddobavljac;
    @JoinColumn(name = "Ponuda_idponuda", referencedColumnName = "idponuda")
    @ManyToOne(optional = false)
    private Ponuda ponudaidponuda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "porudzbinaidPorudzbina")
    private Collection<StavkaPorudzbine> stavkaPorudzbineCollection;

    public Porudzbina() {
    }

    public Porudzbina(Long idPorudzbina) {
        this.idPorudzbina = idPorudzbina;
    }

    public Long getIdPorudzbina() {
        return idPorudzbina;
    }

    public void setIdPorudzbina(Long idPorudzbina) {
        this.idPorudzbina = idPorudzbina;
    }

    public Date getDatumKreiranja() {
        return datumKreiranja;
    }

    public void setDatumKreiranja(Date datumKreiranja) {
        this.datumKreiranja = datumKreiranja;
    }

    public Date getDatumDostave() {
        return datumDostave;
    }

    public void setDatumDostave(Date datumDostave) {
        this.datumDostave = datumDostave;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDobavljacNaziv() {
        return dobavljacNaziv;
    }

    public void setDobavljacNaziv(String dobavljacNaziv) {
        this.dobavljacNaziv = dobavljacNaziv;
    }

    public String getDobavljacPib() {
        return dobavljacPib;
    }

    public void setDobavljacPib(String dobavljacPib) {
        this.dobavljacPib = dobavljacPib;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public Boolean getKorekcija() {
        return korekcija;
    }

    public void setKorekcija(Boolean korekcija) {
        this.korekcija = korekcija;
    }

    public String getKorekcijaText() {
        return korekcijaText;
    }

    public void setKorekcijaText(String korekcijaText) {
        this.korekcijaText = korekcijaText;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Dobavljac getDobavljaciddobavljac() {
        return dobavljaciddobavljac;
    }

    public void setDobavljaciddobavljac(Dobavljac dobavljaciddobavljac) {
        this.dobavljaciddobavljac = dobavljaciddobavljac;
    }

    public Ponuda getPonudaidponuda() {
        return ponudaidponuda;
    }

    public void setPonudaidponuda(Ponuda ponudaidponuda) {
        this.ponudaidponuda = ponudaidponuda;
    }

    @XmlTransient
    public Collection<StavkaPorudzbine> getStavkaPorudzbineCollection() {
        return stavkaPorudzbineCollection;
    }

    public void setStavkaPorudzbineCollection(Collection<StavkaPorudzbine> stavkaPorudzbineCollection) {
        this.stavkaPorudzbineCollection = stavkaPorudzbineCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPorudzbina != null ? idPorudzbina.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Porudzbina)) {
            return false;
        }
        Porudzbina other = (Porudzbina) object;
        if ((this.idPorudzbina == null && other.idPorudzbina != null) || (this.idPorudzbina != null && !this.idPorudzbina.equals(other.idPorudzbina))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Porudzbina[ idPorudzbina=" + idPorudzbina + " ]";
    }
    
}
