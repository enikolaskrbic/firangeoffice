package loger;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.ZonedDateTime;

/**
 * Created by Skrbic
 */

public class LoggerUtil {
    private static final String FILENAME_INFO = "logInfo.txt";
    private static final String FILENAME_ERROR= "logError.txt";
    
    
    public static void logInfo(String data,String username,String metoda,String k){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(FILENAME_INFO);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            String time = "\n[INFO] time|" + ZonedDateTime.now().toString();
            String user = " user|"+username;
            String text = " data|"+ data;
            String method = " method|"+metoda;
            String klasa = " class|" + k;
            String log = time +klasa+method+user+text;
            bw.write(log +  "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public static void logError(String data,String username,String metoda,String k){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(FILENAME_ERROR);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            String time = "\n[ERROR] time|" + ZonedDateTime.now().toString();
            String user = " user|"+username;
            String method = " method|"+metoda;
            String text = " data|"+ data;
            String klasa = " class|" + k;
            String log = time +user+klasa+method+text;
            bw.write(log +  "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
