/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import javafx.stage.Stage;

/**
 * This interface represent method for setting parent stage.
 * All fxml controllers must implement this interface in order to be display and be able to display other fxmls.
 * @author Igor
 */
public interface ControlledScreen {
    /**
     * Sets parent of stage
     * @param stage 
     */
    public void setStage(Stage stage);
    /**
     * 
     * @return 
     */
    public boolean isOkClicked();

}
