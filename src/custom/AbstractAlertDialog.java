/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;

/**
 *
 * @author Skrbic
 */
public class AbstractAlertDialog {
    private Alert alert;
    private Optional<ButtonType> result;
    
    public AbstractAlertDialog(String title,String header, String content, AlertType alertType) {
        alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        
        DialogPane dialogPane = alert.getDialogPane();
        
        dialogPane.getStylesheets().add(
        getClass().getResource("/css/lawOfficeMetro.css").toExternalForm());
        dialogPane.getStyleClass().add("dialog-background");
        
        result = alert.showAndWait();
    }

    public Alert getAlert() {
        return alert;
    }
    public Optional<ButtonType> getResult() {
        return result;
    }
    
    
    
    
    
    
    
    
}
