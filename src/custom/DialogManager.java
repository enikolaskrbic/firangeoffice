package custom;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static firangeApp.Firange.fl;

/**
 * Represents main dialog manger of the applications. Used to instance new
 * stage.
 *
 * @author Igor
 */
public class DialogManager {

   
    private Stage modal_dialog;
    private Scene scene;
    private final String name;
    private final Stage owner;

    public DialogManager(String name, Stage owner) {
        this.name = name;
        this.owner = owner;
        
    }

    /**
     * Create new stage
     *
     * @return ControlledScreen that represent controller class. To used you
     * must cast to appropriate controller class
     */
    public AbstracController setDialog(boolean update, Object obj) {

        fl = new FXMLLoader();
        fl.setLocation(getClass().getResource(name));
        try {
            fl.load();
        } catch (IOException ex) {
            Logger.getLogger(DialogManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        Parent root = fl.getRoot();
        modal_dialog = new Stage(StageStyle.DECORATED);
        modal_dialog.initModality(Modality.WINDOW_MODAL);
        modal_dialog.initOwner(owner);
        modal_dialog.setResizable(false);
        scene = new Scene(root);

        AbstracController cs = (AbstracController) fl.getController();
        cs.setStage(modal_dialog);
        cs.initialize(update,obj);
        modal_dialog.setScene(scene);
        modal_dialog.showAndWait();
        return cs;
    }  
}
