/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package privilege;

import controller.MainFrameController;
import entity.Korisnik;
import entity.Ponuda;
import enums.Role;
import enums.StatusPonude;

/**
 *
 * @author Skrbic
 */
public class PrivilegeUtil {

    public static boolean checkPrivilege(Ponuda ponuda) {
        Korisnik korisnik = MainFrameController.korisnik;
        String rola = korisnik.getRole();
        boolean retVal = false;
        switch (ponuda.getStatus()) {
            case "NA_CEKANJU_KUPCA":
                if (rola.equals(Role.SUPERADMIN.toString())
                        || rola.equals(Role.MENADZER.toString())
                        || rola.equals(Role.ZAPOSLEN.toString())) {
                    retVal = true;
                }
                break;
            case "PRIHVACENA_PONUDA":
                if (rola.equals(Role.SUPERADMIN.toString())
                        || rola.equals(Role.MENADZER.toString())
                        || rola.equals(Role.ZAPOSLEN.toString())) {
                    retVal = true;
                }
                break;
        }

        return retVal;
    }
  
    
    
}
