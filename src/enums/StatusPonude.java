/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enums;

/**
 *
 * @author Skrbic
 */
public enum StatusPonude {
    NA_CEKANJU_KUPCA,PRIHVACENA_PONUDA,NA_CEKANJU_DOBAVLJACA,PORUDZBINE_KREIRANE,
    DOSTAVLJA_SE,ROBA_PRIMLJENA,ROBA_SASIVENA,ROBA_NA_MONTAZI,ROBA_NA_SIVENJU,ROBA_SE_MONTIRA,
    ZAVRSENA,
    KUPAC_ODBIO,DOBAVLJAC_ODBIO,
    STORNIRANO
}
