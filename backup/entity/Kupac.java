/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "kupac")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kupac.findAll", query = "SELECT k FROM Kupac k"),
    @NamedQuery(name = "Kupac.findByIdkupac", query = "SELECT k FROM Kupac k WHERE k.idkupac = :idkupac"),
    @NamedQuery(name = "Kupac.findByIme", query = "SELECT k FROM Kupac k WHERE k.ime = :ime"),
    @NamedQuery(name = "Kupac.findByPrezime", query = "SELECT k FROM Kupac k WHERE k.prezime = :prezime"),
    @NamedQuery(name = "Kupac.findByAdresa", query = "SELECT k FROM Kupac k WHERE k.adresa = :adresa"),
    @NamedQuery(name = "Kupac.findByTelefon", query = "SELECT k FROM Kupac k WHERE k.telefon = :telefon"),
    @NamedQuery(name = "Kupac.findByEmail", query = "SELECT k FROM Kupac k WHERE k.email = :email"),
    @NamedQuery(name = "Kupac.findByStatus", query = "SELECT k FROM Kupac k WHERE k.status = :status"),
    @NamedQuery(name = "Kupac.findByPib", query = "SELECT k FROM Kupac k WHERE k.pib = :pib"),
    @NamedQuery(name = "Kupac.findByNaziv", query = "SELECT k FROM Kupac k WHERE k.naziv = :naziv"),
    @NamedQuery(name = "Kupac.findByIdDeleted", query = "SELECT k FROM Kupac k WHERE k.idDeleted = :idDeleted")})
public class Kupac implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idkupac")
    private Long idkupac;
    @Basic(optional = false)
    @Column(name = "ime")
    private String ime;
    @Basic(optional = false)
    @Column(name = "prezime")
    private String prezime;
    @Column(name = "adresa")
    private String adresa;
    @Column(name = "telefon")
    private String telefon;
    @Column(name = "email")
    private String email;
    @Column(name = "status")
    private String status;
    @Column(name = "pib")
    private String pib;
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kupacidkupac")
    private Collection<Ponuda> ponudaCollection;

    public Kupac() {
    }

    public Kupac(Long idkupac) {
        this.idkupac = idkupac;
    }

    public Kupac(Long idkupac, String ime, String prezime) {
        this.idkupac = idkupac;
        this.ime = ime;
        this.prezime = prezime;
    }

    public Long getIdkupac() {
        return idkupac;
    }

    public void setIdkupac(Long idkupac) {
        this.idkupac = idkupac;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    @XmlTransient
    public Collection<Ponuda> getPonudaCollection() {
        return ponudaCollection;
    }

    public void setPonudaCollection(Collection<Ponuda> ponudaCollection) {
        this.ponudaCollection = ponudaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idkupac != null ? idkupac.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kupac)) {
            return false;
        }
        Kupac other = (Kupac) object;
        if ((this.idkupac == null && other.idkupac != null) || (this.idkupac != null && !this.idkupac.equals(other.idkupac))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Kupac[ idkupac=" + idkupac + " ]";
    }
    
}
