/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "stavka_porudzbine")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StavkaPorudzbine.findAll", query = "SELECT s FROM StavkaPorudzbine s"),
    @NamedQuery(name = "StavkaPorudzbine.findByIdStavkaPorudzbine", query = "SELECT s FROM StavkaPorudzbine s WHERE s.idStavkaPorudzbine = :idStavkaPorudzbine"),
    @NamedQuery(name = "StavkaPorudzbine.findByKolicina", query = "SELECT s FROM StavkaPorudzbine s WHERE s.kolicina = :kolicina"),
    @NamedQuery(name = "StavkaPorudzbine.findByNazivStavke", query = "SELECT s FROM StavkaPorudzbine s WHERE s.nazivStavke = :nazivStavke"),
    @NamedQuery(name = "StavkaPorudzbine.findBySifraStavke", query = "SELECT s FROM StavkaPorudzbine s WHERE s.sifraStavke = :sifraStavke"),
    @NamedQuery(name = "StavkaPorudzbine.findByDatumDostave", query = "SELECT s FROM StavkaPorudzbine s WHERE s.datumDostave = :datumDostave"),
    @NamedQuery(name = "StavkaPorudzbine.findByIdDeleted", query = "SELECT s FROM StavkaPorudzbine s WHERE s.idDeleted = :idDeleted")})
public class StavkaPorudzbine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idStavkaPorudzbine")
    private Long idStavkaPorudzbine;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "kolicina")
    private BigDecimal kolicina;
    @Column(name = "naziv_stavke")
    private String nazivStavke;
    @Column(name = "sifra_stavke")
    private String sifraStavke;
    @Column(name = "datum_dostave")
    @Temporal(TemporalType.DATE)
    private Date datumDostave;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @JoinColumn(name = "Porudzbina_idPorudzbina", referencedColumnName = "idPorudzbina")
    @ManyToOne(optional = false)
    private Porudzbina porudzbinaidPorudzbina;
    @JoinColumn(name = "stavka_idstavka", referencedColumnName = "idstavka")
    @ManyToOne(optional = false)
    private StavkaPonude stavkaIdstavka;

    public StavkaPorudzbine() {
    }

    public StavkaPorudzbine(Long idStavkaPorudzbine) {
        this.idStavkaPorudzbine = idStavkaPorudzbine;
    }

    public Long getIdStavkaPorudzbine() {
        return idStavkaPorudzbine;
    }

    public void setIdStavkaPorudzbine(Long idStavkaPorudzbine) {
        this.idStavkaPorudzbine = idStavkaPorudzbine;
    }

    public BigDecimal getKolicina() {
        return kolicina;
    }

    public void setKolicina(BigDecimal kolicina) {
        this.kolicina = kolicina;
    }

    public String getNazivStavke() {
        return nazivStavke;
    }

    public void setNazivStavke(String nazivStavke) {
        this.nazivStavke = nazivStavke;
    }

    public String getSifraStavke() {
        return sifraStavke;
    }

    public void setSifraStavke(String sifraStavke) {
        this.sifraStavke = sifraStavke;
    }

    public Date getDatumDostave() {
        return datumDostave;
    }

    public void setDatumDostave(Date datumDostave) {
        this.datumDostave = datumDostave;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public Porudzbina getPorudzbinaidPorudzbina() {
        return porudzbinaidPorudzbina;
    }

    public void setPorudzbinaidPorudzbina(Porudzbina porudzbinaidPorudzbina) {
        this.porudzbinaidPorudzbina = porudzbinaidPorudzbina;
    }

    public StavkaPonude getStavkaIdstavka() {
        return stavkaIdstavka;
    }

    public void setStavkaIdstavka(StavkaPonude stavkaIdstavka) {
        this.stavkaIdstavka = stavkaIdstavka;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStavkaPorudzbine != null ? idStavkaPorudzbine.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StavkaPorudzbine)) {
            return false;
        }
        StavkaPorudzbine other = (StavkaPorudzbine) object;
        if ((this.idStavkaPorudzbine == null && other.idStavkaPorudzbine != null) || (this.idStavkaPorudzbine != null && !this.idStavkaPorudzbine.equals(other.idStavkaPorudzbine))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.StavkaPorudzbine[ idStavkaPorudzbine=" + idStavkaPorudzbine + " ]";
    }
    
}
