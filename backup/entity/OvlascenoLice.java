/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "ovlasceno_lice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OvlascenoLice.findAll", query = "SELECT o FROM OvlascenoLice o"),
    @NamedQuery(name = "OvlascenoLice.findByIdovlascenoLice", query = "SELECT o FROM OvlascenoLice o WHERE o.idovlascenoLice = :idovlascenoLice"),
    @NamedQuery(name = "OvlascenoLice.findByIme", query = "SELECT o FROM OvlascenoLice o WHERE o.ime = :ime"),
    @NamedQuery(name = "OvlascenoLice.findByPrezime", query = "SELECT o FROM OvlascenoLice o WHERE o.prezime = :prezime"),
    @NamedQuery(name = "OvlascenoLice.findByIdDeleted", query = "SELECT o FROM OvlascenoLice o WHERE o.idDeleted = :idDeleted")})
public class OvlascenoLice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idovlasceno_lice")
    private Long idovlascenoLice;
    @Basic(optional = false)
    @Column(name = "ime")
    private String ime;
    @Basic(optional = false)
    @Column(name = "prezime")
    private String prezime;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @JoinColumn(name = "dobavljac", referencedColumnName = "iddobavljac")
    @ManyToOne(optional = false)
    private Dobavljac dobavljac;

    public OvlascenoLice() {
    }

    public OvlascenoLice(Long idovlascenoLice) {
        this.idovlascenoLice = idovlascenoLice;
    }

    public OvlascenoLice(Long idovlascenoLice, String ime, String prezime) {
        this.idovlascenoLice = idovlascenoLice;
        this.ime = ime;
        this.prezime = prezime;
    }

    public Long getIdovlascenoLice() {
        return idovlascenoLice;
    }

    public void setIdovlascenoLice(Long idovlascenoLice) {
        this.idovlascenoLice = idovlascenoLice;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public Dobavljac getDobavljac() {
        return dobavljac;
    }

    public void setDobavljac(Dobavljac dobavljac) {
        this.dobavljac = dobavljac;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idovlascenoLice != null ? idovlascenoLice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OvlascenoLice)) {
            return false;
        }
        OvlascenoLice other = (OvlascenoLice) object;
        if ((this.idovlascenoLice == null && other.idovlascenoLice != null) || (this.idovlascenoLice != null && !this.idovlascenoLice.equals(other.idovlascenoLice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.OvlascenoLice[ idovlascenoLice=" + idovlascenoLice + " ]";
    }
    
}
