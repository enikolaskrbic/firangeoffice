/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "artikl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Artikl.findAll", query = "SELECT a FROM Artikl a"),
    @NamedQuery(name = "Artikl.findByIdArtikl", query = "SELECT a FROM Artikl a WHERE a.idArtikl = :idArtikl"),
    @NamedQuery(name = "Artikl.findByNaziv", query = "SELECT a FROM Artikl a WHERE a.naziv = :naziv"),
    @NamedQuery(name = "Artikl.findBySifra", query = "SELECT a FROM Artikl a WHERE a.sifra = :sifra"),
    @NamedQuery(name = "Artikl.findByCena", query = "SELECT a FROM Artikl a WHERE a.cena = :cena"),
    @NamedQuery(name = "Artikl.findByStatus", query = "SELECT a FROM Artikl a WHERE a.status = :status"),
    @NamedQuery(name = "Artikl.findByTip", query = "SELECT a FROM Artikl a WHERE a.tip = :tip and a.idDeleted = :idDeleted"),
    @NamedQuery(name = "Artikl.findByIdDeleted", query = "SELECT a FROM Artikl a WHERE a.idDeleted = :idDeleted")})
public class Artikl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_artikl")
    private Long idArtikl;
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "sifra")
    private String sifra;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cena")
    private BigDecimal cena;
    @Column(name = "status")
    private String status;
    @Column(name = "tip")
    private String tip;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @JoinColumn(name = "JedinicaMere_idJedinicaMere", referencedColumnName = "idJedinicaMere")
    @ManyToOne
    private JedinicaMere jedinicaMereidJedinicaMere;
    @JoinColumn(name = "kolekcija_idkolekcija", referencedColumnName = "idkolekcija")
    @ManyToOne
    private Kolekcija kolekcijaIdkolekcija;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "artikl")
    private Collection<StavkaPonude> stavkaPonudeCollection;

    public Artikl() {
    }

    public Artikl(Long idArtikl) {
        this.idArtikl = idArtikl;
    }

    public Long getIdArtikl() {
        return idArtikl;
    }

    public void setIdArtikl(Long idArtikl) {
        this.idArtikl = idArtikl;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public JedinicaMere getJedinicaMereidJedinicaMere() {
        return jedinicaMereidJedinicaMere;
    }

    public void setJedinicaMereidJedinicaMere(JedinicaMere jedinicaMereidJedinicaMere) {
        this.jedinicaMereidJedinicaMere = jedinicaMereidJedinicaMere;
    }

    public Kolekcija getKolekcijaIdkolekcija() {
        return kolekcijaIdkolekcija;
    }

    public void setKolekcijaIdkolekcija(Kolekcija kolekcijaIdkolekcija) {
        this.kolekcijaIdkolekcija = kolekcijaIdkolekcija;
    }

    @XmlTransient
    public Collection<StavkaPonude> getStavkaPonudeCollection() {
        return stavkaPonudeCollection;
    }

    public void setStavkaPonudeCollection(Collection<StavkaPonude> stavkaPonudeCollection) {
        this.stavkaPonudeCollection = stavkaPonudeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArtikl != null ? idArtikl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Artikl)) {
            return false;
        }
        Artikl other = (Artikl) object;
        if ((this.idArtikl == null && other.idArtikl != null) || (this.idArtikl != null && !this.idArtikl.equals(other.idArtikl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Artikl[ idArtikl=" + idArtikl + " ]";
    }
    
}
