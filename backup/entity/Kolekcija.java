/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Skrbic
 */
@Entity
@Table(name = "kolekcija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kolekcija.findAll", query = "SELECT k FROM Kolekcija k"),
    @NamedQuery(name = "Kolekcija.findByIdkolekcija", query = "SELECT k FROM Kolekcija k WHERE k.idkolekcija = :idkolekcija"),
    @NamedQuery(name = "Kolekcija.findByNaziv", query = "SELECT k FROM Kolekcija k WHERE k.naziv = :naziv"),
    @NamedQuery(name = "Kolekcija.findByDaniIsporuke", query = "SELECT k FROM Kolekcija k WHERE k.daniIsporuke = :daniIsporuke"),
    @NamedQuery(name = "Kolekcija.findByOpis", query = "SELECT k FROM Kolekcija k WHERE k.opis = :opis"),
    @NamedQuery(name = "Kolekcija.findByIdDeleted", query = "SELECT k FROM Kolekcija k WHERE k.idDeleted = :idDeleted")})
public class Kolekcija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idkolekcija")
    private Long idkolekcija;
    @Basic(optional = false)
    @Column(name = "naziv")
    private String naziv;
    @Basic(optional = false)
    @Column(name = "dani_isporuke")
    private int daniIsporuke;
    @Column(name = "opis")
    private String opis;
    @Column(name = "idDeleted")
    private Boolean idDeleted;
    @JoinColumn(name = "dobavljac", referencedColumnName = "iddobavljac")
    @ManyToOne(optional = false)
    private Dobavljac dobavljac;
    @OneToMany(mappedBy = "kolekcijaIdkolekcija")
    private Collection<Artikl> artiklCollection;

    public Kolekcija() {
    }

    public Kolekcija(Long idkolekcija) {
        this.idkolekcija = idkolekcija;
    }

    public Kolekcija(Long idkolekcija, String naziv, int daniIsporuke) {
        this.idkolekcija = idkolekcija;
        this.naziv = naziv;
        this.daniIsporuke = daniIsporuke;
    }

    public Long getIdkolekcija() {
        return idkolekcija;
    }

    public void setIdkolekcija(Long idkolekcija) {
        this.idkolekcija = idkolekcija;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getDaniIsporuke() {
        return daniIsporuke;
    }

    public void setDaniIsporuke(int daniIsporuke) {
        this.daniIsporuke = daniIsporuke;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Boolean getIdDeleted() {
        return idDeleted;
    }

    public void setIdDeleted(Boolean idDeleted) {
        this.idDeleted = idDeleted;
    }

    public Dobavljac getDobavljac() {
        return dobavljac;
    }

    public void setDobavljac(Dobavljac dobavljac) {
        this.dobavljac = dobavljac;
    }

    @XmlTransient
    public Collection<Artikl> getArtiklCollection() {
        return artiklCollection;
    }

    public void setArtiklCollection(Collection<Artikl> artiklCollection) {
        this.artiklCollection = artiklCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idkolekcija != null ? idkolekcija.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kolekcija)) {
            return false;
        }
        Kolekcija other = (Kolekcija) object;
        if ((this.idkolekcija == null && other.idkolekcija != null) || (this.idkolekcija != null && !this.idkolekcija.equals(other.idkolekcija))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naziv;
    }
    
}
