/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import entity.Kupac;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Ponuda;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class KupacJpaController implements Serializable {

    public KupacJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Kupac kupac) {
        if (kupac.getPonudaCollection() == null) {
            kupac.setPonudaCollection(new ArrayList<Ponuda>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Ponuda> attachedPonudaCollection = new ArrayList<Ponuda>();
            for (Ponuda ponudaCollectionPonudaToAttach : kupac.getPonudaCollection()) {
                ponudaCollectionPonudaToAttach = em.getReference(ponudaCollectionPonudaToAttach.getClass(), ponudaCollectionPonudaToAttach.getIdponuda());
                attachedPonudaCollection.add(ponudaCollectionPonudaToAttach);
            }
            kupac.setPonudaCollection(attachedPonudaCollection);
            em.persist(kupac);
            for (Ponuda ponudaCollectionPonuda : kupac.getPonudaCollection()) {
                Kupac oldKupacidkupacOfPonudaCollectionPonuda = ponudaCollectionPonuda.getKupacidkupac();
                ponudaCollectionPonuda.setKupacidkupac(kupac);
                ponudaCollectionPonuda = em.merge(ponudaCollectionPonuda);
                if (oldKupacidkupacOfPonudaCollectionPonuda != null) {
                    oldKupacidkupacOfPonudaCollectionPonuda.getPonudaCollection().remove(ponudaCollectionPonuda);
                    oldKupacidkupacOfPonudaCollectionPonuda = em.merge(oldKupacidkupacOfPonudaCollectionPonuda);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Kupac kupac) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Kupac persistentKupac = em.find(Kupac.class, kupac.getIdkupac());
            Collection<Ponuda> ponudaCollectionOld = persistentKupac.getPonudaCollection();
            Collection<Ponuda> ponudaCollectionNew = kupac.getPonudaCollection();
            List<String> illegalOrphanMessages = null;
            for (Ponuda ponudaCollectionOldPonuda : ponudaCollectionOld) {
                if (!ponudaCollectionNew.contains(ponudaCollectionOldPonuda)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ponuda " + ponudaCollectionOldPonuda + " since its kupacidkupac field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Ponuda> attachedPonudaCollectionNew = new ArrayList<Ponuda>();
            for (Ponuda ponudaCollectionNewPonudaToAttach : ponudaCollectionNew) {
                ponudaCollectionNewPonudaToAttach = em.getReference(ponudaCollectionNewPonudaToAttach.getClass(), ponudaCollectionNewPonudaToAttach.getIdponuda());
                attachedPonudaCollectionNew.add(ponudaCollectionNewPonudaToAttach);
            }
            ponudaCollectionNew = attachedPonudaCollectionNew;
            kupac.setPonudaCollection(ponudaCollectionNew);
            kupac = em.merge(kupac);
            for (Ponuda ponudaCollectionNewPonuda : ponudaCollectionNew) {
                if (!ponudaCollectionOld.contains(ponudaCollectionNewPonuda)) {
                    Kupac oldKupacidkupacOfPonudaCollectionNewPonuda = ponudaCollectionNewPonuda.getKupacidkupac();
                    ponudaCollectionNewPonuda.setKupacidkupac(kupac);
                    ponudaCollectionNewPonuda = em.merge(ponudaCollectionNewPonuda);
                    if (oldKupacidkupacOfPonudaCollectionNewPonuda != null && !oldKupacidkupacOfPonudaCollectionNewPonuda.equals(kupac)) {
                        oldKupacidkupacOfPonudaCollectionNewPonuda.getPonudaCollection().remove(ponudaCollectionNewPonuda);
                        oldKupacidkupacOfPonudaCollectionNewPonuda = em.merge(oldKupacidkupacOfPonudaCollectionNewPonuda);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = kupac.getIdkupac();
                if (findKupac(id) == null) {
                    throw new NonexistentEntityException("The kupac with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Kupac kupac;
            try {
                kupac = em.getReference(Kupac.class, id);
                kupac.getIdkupac();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The kupac with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Ponuda> ponudaCollectionOrphanCheck = kupac.getPonudaCollection();
            for (Ponuda ponudaCollectionOrphanCheckPonuda : ponudaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Kupac (" + kupac + ") cannot be destroyed since the Ponuda " + ponudaCollectionOrphanCheckPonuda + " in its ponudaCollection field has a non-nullable kupacidkupac field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(kupac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Kupac> findKupacEntities() {
        return findKupacEntities(true, -1, -1);
    }

    public List<Kupac> findKupacEntities(int maxResults, int firstResult) {
        return findKupacEntities(false, maxResults, firstResult);
    }

    private List<Kupac> findKupacEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Kupac.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Kupac findKupac(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Kupac.class, id);
        } finally {
            em.close();
        }
    }

    public int getKupacCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Kupac> rt = cq.from(Kupac.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<Kupac> findKupacByDeleted(boolean deleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Kupac.findByIdDeleted")
                    .setParameter("idDeleted", deleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
}
