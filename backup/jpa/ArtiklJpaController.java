/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import entity.Artikl;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.JedinicaMere;
import entity.Kolekcija;
import entity.StavkaPonude;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class ArtiklJpaController implements Serializable {

    public ArtiklJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Artikl artikl) {
        if (artikl.getStavkaPonudeCollection() == null) {
            artikl.setStavkaPonudeCollection(new ArrayList<StavkaPonude>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            JedinicaMere jedinicaMereidJedinicaMere = artikl.getJedinicaMereidJedinicaMere();
            if (jedinicaMereidJedinicaMere != null) {
                jedinicaMereidJedinicaMere = em.getReference(jedinicaMereidJedinicaMere.getClass(), jedinicaMereidJedinicaMere.getIdJedinicaMere());
                artikl.setJedinicaMereidJedinicaMere(jedinicaMereidJedinicaMere);
            }
            Kolekcija kolekcijaIdkolekcija = artikl.getKolekcijaIdkolekcija();
            if (kolekcijaIdkolekcija != null) {
                kolekcijaIdkolekcija = em.getReference(kolekcijaIdkolekcija.getClass(), kolekcijaIdkolekcija.getIdkolekcija());
                artikl.setKolekcijaIdkolekcija(kolekcijaIdkolekcija);
            }
            Collection<StavkaPonude> attachedStavkaPonudeCollection = new ArrayList<StavkaPonude>();
            for (StavkaPonude stavkaPonudeCollectionStavkaPonudeToAttach : artikl.getStavkaPonudeCollection()) {
                stavkaPonudeCollectionStavkaPonudeToAttach = em.getReference(stavkaPonudeCollectionStavkaPonudeToAttach.getClass(), stavkaPonudeCollectionStavkaPonudeToAttach.getIdstavka());
                attachedStavkaPonudeCollection.add(stavkaPonudeCollectionStavkaPonudeToAttach);
            }
            artikl.setStavkaPonudeCollection(attachedStavkaPonudeCollection);
            em.persist(artikl);
            if (jedinicaMereidJedinicaMere != null) {
                jedinicaMereidJedinicaMere.getArtiklCollection().add(artikl);
                jedinicaMereidJedinicaMere = em.merge(jedinicaMereidJedinicaMere);
            }
            if (kolekcijaIdkolekcija != null) {
                kolekcijaIdkolekcija.getArtiklCollection().add(artikl);
                kolekcijaIdkolekcija = em.merge(kolekcijaIdkolekcija);
            }
            for (StavkaPonude stavkaPonudeCollectionStavkaPonude : artikl.getStavkaPonudeCollection()) {
                Artikl oldArtiklOfStavkaPonudeCollectionStavkaPonude = stavkaPonudeCollectionStavkaPonude.getArtikl();
                stavkaPonudeCollectionStavkaPonude.setArtikl(artikl);
                stavkaPonudeCollectionStavkaPonude = em.merge(stavkaPonudeCollectionStavkaPonude);
                if (oldArtiklOfStavkaPonudeCollectionStavkaPonude != null) {
                    oldArtiklOfStavkaPonudeCollectionStavkaPonude.getStavkaPonudeCollection().remove(stavkaPonudeCollectionStavkaPonude);
                    oldArtiklOfStavkaPonudeCollectionStavkaPonude = em.merge(oldArtiklOfStavkaPonudeCollectionStavkaPonude);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Artikl artikl) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Artikl persistentArtikl = em.find(Artikl.class, artikl.getIdArtikl());
            JedinicaMere jedinicaMereidJedinicaMereOld = persistentArtikl.getJedinicaMereidJedinicaMere();
            JedinicaMere jedinicaMereidJedinicaMereNew = artikl.getJedinicaMereidJedinicaMere();
            Kolekcija kolekcijaIdkolekcijaOld = persistentArtikl.getKolekcijaIdkolekcija();
            Kolekcija kolekcijaIdkolekcijaNew = artikl.getKolekcijaIdkolekcija();
            Collection<StavkaPonude> stavkaPonudeCollectionOld = persistentArtikl.getStavkaPonudeCollection();
            Collection<StavkaPonude> stavkaPonudeCollectionNew = artikl.getStavkaPonudeCollection();
            List<String> illegalOrphanMessages = null;
            for (StavkaPonude stavkaPonudeCollectionOldStavkaPonude : stavkaPonudeCollectionOld) {
                if (!stavkaPonudeCollectionNew.contains(stavkaPonudeCollectionOldStavkaPonude)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StavkaPonude " + stavkaPonudeCollectionOldStavkaPonude + " since its artikl field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (jedinicaMereidJedinicaMereNew != null) {
                jedinicaMereidJedinicaMereNew = em.getReference(jedinicaMereidJedinicaMereNew.getClass(), jedinicaMereidJedinicaMereNew.getIdJedinicaMere());
                artikl.setJedinicaMereidJedinicaMere(jedinicaMereidJedinicaMereNew);
            }
            if (kolekcijaIdkolekcijaNew != null) {
                kolekcijaIdkolekcijaNew = em.getReference(kolekcijaIdkolekcijaNew.getClass(), kolekcijaIdkolekcijaNew.getIdkolekcija());
                artikl.setKolekcijaIdkolekcija(kolekcijaIdkolekcijaNew);
            }
            Collection<StavkaPonude> attachedStavkaPonudeCollectionNew = new ArrayList<StavkaPonude>();
            for (StavkaPonude stavkaPonudeCollectionNewStavkaPonudeToAttach : stavkaPonudeCollectionNew) {
                stavkaPonudeCollectionNewStavkaPonudeToAttach = em.getReference(stavkaPonudeCollectionNewStavkaPonudeToAttach.getClass(), stavkaPonudeCollectionNewStavkaPonudeToAttach.getIdstavka());
                attachedStavkaPonudeCollectionNew.add(stavkaPonudeCollectionNewStavkaPonudeToAttach);
            }
            stavkaPonudeCollectionNew = attachedStavkaPonudeCollectionNew;
            artikl.setStavkaPonudeCollection(stavkaPonudeCollectionNew);
            artikl = em.merge(artikl);
            if (jedinicaMereidJedinicaMereOld != null && !jedinicaMereidJedinicaMereOld.equals(jedinicaMereidJedinicaMereNew)) {
                jedinicaMereidJedinicaMereOld.getArtiklCollection().remove(artikl);
                jedinicaMereidJedinicaMereOld = em.merge(jedinicaMereidJedinicaMereOld);
            }
            if (jedinicaMereidJedinicaMereNew != null && !jedinicaMereidJedinicaMereNew.equals(jedinicaMereidJedinicaMereOld)) {
                jedinicaMereidJedinicaMereNew.getArtiklCollection().add(artikl);
                jedinicaMereidJedinicaMereNew = em.merge(jedinicaMereidJedinicaMereNew);
            }
            if (kolekcijaIdkolekcijaOld != null && !kolekcijaIdkolekcijaOld.equals(kolekcijaIdkolekcijaNew)) {
                kolekcijaIdkolekcijaOld.getArtiklCollection().remove(artikl);
                kolekcijaIdkolekcijaOld = em.merge(kolekcijaIdkolekcijaOld);
            }
            if (kolekcijaIdkolekcijaNew != null && !kolekcijaIdkolekcijaNew.equals(kolekcijaIdkolekcijaOld)) {
                kolekcijaIdkolekcijaNew.getArtiklCollection().add(artikl);
                kolekcijaIdkolekcijaNew = em.merge(kolekcijaIdkolekcijaNew);
            }
            for (StavkaPonude stavkaPonudeCollectionNewStavkaPonude : stavkaPonudeCollectionNew) {
                if (!stavkaPonudeCollectionOld.contains(stavkaPonudeCollectionNewStavkaPonude)) {
                    Artikl oldArtiklOfStavkaPonudeCollectionNewStavkaPonude = stavkaPonudeCollectionNewStavkaPonude.getArtikl();
                    stavkaPonudeCollectionNewStavkaPonude.setArtikl(artikl);
                    stavkaPonudeCollectionNewStavkaPonude = em.merge(stavkaPonudeCollectionNewStavkaPonude);
                    if (oldArtiklOfStavkaPonudeCollectionNewStavkaPonude != null && !oldArtiklOfStavkaPonudeCollectionNewStavkaPonude.equals(artikl)) {
                        oldArtiklOfStavkaPonudeCollectionNewStavkaPonude.getStavkaPonudeCollection().remove(stavkaPonudeCollectionNewStavkaPonude);
                        oldArtiklOfStavkaPonudeCollectionNewStavkaPonude = em.merge(oldArtiklOfStavkaPonudeCollectionNewStavkaPonude);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = artikl.getIdArtikl();
                if (findArtikl(id) == null) {
                    throw new NonexistentEntityException("The artikl with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Artikl artikl;
            try {
                artikl = em.getReference(Artikl.class, id);
                artikl.getIdArtikl();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The artikl with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StavkaPonude> stavkaPonudeCollectionOrphanCheck = artikl.getStavkaPonudeCollection();
            for (StavkaPonude stavkaPonudeCollectionOrphanCheckStavkaPonude : stavkaPonudeCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Artikl (" + artikl + ") cannot be destroyed since the StavkaPonude " + stavkaPonudeCollectionOrphanCheckStavkaPonude + " in its stavkaPonudeCollection field has a non-nullable artikl field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            JedinicaMere jedinicaMereidJedinicaMere = artikl.getJedinicaMereidJedinicaMere();
            if (jedinicaMereidJedinicaMere != null) {
                jedinicaMereidJedinicaMere.getArtiklCollection().remove(artikl);
                jedinicaMereidJedinicaMere = em.merge(jedinicaMereidJedinicaMere);
            }
            Kolekcija kolekcijaIdkolekcija = artikl.getKolekcijaIdkolekcija();
            if (kolekcijaIdkolekcija != null) {
                kolekcijaIdkolekcija.getArtiklCollection().remove(artikl);
                kolekcijaIdkolekcija = em.merge(kolekcijaIdkolekcija);
            }
            em.remove(artikl);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Artikl> findArtiklEntities() {
        return findArtiklEntities(true, -1, -1);
    }

    public List<Artikl> findArtiklEntities(int maxResults, int firstResult) {
        return findArtiklEntities(false, maxResults, firstResult);
    }

    private List<Artikl> findArtiklEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Artikl.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Artikl findArtikl(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Artikl.class, id);
        } finally {
            em.close();
        }
    }

    public int getArtiklCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Artikl> rt = cq.from(Artikl.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<Artikl> findByTip(String tip, boolean idDeleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Artikl.findByTip")
                    .setParameter("tip", tip)
                    .setParameter("idDeleted", idDeleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Artikl> findByActive(boolean idDeleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Artikl.findByIdDeleted")
                    .setParameter("idDeleted", idDeleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
}
