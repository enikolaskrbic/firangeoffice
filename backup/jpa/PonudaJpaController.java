/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Korisnik;
import entity.Kupac;
import entity.Ponuda;
import entity.Porudzbina;
import java.util.ArrayList;
import java.util.Collection;
import entity.StavkaPonude;
import entity.Uplata;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class PonudaJpaController implements Serializable {

    public PonudaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ponuda ponuda) {
        if (ponuda.getPorudzbinaCollection() == null) {
            ponuda.setPorudzbinaCollection(new ArrayList<Porudzbina>());
        }
        if (ponuda.getStavkaPonudeCollection() == null) {
            ponuda.setStavkaPonudeCollection(new ArrayList<StavkaPonude>());
        }
        if (ponuda.getUplataCollection() == null) {
            ponuda.setUplataCollection(new ArrayList<Uplata>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Korisnik korisnikidkorisnik = ponuda.getKorisnikidkorisnik();
            if (korisnikidkorisnik != null) {
                korisnikidkorisnik = em.getReference(korisnikidkorisnik.getClass(), korisnikidkorisnik.getIdKorisnik());
                ponuda.setKorisnikidkorisnik(korisnikidkorisnik);
            }
            Kupac kupacidkupac = ponuda.getKupacidkupac();
            if (kupacidkupac != null) {
                kupacidkupac = em.getReference(kupacidkupac.getClass(), kupacidkupac.getIdkupac());
                ponuda.setKupacidkupac(kupacidkupac);
            }
            Collection<Porudzbina> attachedPorudzbinaCollection = new ArrayList<Porudzbina>();
            for (Porudzbina porudzbinaCollectionPorudzbinaToAttach : ponuda.getPorudzbinaCollection()) {
                porudzbinaCollectionPorudzbinaToAttach = em.getReference(porudzbinaCollectionPorudzbinaToAttach.getClass(), porudzbinaCollectionPorudzbinaToAttach.getIdPorudzbina());
                attachedPorudzbinaCollection.add(porudzbinaCollectionPorudzbinaToAttach);
            }
            ponuda.setPorudzbinaCollection(attachedPorudzbinaCollection);
            Collection<StavkaPonude> attachedStavkaPonudeCollection = new ArrayList<StavkaPonude>();
            for (StavkaPonude stavkaPonudeCollectionStavkaPonudeToAttach : ponuda.getStavkaPonudeCollection()) {
                stavkaPonudeCollectionStavkaPonudeToAttach = em.getReference(stavkaPonudeCollectionStavkaPonudeToAttach.getClass(), stavkaPonudeCollectionStavkaPonudeToAttach.getIdstavka());
                attachedStavkaPonudeCollection.add(stavkaPonudeCollectionStavkaPonudeToAttach);
            }
            ponuda.setStavkaPonudeCollection(attachedStavkaPonudeCollection);
            Collection<Uplata> attachedUplataCollection = new ArrayList<Uplata>();
            for (Uplata uplataCollectionUplataToAttach : ponuda.getUplataCollection()) {
                uplataCollectionUplataToAttach = em.getReference(uplataCollectionUplataToAttach.getClass(), uplataCollectionUplataToAttach.getIdUplata());
                attachedUplataCollection.add(uplataCollectionUplataToAttach);
            }
            ponuda.setUplataCollection(attachedUplataCollection);
            em.persist(ponuda);
            if (korisnikidkorisnik != null) {
                korisnikidkorisnik.getPonudaCollection().add(ponuda);
                korisnikidkorisnik = em.merge(korisnikidkorisnik);
            }
            if (kupacidkupac != null) {
                kupacidkupac.getPonudaCollection().add(ponuda);
                kupacidkupac = em.merge(kupacidkupac);
            }
            for (Porudzbina porudzbinaCollectionPorudzbina : ponuda.getPorudzbinaCollection()) {
                Ponuda oldPonudaidponudaOfPorudzbinaCollectionPorudzbina = porudzbinaCollectionPorudzbina.getPonudaidponuda();
                porudzbinaCollectionPorudzbina.setPonudaidponuda(ponuda);
                porudzbinaCollectionPorudzbina = em.merge(porudzbinaCollectionPorudzbina);
                if (oldPonudaidponudaOfPorudzbinaCollectionPorudzbina != null) {
                    oldPonudaidponudaOfPorudzbinaCollectionPorudzbina.getPorudzbinaCollection().remove(porudzbinaCollectionPorudzbina);
                    oldPonudaidponudaOfPorudzbinaCollectionPorudzbina = em.merge(oldPonudaidponudaOfPorudzbinaCollectionPorudzbina);
                }
            }
            for (StavkaPonude stavkaPonudeCollectionStavkaPonude : ponuda.getStavkaPonudeCollection()) {
                Ponuda oldPonudaOfStavkaPonudeCollectionStavkaPonude = stavkaPonudeCollectionStavkaPonude.getPonuda();
                stavkaPonudeCollectionStavkaPonude.setPonuda(ponuda);
                stavkaPonudeCollectionStavkaPonude = em.merge(stavkaPonudeCollectionStavkaPonude);
                if (oldPonudaOfStavkaPonudeCollectionStavkaPonude != null) {
                    oldPonudaOfStavkaPonudeCollectionStavkaPonude.getStavkaPonudeCollection().remove(stavkaPonudeCollectionStavkaPonude);
                    oldPonudaOfStavkaPonudeCollectionStavkaPonude = em.merge(oldPonudaOfStavkaPonudeCollectionStavkaPonude);
                }
            }
            for (Uplata uplataCollectionUplata : ponuda.getUplataCollection()) {
                Ponuda oldIdPonudaOfUplataCollectionUplata = uplataCollectionUplata.getIdPonuda();
                uplataCollectionUplata.setIdPonuda(ponuda);
                uplataCollectionUplata = em.merge(uplataCollectionUplata);
                if (oldIdPonudaOfUplataCollectionUplata != null) {
                    oldIdPonudaOfUplataCollectionUplata.getUplataCollection().remove(uplataCollectionUplata);
                    oldIdPonudaOfUplataCollectionUplata = em.merge(oldIdPonudaOfUplataCollectionUplata);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ponuda ponuda) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ponuda persistentPonuda = em.find(Ponuda.class, ponuda.getIdponuda());
            Korisnik korisnikidkorisnikOld = persistentPonuda.getKorisnikidkorisnik();
            Korisnik korisnikidkorisnikNew = ponuda.getKorisnikidkorisnik();
            Kupac kupacidkupacOld = persistentPonuda.getKupacidkupac();
            Kupac kupacidkupacNew = ponuda.getKupacidkupac();
            Collection<Porudzbina> porudzbinaCollectionOld = persistentPonuda.getPorudzbinaCollection();
            Collection<Porudzbina> porudzbinaCollectionNew = ponuda.getPorudzbinaCollection();
            Collection<StavkaPonude> stavkaPonudeCollectionOld = persistentPonuda.getStavkaPonudeCollection();
            Collection<StavkaPonude> stavkaPonudeCollectionNew = ponuda.getStavkaPonudeCollection();
            Collection<Uplata> uplataCollectionOld = persistentPonuda.getUplataCollection();
            Collection<Uplata> uplataCollectionNew = ponuda.getUplataCollection();
            List<String> illegalOrphanMessages = null;
            for (Porudzbina porudzbinaCollectionOldPorudzbina : porudzbinaCollectionOld) {
                if (!porudzbinaCollectionNew.contains(porudzbinaCollectionOldPorudzbina)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Porudzbina " + porudzbinaCollectionOldPorudzbina + " since its ponudaidponuda field is not nullable.");
                }
            }
            for (StavkaPonude stavkaPonudeCollectionOldStavkaPonude : stavkaPonudeCollectionOld) {
                if (!stavkaPonudeCollectionNew.contains(stavkaPonudeCollectionOldStavkaPonude)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StavkaPonude " + stavkaPonudeCollectionOldStavkaPonude + " since its ponuda field is not nullable.");
                }
            }
            for (Uplata uplataCollectionOldUplata : uplataCollectionOld) {
                if (!uplataCollectionNew.contains(uplataCollectionOldUplata)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Uplata " + uplataCollectionOldUplata + " since its idPonuda field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (korisnikidkorisnikNew != null) {
                korisnikidkorisnikNew = em.getReference(korisnikidkorisnikNew.getClass(), korisnikidkorisnikNew.getIdKorisnik());
                ponuda.setKorisnikidkorisnik(korisnikidkorisnikNew);
            }
            if (kupacidkupacNew != null) {
                kupacidkupacNew = em.getReference(kupacidkupacNew.getClass(), kupacidkupacNew.getIdkupac());
                ponuda.setKupacidkupac(kupacidkupacNew);
            }
            Collection<Porudzbina> attachedPorudzbinaCollectionNew = new ArrayList<Porudzbina>();
            for (Porudzbina porudzbinaCollectionNewPorudzbinaToAttach : porudzbinaCollectionNew) {
                porudzbinaCollectionNewPorudzbinaToAttach = em.getReference(porudzbinaCollectionNewPorudzbinaToAttach.getClass(), porudzbinaCollectionNewPorudzbinaToAttach.getIdPorudzbina());
                attachedPorudzbinaCollectionNew.add(porudzbinaCollectionNewPorudzbinaToAttach);
            }
            porudzbinaCollectionNew = attachedPorudzbinaCollectionNew;
            ponuda.setPorudzbinaCollection(porudzbinaCollectionNew);
            Collection<StavkaPonude> attachedStavkaPonudeCollectionNew = new ArrayList<StavkaPonude>();
            for (StavkaPonude stavkaPonudeCollectionNewStavkaPonudeToAttach : stavkaPonudeCollectionNew) {
                stavkaPonudeCollectionNewStavkaPonudeToAttach = em.getReference(stavkaPonudeCollectionNewStavkaPonudeToAttach.getClass(), stavkaPonudeCollectionNewStavkaPonudeToAttach.getIdstavka());
                attachedStavkaPonudeCollectionNew.add(stavkaPonudeCollectionNewStavkaPonudeToAttach);
            }
            stavkaPonudeCollectionNew = attachedStavkaPonudeCollectionNew;
            ponuda.setStavkaPonudeCollection(stavkaPonudeCollectionNew);
            Collection<Uplata> attachedUplataCollectionNew = new ArrayList<Uplata>();
            for (Uplata uplataCollectionNewUplataToAttach : uplataCollectionNew) {
                uplataCollectionNewUplataToAttach = em.getReference(uplataCollectionNewUplataToAttach.getClass(), uplataCollectionNewUplataToAttach.getIdUplata());
                attachedUplataCollectionNew.add(uplataCollectionNewUplataToAttach);
            }
            uplataCollectionNew = attachedUplataCollectionNew;
            ponuda.setUplataCollection(uplataCollectionNew);
            ponuda = em.merge(ponuda);
            if (korisnikidkorisnikOld != null && !korisnikidkorisnikOld.equals(korisnikidkorisnikNew)) {
                korisnikidkorisnikOld.getPonudaCollection().remove(ponuda);
                korisnikidkorisnikOld = em.merge(korisnikidkorisnikOld);
            }
            if (korisnikidkorisnikNew != null && !korisnikidkorisnikNew.equals(korisnikidkorisnikOld)) {
                korisnikidkorisnikNew.getPonudaCollection().add(ponuda);
                korisnikidkorisnikNew = em.merge(korisnikidkorisnikNew);
            }
            if (kupacidkupacOld != null && !kupacidkupacOld.equals(kupacidkupacNew)) {
                kupacidkupacOld.getPonudaCollection().remove(ponuda);
                kupacidkupacOld = em.merge(kupacidkupacOld);
            }
            if (kupacidkupacNew != null && !kupacidkupacNew.equals(kupacidkupacOld)) {
                kupacidkupacNew.getPonudaCollection().add(ponuda);
                kupacidkupacNew = em.merge(kupacidkupacNew);
            }
            for (Porudzbina porudzbinaCollectionNewPorudzbina : porudzbinaCollectionNew) {
                if (!porudzbinaCollectionOld.contains(porudzbinaCollectionNewPorudzbina)) {
                    Ponuda oldPonudaidponudaOfPorudzbinaCollectionNewPorudzbina = porudzbinaCollectionNewPorudzbina.getPonudaidponuda();
                    porudzbinaCollectionNewPorudzbina.setPonudaidponuda(ponuda);
                    porudzbinaCollectionNewPorudzbina = em.merge(porudzbinaCollectionNewPorudzbina);
                    if (oldPonudaidponudaOfPorudzbinaCollectionNewPorudzbina != null && !oldPonudaidponudaOfPorudzbinaCollectionNewPorudzbina.equals(ponuda)) {
                        oldPonudaidponudaOfPorudzbinaCollectionNewPorudzbina.getPorudzbinaCollection().remove(porudzbinaCollectionNewPorudzbina);
                        oldPonudaidponudaOfPorudzbinaCollectionNewPorudzbina = em.merge(oldPonudaidponudaOfPorudzbinaCollectionNewPorudzbina);
                    }
                }
            }
            for (StavkaPonude stavkaPonudeCollectionNewStavkaPonude : stavkaPonudeCollectionNew) {
                if (!stavkaPonudeCollectionOld.contains(stavkaPonudeCollectionNewStavkaPonude)) {
                    Ponuda oldPonudaOfStavkaPonudeCollectionNewStavkaPonude = stavkaPonudeCollectionNewStavkaPonude.getPonuda();
                    stavkaPonudeCollectionNewStavkaPonude.setPonuda(ponuda);
                    stavkaPonudeCollectionNewStavkaPonude = em.merge(stavkaPonudeCollectionNewStavkaPonude);
                    if (oldPonudaOfStavkaPonudeCollectionNewStavkaPonude != null && !oldPonudaOfStavkaPonudeCollectionNewStavkaPonude.equals(ponuda)) {
                        oldPonudaOfStavkaPonudeCollectionNewStavkaPonude.getStavkaPonudeCollection().remove(stavkaPonudeCollectionNewStavkaPonude);
                        oldPonudaOfStavkaPonudeCollectionNewStavkaPonude = em.merge(oldPonudaOfStavkaPonudeCollectionNewStavkaPonude);
                    }
                }
            }
            for (Uplata uplataCollectionNewUplata : uplataCollectionNew) {
                if (!uplataCollectionOld.contains(uplataCollectionNewUplata)) {
                    Ponuda oldIdPonudaOfUplataCollectionNewUplata = uplataCollectionNewUplata.getIdPonuda();
                    uplataCollectionNewUplata.setIdPonuda(ponuda);
                    uplataCollectionNewUplata = em.merge(uplataCollectionNewUplata);
                    if (oldIdPonudaOfUplataCollectionNewUplata != null && !oldIdPonudaOfUplataCollectionNewUplata.equals(ponuda)) {
                        oldIdPonudaOfUplataCollectionNewUplata.getUplataCollection().remove(uplataCollectionNewUplata);
                        oldIdPonudaOfUplataCollectionNewUplata = em.merge(oldIdPonudaOfUplataCollectionNewUplata);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = ponuda.getIdponuda();
                if (findPonuda(id) == null) {
                    throw new NonexistentEntityException("The ponuda with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ponuda ponuda;
            try {
                ponuda = em.getReference(Ponuda.class, id);
                ponuda.getIdponuda();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ponuda with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Porudzbina> porudzbinaCollectionOrphanCheck = ponuda.getPorudzbinaCollection();
            for (Porudzbina porudzbinaCollectionOrphanCheckPorudzbina : porudzbinaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ponuda (" + ponuda + ") cannot be destroyed since the Porudzbina " + porudzbinaCollectionOrphanCheckPorudzbina + " in its porudzbinaCollection field has a non-nullable ponudaidponuda field.");
            }
            Collection<StavkaPonude> stavkaPonudeCollectionOrphanCheck = ponuda.getStavkaPonudeCollection();
            for (StavkaPonude stavkaPonudeCollectionOrphanCheckStavkaPonude : stavkaPonudeCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ponuda (" + ponuda + ") cannot be destroyed since the StavkaPonude " + stavkaPonudeCollectionOrphanCheckStavkaPonude + " in its stavkaPonudeCollection field has a non-nullable ponuda field.");
            }
            Collection<Uplata> uplataCollectionOrphanCheck = ponuda.getUplataCollection();
            for (Uplata uplataCollectionOrphanCheckUplata : uplataCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ponuda (" + ponuda + ") cannot be destroyed since the Uplata " + uplataCollectionOrphanCheckUplata + " in its uplataCollection field has a non-nullable idPonuda field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Korisnik korisnikidkorisnik = ponuda.getKorisnikidkorisnik();
            if (korisnikidkorisnik != null) {
                korisnikidkorisnik.getPonudaCollection().remove(ponuda);
                korisnikidkorisnik = em.merge(korisnikidkorisnik);
            }
            Kupac kupacidkupac = ponuda.getKupacidkupac();
            if (kupacidkupac != null) {
                kupacidkupac.getPonudaCollection().remove(ponuda);
                kupacidkupac = em.merge(kupacidkupac);
            }
            em.remove(ponuda);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ponuda> findPonudaEntities() {
        return findPonudaEntities(true, -1, -1);
    }

    public List<Ponuda> findPonudaEntities(int maxResults, int firstResult) {
        return findPonudaEntities(false, maxResults, firstResult);
    }

    private List<Ponuda> findPonudaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ponuda.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ponuda findPonuda(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ponuda.class, id);
        } finally {
            em.close();
        }
    }

    public int getPonudaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ponuda> rt = cq.from(Ponuda.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
