/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Artikl;
import entity.JedinicaMere;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class JedinicaMereJpaController implements Serializable {

    public JedinicaMereJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(JedinicaMere jedinicaMere) {
        if (jedinicaMere.getArtiklCollection() == null) {
            jedinicaMere.setArtiklCollection(new ArrayList<Artikl>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Artikl> attachedArtiklCollection = new ArrayList<Artikl>();
            for (Artikl artiklCollectionArtiklToAttach : jedinicaMere.getArtiklCollection()) {
                artiklCollectionArtiklToAttach = em.getReference(artiklCollectionArtiklToAttach.getClass(), artiklCollectionArtiklToAttach.getIdArtikl());
                attachedArtiklCollection.add(artiklCollectionArtiklToAttach);
            }
            jedinicaMere.setArtiklCollection(attachedArtiklCollection);
            em.persist(jedinicaMere);
            for (Artikl artiklCollectionArtikl : jedinicaMere.getArtiklCollection()) {
                JedinicaMere oldJedinicaMereidJedinicaMereOfArtiklCollectionArtikl = artiklCollectionArtikl.getJedinicaMereidJedinicaMere();
                artiklCollectionArtikl.setJedinicaMereidJedinicaMere(jedinicaMere);
                artiklCollectionArtikl = em.merge(artiklCollectionArtikl);
                if (oldJedinicaMereidJedinicaMereOfArtiklCollectionArtikl != null) {
                    oldJedinicaMereidJedinicaMereOfArtiklCollectionArtikl.getArtiklCollection().remove(artiklCollectionArtikl);
                    oldJedinicaMereidJedinicaMereOfArtiklCollectionArtikl = em.merge(oldJedinicaMereidJedinicaMereOfArtiklCollectionArtikl);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(JedinicaMere jedinicaMere) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            JedinicaMere persistentJedinicaMere = em.find(JedinicaMere.class, jedinicaMere.getIdJedinicaMere());
            Collection<Artikl> artiklCollectionOld = persistentJedinicaMere.getArtiklCollection();
            Collection<Artikl> artiklCollectionNew = jedinicaMere.getArtiklCollection();
            List<String> illegalOrphanMessages = null;
            for (Artikl artiklCollectionOldArtikl : artiklCollectionOld) {
                if (!artiklCollectionNew.contains(artiklCollectionOldArtikl)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Artikl " + artiklCollectionOldArtikl + " since its jedinicaMereidJedinicaMere field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Artikl> attachedArtiklCollectionNew = new ArrayList<Artikl>();
            for (Artikl artiklCollectionNewArtiklToAttach : artiklCollectionNew) {
                artiklCollectionNewArtiklToAttach = em.getReference(artiklCollectionNewArtiklToAttach.getClass(), artiklCollectionNewArtiklToAttach.getIdArtikl());
                attachedArtiklCollectionNew.add(artiklCollectionNewArtiklToAttach);
            }
            artiklCollectionNew = attachedArtiklCollectionNew;
            jedinicaMere.setArtiklCollection(artiklCollectionNew);
            jedinicaMere = em.merge(jedinicaMere);
            for (Artikl artiklCollectionNewArtikl : artiklCollectionNew) {
                if (!artiklCollectionOld.contains(artiklCollectionNewArtikl)) {
                    JedinicaMere oldJedinicaMereidJedinicaMereOfArtiklCollectionNewArtikl = artiklCollectionNewArtikl.getJedinicaMereidJedinicaMere();
                    artiklCollectionNewArtikl.setJedinicaMereidJedinicaMere(jedinicaMere);
                    artiklCollectionNewArtikl = em.merge(artiklCollectionNewArtikl);
                    if (oldJedinicaMereidJedinicaMereOfArtiklCollectionNewArtikl != null && !oldJedinicaMereidJedinicaMereOfArtiklCollectionNewArtikl.equals(jedinicaMere)) {
                        oldJedinicaMereidJedinicaMereOfArtiklCollectionNewArtikl.getArtiklCollection().remove(artiklCollectionNewArtikl);
                        oldJedinicaMereidJedinicaMereOfArtiklCollectionNewArtikl = em.merge(oldJedinicaMereidJedinicaMereOfArtiklCollectionNewArtikl);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = jedinicaMere.getIdJedinicaMere();
                if (findJedinicaMere(id) == null) {
                    throw new NonexistentEntityException("The jedinicaMere with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            JedinicaMere jedinicaMere;
            try {
                jedinicaMere = em.getReference(JedinicaMere.class, id);
                jedinicaMere.getIdJedinicaMere();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The jedinicaMere with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Artikl> artiklCollectionOrphanCheck = jedinicaMere.getArtiklCollection();
            for (Artikl artiklCollectionOrphanCheckArtikl : artiklCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This JedinicaMere (" + jedinicaMere + ") cannot be destroyed since the Artikl " + artiklCollectionOrphanCheckArtikl + " in its artiklCollection field has a non-nullable jedinicaMereidJedinicaMere field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(jedinicaMere);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<JedinicaMere> findJedinicaMereEntities() {
        return findJedinicaMereEntities(true, -1, -1);
    }

    public List<JedinicaMere> findJedinicaMereEntities(int maxResults, int firstResult) {
        return findJedinicaMereEntities(false, maxResults, firstResult);
    }

    private List<JedinicaMere> findJedinicaMereEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(JedinicaMere.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public JedinicaMere findJedinicaMere(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(JedinicaMere.class, id);
        } finally {
            em.close();
        }
    }

    public int getJedinicaMereCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<JedinicaMere> rt = cq.from(JedinicaMere.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<JedinicaMere> findJedinicaMereByDeleted(boolean deleted) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("JedinicaMere.findByIdDeleted")
                    .setParameter("idDeleted", deleted)
                    .getResultList();
        } finally {
            em.close();
        }
    }
}
