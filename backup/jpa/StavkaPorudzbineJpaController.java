/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Porudzbina;
import entity.StavkaPonude;
import entity.StavkaPorudzbine;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class StavkaPorudzbineJpaController implements Serializable {

    public StavkaPorudzbineJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StavkaPorudzbine stavkaPorudzbine) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Porudzbina porudzbinaidPorudzbina = stavkaPorudzbine.getPorudzbinaidPorudzbina();
            if (porudzbinaidPorudzbina != null) {
                porudzbinaidPorudzbina = em.getReference(porudzbinaidPorudzbina.getClass(), porudzbinaidPorudzbina.getIdPorudzbina());
                stavkaPorudzbine.setPorudzbinaidPorudzbina(porudzbinaidPorudzbina);
            }
            StavkaPonude stavkaIdstavka = stavkaPorudzbine.getStavkaIdstavka();
            if (stavkaIdstavka != null) {
                stavkaIdstavka = em.getReference(stavkaIdstavka.getClass(), stavkaIdstavka.getIdstavka());
                stavkaPorudzbine.setStavkaIdstavka(stavkaIdstavka);
            }
            em.persist(stavkaPorudzbine);
            if (porudzbinaidPorudzbina != null) {
                porudzbinaidPorudzbina.getStavkaPorudzbineCollection().add(stavkaPorudzbine);
                porudzbinaidPorudzbina = em.merge(porudzbinaidPorudzbina);
            }
            if (stavkaIdstavka != null) {
                stavkaIdstavka.getStavkaPorudzbineCollection().add(stavkaPorudzbine);
                stavkaIdstavka = em.merge(stavkaIdstavka);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StavkaPorudzbine stavkaPorudzbine) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StavkaPorudzbine persistentStavkaPorudzbine = em.find(StavkaPorudzbine.class, stavkaPorudzbine.getIdStavkaPorudzbine());
            Porudzbina porudzbinaidPorudzbinaOld = persistentStavkaPorudzbine.getPorudzbinaidPorudzbina();
            Porudzbina porudzbinaidPorudzbinaNew = stavkaPorudzbine.getPorudzbinaidPorudzbina();
            StavkaPonude stavkaIdstavkaOld = persistentStavkaPorudzbine.getStavkaIdstavka();
            StavkaPonude stavkaIdstavkaNew = stavkaPorudzbine.getStavkaIdstavka();
            if (porudzbinaidPorudzbinaNew != null) {
                porudzbinaidPorudzbinaNew = em.getReference(porudzbinaidPorudzbinaNew.getClass(), porudzbinaidPorudzbinaNew.getIdPorudzbina());
                stavkaPorudzbine.setPorudzbinaidPorudzbina(porudzbinaidPorudzbinaNew);
            }
            if (stavkaIdstavkaNew != null) {
                stavkaIdstavkaNew = em.getReference(stavkaIdstavkaNew.getClass(), stavkaIdstavkaNew.getIdstavka());
                stavkaPorudzbine.setStavkaIdstavka(stavkaIdstavkaNew);
            }
            stavkaPorudzbine = em.merge(stavkaPorudzbine);
            if (porudzbinaidPorudzbinaOld != null && !porudzbinaidPorudzbinaOld.equals(porudzbinaidPorudzbinaNew)) {
                porudzbinaidPorudzbinaOld.getStavkaPorudzbineCollection().remove(stavkaPorudzbine);
                porudzbinaidPorudzbinaOld = em.merge(porudzbinaidPorudzbinaOld);
            }
            if (porudzbinaidPorudzbinaNew != null && !porudzbinaidPorudzbinaNew.equals(porudzbinaidPorudzbinaOld)) {
                porudzbinaidPorudzbinaNew.getStavkaPorudzbineCollection().add(stavkaPorudzbine);
                porudzbinaidPorudzbinaNew = em.merge(porudzbinaidPorudzbinaNew);
            }
            if (stavkaIdstavkaOld != null && !stavkaIdstavkaOld.equals(stavkaIdstavkaNew)) {
                stavkaIdstavkaOld.getStavkaPorudzbineCollection().remove(stavkaPorudzbine);
                stavkaIdstavkaOld = em.merge(stavkaIdstavkaOld);
            }
            if (stavkaIdstavkaNew != null && !stavkaIdstavkaNew.equals(stavkaIdstavkaOld)) {
                stavkaIdstavkaNew.getStavkaPorudzbineCollection().add(stavkaPorudzbine);
                stavkaIdstavkaNew = em.merge(stavkaIdstavkaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = stavkaPorudzbine.getIdStavkaPorudzbine();
                if (findStavkaPorudzbine(id) == null) {
                    throw new NonexistentEntityException("The stavkaPorudzbine with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StavkaPorudzbine stavkaPorudzbine;
            try {
                stavkaPorudzbine = em.getReference(StavkaPorudzbine.class, id);
                stavkaPorudzbine.getIdStavkaPorudzbine();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stavkaPorudzbine with id " + id + " no longer exists.", enfe);
            }
            Porudzbina porudzbinaidPorudzbina = stavkaPorudzbine.getPorudzbinaidPorudzbina();
            if (porudzbinaidPorudzbina != null) {
                porudzbinaidPorudzbina.getStavkaPorudzbineCollection().remove(stavkaPorudzbine);
                porudzbinaidPorudzbina = em.merge(porudzbinaidPorudzbina);
            }
            StavkaPonude stavkaIdstavka = stavkaPorudzbine.getStavkaIdstavka();
            if (stavkaIdstavka != null) {
                stavkaIdstavka.getStavkaPorudzbineCollection().remove(stavkaPorudzbine);
                stavkaIdstavka = em.merge(stavkaIdstavka);
            }
            em.remove(stavkaPorudzbine);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StavkaPorudzbine> findStavkaPorudzbineEntities() {
        return findStavkaPorudzbineEntities(true, -1, -1);
    }

    public List<StavkaPorudzbine> findStavkaPorudzbineEntities(int maxResults, int firstResult) {
        return findStavkaPorudzbineEntities(false, maxResults, firstResult);
    }

    private List<StavkaPorudzbine> findStavkaPorudzbineEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StavkaPorudzbine.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StavkaPorudzbine findStavkaPorudzbine(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StavkaPorudzbine.class, id);
        } finally {
            em.close();
        }
    }

    public int getStavkaPorudzbineCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StavkaPorudzbine> rt = cq.from(StavkaPorudzbine.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
