/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Dobavljac;
import entity.Ponuda;
import entity.Porudzbina;
import entity.StavkaPorudzbine;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class PorudzbinaJpaController implements Serializable {

    public PorudzbinaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Porudzbina porudzbina) {
        if (porudzbina.getStavkaPorudzbineCollection() == null) {
            porudzbina.setStavkaPorudzbineCollection(new ArrayList<StavkaPorudzbine>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dobavljac dobavljaciddobavljac = porudzbina.getDobavljaciddobavljac();
            if (dobavljaciddobavljac != null) {
                dobavljaciddobavljac = em.getReference(dobavljaciddobavljac.getClass(), dobavljaciddobavljac.getIddobavljac());
                porudzbina.setDobavljaciddobavljac(dobavljaciddobavljac);
            }
            Ponuda ponudaidponuda = porudzbina.getPonudaidponuda();
            if (ponudaidponuda != null) {
                ponudaidponuda = em.getReference(ponudaidponuda.getClass(), ponudaidponuda.getIdponuda());
                porudzbina.setPonudaidponuda(ponudaidponuda);
            }
            Collection<StavkaPorudzbine> attachedStavkaPorudzbineCollection = new ArrayList<StavkaPorudzbine>();
            for (StavkaPorudzbine stavkaPorudzbineCollectionStavkaPorudzbineToAttach : porudzbina.getStavkaPorudzbineCollection()) {
                stavkaPorudzbineCollectionStavkaPorudzbineToAttach = em.getReference(stavkaPorudzbineCollectionStavkaPorudzbineToAttach.getClass(), stavkaPorudzbineCollectionStavkaPorudzbineToAttach.getIdStavkaPorudzbine());
                attachedStavkaPorudzbineCollection.add(stavkaPorudzbineCollectionStavkaPorudzbineToAttach);
            }
            porudzbina.setStavkaPorudzbineCollection(attachedStavkaPorudzbineCollection);
            em.persist(porudzbina);
            if (dobavljaciddobavljac != null) {
                dobavljaciddobavljac.getPorudzbinaCollection().add(porudzbina);
                dobavljaciddobavljac = em.merge(dobavljaciddobavljac);
            }
            if (ponudaidponuda != null) {
                ponudaidponuda.getPorudzbinaCollection().add(porudzbina);
                ponudaidponuda = em.merge(ponudaidponuda);
            }
            for (StavkaPorudzbine stavkaPorudzbineCollectionStavkaPorudzbine : porudzbina.getStavkaPorudzbineCollection()) {
                Porudzbina oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionStavkaPorudzbine = stavkaPorudzbineCollectionStavkaPorudzbine.getPorudzbinaidPorudzbina();
                stavkaPorudzbineCollectionStavkaPorudzbine.setPorudzbinaidPorudzbina(porudzbina);
                stavkaPorudzbineCollectionStavkaPorudzbine = em.merge(stavkaPorudzbineCollectionStavkaPorudzbine);
                if (oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionStavkaPorudzbine != null) {
                    oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionStavkaPorudzbine.getStavkaPorudzbineCollection().remove(stavkaPorudzbineCollectionStavkaPorudzbine);
                    oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionStavkaPorudzbine = em.merge(oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionStavkaPorudzbine);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Porudzbina porudzbina) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Porudzbina persistentPorudzbina = em.find(Porudzbina.class, porudzbina.getIdPorudzbina());
            Dobavljac dobavljaciddobavljacOld = persistentPorudzbina.getDobavljaciddobavljac();
            Dobavljac dobavljaciddobavljacNew = porudzbina.getDobavljaciddobavljac();
            Ponuda ponudaidponudaOld = persistentPorudzbina.getPonudaidponuda();
            Ponuda ponudaidponudaNew = porudzbina.getPonudaidponuda();
            Collection<StavkaPorudzbine> stavkaPorudzbineCollectionOld = persistentPorudzbina.getStavkaPorudzbineCollection();
            Collection<StavkaPorudzbine> stavkaPorudzbineCollectionNew = porudzbina.getStavkaPorudzbineCollection();
            List<String> illegalOrphanMessages = null;
            for (StavkaPorudzbine stavkaPorudzbineCollectionOldStavkaPorudzbine : stavkaPorudzbineCollectionOld) {
                if (!stavkaPorudzbineCollectionNew.contains(stavkaPorudzbineCollectionOldStavkaPorudzbine)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StavkaPorudzbine " + stavkaPorudzbineCollectionOldStavkaPorudzbine + " since its porudzbinaidPorudzbina field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (dobavljaciddobavljacNew != null) {
                dobavljaciddobavljacNew = em.getReference(dobavljaciddobavljacNew.getClass(), dobavljaciddobavljacNew.getIddobavljac());
                porudzbina.setDobavljaciddobavljac(dobavljaciddobavljacNew);
            }
            if (ponudaidponudaNew != null) {
                ponudaidponudaNew = em.getReference(ponudaidponudaNew.getClass(), ponudaidponudaNew.getIdponuda());
                porudzbina.setPonudaidponuda(ponudaidponudaNew);
            }
            Collection<StavkaPorudzbine> attachedStavkaPorudzbineCollectionNew = new ArrayList<StavkaPorudzbine>();
            for (StavkaPorudzbine stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach : stavkaPorudzbineCollectionNew) {
                stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach = em.getReference(stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach.getClass(), stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach.getIdStavkaPorudzbine());
                attachedStavkaPorudzbineCollectionNew.add(stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach);
            }
            stavkaPorudzbineCollectionNew = attachedStavkaPorudzbineCollectionNew;
            porudzbina.setStavkaPorudzbineCollection(stavkaPorudzbineCollectionNew);
            porudzbina = em.merge(porudzbina);
            if (dobavljaciddobavljacOld != null && !dobavljaciddobavljacOld.equals(dobavljaciddobavljacNew)) {
                dobavljaciddobavljacOld.getPorudzbinaCollection().remove(porudzbina);
                dobavljaciddobavljacOld = em.merge(dobavljaciddobavljacOld);
            }
            if (dobavljaciddobavljacNew != null && !dobavljaciddobavljacNew.equals(dobavljaciddobavljacOld)) {
                dobavljaciddobavljacNew.getPorudzbinaCollection().add(porudzbina);
                dobavljaciddobavljacNew = em.merge(dobavljaciddobavljacNew);
            }
            if (ponudaidponudaOld != null && !ponudaidponudaOld.equals(ponudaidponudaNew)) {
                ponudaidponudaOld.getPorudzbinaCollection().remove(porudzbina);
                ponudaidponudaOld = em.merge(ponudaidponudaOld);
            }
            if (ponudaidponudaNew != null && !ponudaidponudaNew.equals(ponudaidponudaOld)) {
                ponudaidponudaNew.getPorudzbinaCollection().add(porudzbina);
                ponudaidponudaNew = em.merge(ponudaidponudaNew);
            }
            for (StavkaPorudzbine stavkaPorudzbineCollectionNewStavkaPorudzbine : stavkaPorudzbineCollectionNew) {
                if (!stavkaPorudzbineCollectionOld.contains(stavkaPorudzbineCollectionNewStavkaPorudzbine)) {
                    Porudzbina oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionNewStavkaPorudzbine = stavkaPorudzbineCollectionNewStavkaPorudzbine.getPorudzbinaidPorudzbina();
                    stavkaPorudzbineCollectionNewStavkaPorudzbine.setPorudzbinaidPorudzbina(porudzbina);
                    stavkaPorudzbineCollectionNewStavkaPorudzbine = em.merge(stavkaPorudzbineCollectionNewStavkaPorudzbine);
                    if (oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionNewStavkaPorudzbine != null && !oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionNewStavkaPorudzbine.equals(porudzbina)) {
                        oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionNewStavkaPorudzbine.getStavkaPorudzbineCollection().remove(stavkaPorudzbineCollectionNewStavkaPorudzbine);
                        oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionNewStavkaPorudzbine = em.merge(oldPorudzbinaidPorudzbinaOfStavkaPorudzbineCollectionNewStavkaPorudzbine);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = porudzbina.getIdPorudzbina();
                if (findPorudzbina(id) == null) {
                    throw new NonexistentEntityException("The porudzbina with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Porudzbina porudzbina;
            try {
                porudzbina = em.getReference(Porudzbina.class, id);
                porudzbina.getIdPorudzbina();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The porudzbina with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StavkaPorudzbine> stavkaPorudzbineCollectionOrphanCheck = porudzbina.getStavkaPorudzbineCollection();
            for (StavkaPorudzbine stavkaPorudzbineCollectionOrphanCheckStavkaPorudzbine : stavkaPorudzbineCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Porudzbina (" + porudzbina + ") cannot be destroyed since the StavkaPorudzbine " + stavkaPorudzbineCollectionOrphanCheckStavkaPorudzbine + " in its stavkaPorudzbineCollection field has a non-nullable porudzbinaidPorudzbina field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Dobavljac dobavljaciddobavljac = porudzbina.getDobavljaciddobavljac();
            if (dobavljaciddobavljac != null) {
                dobavljaciddobavljac.getPorudzbinaCollection().remove(porudzbina);
                dobavljaciddobavljac = em.merge(dobavljaciddobavljac);
            }
            Ponuda ponudaidponuda = porudzbina.getPonudaidponuda();
            if (ponudaidponuda != null) {
                ponudaidponuda.getPorudzbinaCollection().remove(porudzbina);
                ponudaidponuda = em.merge(ponudaidponuda);
            }
            em.remove(porudzbina);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Porudzbina> findPorudzbinaEntities() {
        return findPorudzbinaEntities(true, -1, -1);
    }

    public List<Porudzbina> findPorudzbinaEntities(int maxResults, int firstResult) {
        return findPorudzbinaEntities(false, maxResults, firstResult);
    }

    private List<Porudzbina> findPorudzbinaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Porudzbina.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Porudzbina findPorudzbina(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Porudzbina.class, id);
        } finally {
            em.close();
        }
    }

    public int getPorudzbinaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Porudzbina> rt = cq.from(Porudzbina.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Porudzbina> findByPonuda(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.createNamedQuery("Porudzbina.findByPonuda")
                    .setParameter("ponudaidponuda", id)
                    .getResultList();
        } finally {
            em.close();
        }
    }
    
}
