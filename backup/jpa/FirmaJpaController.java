/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import entity.Firma;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Korisnik;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class FirmaJpaController implements Serializable {

    public FirmaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Firma firma) {
        if (firma.getKorisnikCollection() == null) {
            firma.setKorisnikCollection(new ArrayList<Korisnik>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Korisnik> attachedKorisnikCollection = new ArrayList<Korisnik>();
            for (Korisnik korisnikCollectionKorisnikToAttach : firma.getKorisnikCollection()) {
                korisnikCollectionKorisnikToAttach = em.getReference(korisnikCollectionKorisnikToAttach.getClass(), korisnikCollectionKorisnikToAttach.getIdKorisnik());
                attachedKorisnikCollection.add(korisnikCollectionKorisnikToAttach);
            }
            firma.setKorisnikCollection(attachedKorisnikCollection);
            em.persist(firma);
            for (Korisnik korisnikCollectionKorisnik : firma.getKorisnikCollection()) {
                Firma oldFirmaidfirmaOfKorisnikCollectionKorisnik = korisnikCollectionKorisnik.getFirmaidfirma();
                korisnikCollectionKorisnik.setFirmaidfirma(firma);
                korisnikCollectionKorisnik = em.merge(korisnikCollectionKorisnik);
                if (oldFirmaidfirmaOfKorisnikCollectionKorisnik != null) {
                    oldFirmaidfirmaOfKorisnikCollectionKorisnik.getKorisnikCollection().remove(korisnikCollectionKorisnik);
                    oldFirmaidfirmaOfKorisnikCollectionKorisnik = em.merge(oldFirmaidfirmaOfKorisnikCollectionKorisnik);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Firma firma) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Firma persistentFirma = em.find(Firma.class, firma.getIdfirma());
            Collection<Korisnik> korisnikCollectionOld = persistentFirma.getKorisnikCollection();
            Collection<Korisnik> korisnikCollectionNew = firma.getKorisnikCollection();
            List<String> illegalOrphanMessages = null;
            for (Korisnik korisnikCollectionOldKorisnik : korisnikCollectionOld) {
                if (!korisnikCollectionNew.contains(korisnikCollectionOldKorisnik)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Korisnik " + korisnikCollectionOldKorisnik + " since its firmaidfirma field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Korisnik> attachedKorisnikCollectionNew = new ArrayList<Korisnik>();
            for (Korisnik korisnikCollectionNewKorisnikToAttach : korisnikCollectionNew) {
                korisnikCollectionNewKorisnikToAttach = em.getReference(korisnikCollectionNewKorisnikToAttach.getClass(), korisnikCollectionNewKorisnikToAttach.getIdKorisnik());
                attachedKorisnikCollectionNew.add(korisnikCollectionNewKorisnikToAttach);
            }
            korisnikCollectionNew = attachedKorisnikCollectionNew;
            firma.setKorisnikCollection(korisnikCollectionNew);
            firma = em.merge(firma);
            for (Korisnik korisnikCollectionNewKorisnik : korisnikCollectionNew) {
                if (!korisnikCollectionOld.contains(korisnikCollectionNewKorisnik)) {
                    Firma oldFirmaidfirmaOfKorisnikCollectionNewKorisnik = korisnikCollectionNewKorisnik.getFirmaidfirma();
                    korisnikCollectionNewKorisnik.setFirmaidfirma(firma);
                    korisnikCollectionNewKorisnik = em.merge(korisnikCollectionNewKorisnik);
                    if (oldFirmaidfirmaOfKorisnikCollectionNewKorisnik != null && !oldFirmaidfirmaOfKorisnikCollectionNewKorisnik.equals(firma)) {
                        oldFirmaidfirmaOfKorisnikCollectionNewKorisnik.getKorisnikCollection().remove(korisnikCollectionNewKorisnik);
                        oldFirmaidfirmaOfKorisnikCollectionNewKorisnik = em.merge(oldFirmaidfirmaOfKorisnikCollectionNewKorisnik);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = firma.getIdfirma();
                if (findFirma(id) == null) {
                    throw new NonexistentEntityException("The firma with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Firma firma;
            try {
                firma = em.getReference(Firma.class, id);
                firma.getIdfirma();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The firma with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Korisnik> korisnikCollectionOrphanCheck = firma.getKorisnikCollection();
            for (Korisnik korisnikCollectionOrphanCheckKorisnik : korisnikCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Firma (" + firma + ") cannot be destroyed since the Korisnik " + korisnikCollectionOrphanCheckKorisnik + " in its korisnikCollection field has a non-nullable firmaidfirma field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(firma);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Firma> findFirmaEntities() {
        return findFirmaEntities(true, -1, -1);
    }

    public List<Firma> findFirmaEntities(int maxResults, int firstResult) {
        return findFirmaEntities(false, maxResults, firstResult);
    }

    private List<Firma> findFirmaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Firma.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Firma findFirma(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Firma.class, id);
        } finally {
            em.close();
        }
    }

    public int getFirmaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Firma> rt = cq.from(Firma.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
