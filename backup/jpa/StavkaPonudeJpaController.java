/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Artikl;
import entity.Ponuda;
import entity.StavkaPonude;
import entity.StavkaPorudzbine;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Skrbic
 */
public class StavkaPonudeJpaController implements Serializable {

    public StavkaPonudeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StavkaPonude stavkaPonude) {
        if (stavkaPonude.getStavkaPorudzbineCollection() == null) {
            stavkaPonude.setStavkaPorudzbineCollection(new ArrayList<StavkaPorudzbine>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Artikl artikl = stavkaPonude.getArtikl();
            if (artikl != null) {
                artikl = em.getReference(artikl.getClass(), artikl.getIdArtikl());
                stavkaPonude.setArtikl(artikl);
            }
            Ponuda ponuda = stavkaPonude.getPonuda();
            if (ponuda != null) {
                ponuda = em.getReference(ponuda.getClass(), ponuda.getIdponuda());
                stavkaPonude.setPonuda(ponuda);
            }
            Collection<StavkaPorudzbine> attachedStavkaPorudzbineCollection = new ArrayList<StavkaPorudzbine>();
            for (StavkaPorudzbine stavkaPorudzbineCollectionStavkaPorudzbineToAttach : stavkaPonude.getStavkaPorudzbineCollection()) {
                stavkaPorudzbineCollectionStavkaPorudzbineToAttach = em.getReference(stavkaPorudzbineCollectionStavkaPorudzbineToAttach.getClass(), stavkaPorudzbineCollectionStavkaPorudzbineToAttach.getIdStavkaPorudzbine());
                attachedStavkaPorudzbineCollection.add(stavkaPorudzbineCollectionStavkaPorudzbineToAttach);
            }
            stavkaPonude.setStavkaPorudzbineCollection(attachedStavkaPorudzbineCollection);
            em.persist(stavkaPonude);
            if (artikl != null) {
                artikl.getStavkaPonudeCollection().add(stavkaPonude);
                artikl = em.merge(artikl);
            }
            if (ponuda != null) {
                ponuda.getStavkaPonudeCollection().add(stavkaPonude);
                ponuda = em.merge(ponuda);
            }
            for (StavkaPorudzbine stavkaPorudzbineCollectionStavkaPorudzbine : stavkaPonude.getStavkaPorudzbineCollection()) {
                StavkaPonude oldStavkaIdstavkaOfStavkaPorudzbineCollectionStavkaPorudzbine = stavkaPorudzbineCollectionStavkaPorudzbine.getStavkaIdstavka();
                stavkaPorudzbineCollectionStavkaPorudzbine.setStavkaIdstavka(stavkaPonude);
                stavkaPorudzbineCollectionStavkaPorudzbine = em.merge(stavkaPorudzbineCollectionStavkaPorudzbine);
                if (oldStavkaIdstavkaOfStavkaPorudzbineCollectionStavkaPorudzbine != null) {
                    oldStavkaIdstavkaOfStavkaPorudzbineCollectionStavkaPorudzbine.getStavkaPorudzbineCollection().remove(stavkaPorudzbineCollectionStavkaPorudzbine);
                    oldStavkaIdstavkaOfStavkaPorudzbineCollectionStavkaPorudzbine = em.merge(oldStavkaIdstavkaOfStavkaPorudzbineCollectionStavkaPorudzbine);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StavkaPonude stavkaPonude) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StavkaPonude persistentStavkaPonude = em.find(StavkaPonude.class, stavkaPonude.getIdstavka());
            Artikl artiklOld = persistentStavkaPonude.getArtikl();
            Artikl artiklNew = stavkaPonude.getArtikl();
            Ponuda ponudaOld = persistentStavkaPonude.getPonuda();
            Ponuda ponudaNew = stavkaPonude.getPonuda();
            Collection<StavkaPorudzbine> stavkaPorudzbineCollectionOld = persistentStavkaPonude.getStavkaPorudzbineCollection();
            Collection<StavkaPorudzbine> stavkaPorudzbineCollectionNew = stavkaPonude.getStavkaPorudzbineCollection();
            List<String> illegalOrphanMessages = null;
            for (StavkaPorudzbine stavkaPorudzbineCollectionOldStavkaPorudzbine : stavkaPorudzbineCollectionOld) {
                if (!stavkaPorudzbineCollectionNew.contains(stavkaPorudzbineCollectionOldStavkaPorudzbine)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StavkaPorudzbine " + stavkaPorudzbineCollectionOldStavkaPorudzbine + " since its stavkaIdstavka field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (artiklNew != null) {
                artiklNew = em.getReference(artiklNew.getClass(), artiklNew.getIdArtikl());
                stavkaPonude.setArtikl(artiklNew);
            }
            if (ponudaNew != null) {
                ponudaNew = em.getReference(ponudaNew.getClass(), ponudaNew.getIdponuda());
                stavkaPonude.setPonuda(ponudaNew);
            }
            Collection<StavkaPorudzbine> attachedStavkaPorudzbineCollectionNew = new ArrayList<StavkaPorudzbine>();
            for (StavkaPorudzbine stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach : stavkaPorudzbineCollectionNew) {
                stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach = em.getReference(stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach.getClass(), stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach.getIdStavkaPorudzbine());
                attachedStavkaPorudzbineCollectionNew.add(stavkaPorudzbineCollectionNewStavkaPorudzbineToAttach);
            }
            stavkaPorudzbineCollectionNew = attachedStavkaPorudzbineCollectionNew;
            stavkaPonude.setStavkaPorudzbineCollection(stavkaPorudzbineCollectionNew);
            stavkaPonude = em.merge(stavkaPonude);
            if (artiklOld != null && !artiklOld.equals(artiklNew)) {
                artiklOld.getStavkaPonudeCollection().remove(stavkaPonude);
                artiklOld = em.merge(artiklOld);
            }
            if (artiklNew != null && !artiklNew.equals(artiklOld)) {
                artiklNew.getStavkaPonudeCollection().add(stavkaPonude);
                artiklNew = em.merge(artiklNew);
            }
            if (ponudaOld != null && !ponudaOld.equals(ponudaNew)) {
                ponudaOld.getStavkaPonudeCollection().remove(stavkaPonude);
                ponudaOld = em.merge(ponudaOld);
            }
            if (ponudaNew != null && !ponudaNew.equals(ponudaOld)) {
                ponudaNew.getStavkaPonudeCollection().add(stavkaPonude);
                ponudaNew = em.merge(ponudaNew);
            }
            for (StavkaPorudzbine stavkaPorudzbineCollectionNewStavkaPorudzbine : stavkaPorudzbineCollectionNew) {
                if (!stavkaPorudzbineCollectionOld.contains(stavkaPorudzbineCollectionNewStavkaPorudzbine)) {
                    StavkaPonude oldStavkaIdstavkaOfStavkaPorudzbineCollectionNewStavkaPorudzbine = stavkaPorudzbineCollectionNewStavkaPorudzbine.getStavkaIdstavka();
                    stavkaPorudzbineCollectionNewStavkaPorudzbine.setStavkaIdstavka(stavkaPonude);
                    stavkaPorudzbineCollectionNewStavkaPorudzbine = em.merge(stavkaPorudzbineCollectionNewStavkaPorudzbine);
                    if (oldStavkaIdstavkaOfStavkaPorudzbineCollectionNewStavkaPorudzbine != null && !oldStavkaIdstavkaOfStavkaPorudzbineCollectionNewStavkaPorudzbine.equals(stavkaPonude)) {
                        oldStavkaIdstavkaOfStavkaPorudzbineCollectionNewStavkaPorudzbine.getStavkaPorudzbineCollection().remove(stavkaPorudzbineCollectionNewStavkaPorudzbine);
                        oldStavkaIdstavkaOfStavkaPorudzbineCollectionNewStavkaPorudzbine = em.merge(oldStavkaIdstavkaOfStavkaPorudzbineCollectionNewStavkaPorudzbine);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = stavkaPonude.getIdstavka();
                if (findStavkaPonude(id) == null) {
                    throw new NonexistentEntityException("The stavkaPonude with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StavkaPonude stavkaPonude;
            try {
                stavkaPonude = em.getReference(StavkaPonude.class, id);
                stavkaPonude.getIdstavka();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stavkaPonude with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StavkaPorudzbine> stavkaPorudzbineCollectionOrphanCheck = stavkaPonude.getStavkaPorudzbineCollection();
            for (StavkaPorudzbine stavkaPorudzbineCollectionOrphanCheckStavkaPorudzbine : stavkaPorudzbineCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StavkaPonude (" + stavkaPonude + ") cannot be destroyed since the StavkaPorudzbine " + stavkaPorudzbineCollectionOrphanCheckStavkaPorudzbine + " in its stavkaPorudzbineCollection field has a non-nullable stavkaIdstavka field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Artikl artikl = stavkaPonude.getArtikl();
            if (artikl != null) {
                artikl.getStavkaPonudeCollection().remove(stavkaPonude);
                artikl = em.merge(artikl);
            }
            Ponuda ponuda = stavkaPonude.getPonuda();
            if (ponuda != null) {
                ponuda.getStavkaPonudeCollection().remove(stavkaPonude);
                ponuda = em.merge(ponuda);
            }
            em.remove(stavkaPonude);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StavkaPonude> findStavkaPonudeEntities() {
        return findStavkaPonudeEntities(true, -1, -1);
    }

    public List<StavkaPonude> findStavkaPonudeEntities(int maxResults, int firstResult) {
        return findStavkaPonudeEntities(false, maxResults, firstResult);
    }

    private List<StavkaPonude> findStavkaPonudeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StavkaPonude.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StavkaPonude findStavkaPonude(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StavkaPonude.class, id);
        } finally {
            em.close();
        }
    }

    public int getStavkaPonudeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StavkaPonude> rt = cq.from(StavkaPonude.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
